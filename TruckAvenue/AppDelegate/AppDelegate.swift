//
//  AppDelegate.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import CoreLocation

let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
let theme = ThemeManager.shared.currentTheme()
let logger = Logger.shared
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    public var route: AppDelegateRoute!
    public var model: AppDelegateModel = AppDelegateModel()
    public let application = UIApplication.shared
    internal let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.initialize()
        
        return true
    }
    
    private func initialize() {
        window = UIWindow(frame: UIScreen.main.bounds)
        route = AppDelegateRoute(window: window)
        self.applyTheme(theme: .theme1)
        route.navigate(to: .splash)
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.startUpdatingLocation()
        }
    }
    
    public func applyTheme(theme: Theme) {
        self.model.themeManager.setCurrentTheme(theme)
        application.delegate?.window??.tintColor = theme.mainColor
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let lat = location.coordinate.latitude as Double
            let lon = location.coordinate.longitude as Double
            CurrentLocationManager.shared.lon = lon
            CurrentLocationManager.shared.lat = lat
            CurrentLocationManager.shared.delegate?.locationChange(lon: lon, lat: lat)
        }
    }
    
    // If we have been deined access give the user the option to change it
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            print("denied")
        }
    }
}
