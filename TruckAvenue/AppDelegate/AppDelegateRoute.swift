//
//  AppDelegateRoute.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class AppDelegateRoute {
    
    public enum Destination {
        case authentication
        case main
        case splash
    }
    
    public var window: UIWindow?
    
    public var navigationController: UINavigationController?
    
    public init(window: UIWindow?) {
        self.window = window
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        UITextField.appearance().tintColor = #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1)
        
//        let navBarLeftImageInsets = UIEdgeInsets(top: 0, left: 10, bottom: 2, right: 0)
//        if let backImage = #imageLiteral(resourceName: "icon_back").withInsets(navBarLeftImageInsets) {
//            UINavigationBar.appearance().backIndicatorImage = backImage
//            UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
//
//        }
//
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0.0, vertical: 0.0), for: .default)
        UINavigationBar.appearance().tintColor = .black
    }
    
    public func navigate(to destination: Destination) {
        switch destination {
        case .authentication:
            showAuthentication()
        case .splash:
            showSplashController()
        case .main:
            showMainController()
        }
    }
    
    public func clearStatusBar() {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    public func fillStatusBar() {
        UIApplication.shared.statusBarView?.backgroundColor = appDelegate.model.themeManager.currentTheme().backgroudColor
    }
    
    public func blurishStatusBar() {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
}

extension AppDelegateRoute {
    private func showAuthentication() {
        let controller = AuthController()
        self.fillStatusBar()
        self.navigationController = UINavigationController(rootViewController: controller)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    private func showSplashController() {
        let controller = SplashViewController()
        self.clearStatusBar()
        
        self.navigationController = UINavigationController(rootViewController: controller)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    private func showMainController() {
        let controller = TabBarController()
        self.blurishStatusBar()
//        self.navigationController = UINavigationController()
//        self.navigationController?.viewControllers = [controller]
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }
}
