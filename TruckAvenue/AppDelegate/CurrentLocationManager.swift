//
//  MapViewModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public protocol CurrentLocationProtocol {
    func locationChange(lon: Double, lat: Double)
}

public class CurrentLocationManager {
    public var lon: Double = 0
    public var lat: Double = 0
    public var delegate: CurrentLocationProtocol?
    
    public static let shared: CurrentLocationManager = CurrentLocationManager()
    
    private init() {
        
    }
}
