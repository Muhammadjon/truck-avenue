//
//  AppDelegateModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class AppDelegateModel {
    
    public let themeManager = ThemeManager.shared
    private let apiWorker: MainApiWorker = MainApiWorker.shared
    private let userApiWorker: AuthenticationApiWorker = AuthenticationApiWorker.shared

    public init() {
        
    }
    
    public func loadPrerequisites(_ completion: @escaping ((Bool) -> Void)) {
        var maxTask = 4
        var taskDone = 0
        
        func handleTaskFinish() {
            taskDone = taskDone + 1
            if taskDone == maxTask {
                logger.log(AppDelegateModel.self, message: "Prerequisites loading successfull")
                logger.log(AppDelegateModel.self, message: "Token \(UserPreferences.token)")
                completion(true)
            }
        }
        
        self.loadTruckTypes { (isTypesLoaded) in
            if isTypesLoaded {
                logger.log(AppDelegateModel.self, message: "Truck types loaded")
            }
            handleTaskFinish()
        }
        
        self.loadTrucks { (isTrucksLoaded) in
            if isTrucksLoaded {
                logger.log(AppDelegateModel.self, message: "Truck list loaded")
            }
            handleTaskFinish()
        }
        
        self.loadUserDetails {
            handleTaskFinish()
        }
        
        self.apiWorker.loadAllCards { (isOK) in
            
            handleTaskFinish()
        }
    }
    
    public func loadTruckTypes(_ completion: @escaping ((Bool) -> Void)) {
        apiWorker.getTruckTypeList { (model) in
            if let model = model {
                model.trucks?.forEach({ (item) in
                    let type = TruckType()
                    type.wrap(item)
                    TruckTypeManager.shared.items.append(type)
                })
                completion(true)
                return
            }
            completion(false)
        }
    }
    
    public func loadTrucks(_ completion: @escaping ((Bool) -> Void)) {
        _ = TruckManager.shared.deleteAll()
        apiWorker.getTruckList { (model) in
            model?.trucks?.forEach({ (truckItem) in
                TruckManager.shared.append(response: truckItem)
            })
            TruckManager.shared.sort()
            completion(model != nil)
        }
    }
    
    public func loadUserDetails(_ completion: @escaping (() -> Void)) {
        userApiWorker.getUserDetails(completion)
    }
}
