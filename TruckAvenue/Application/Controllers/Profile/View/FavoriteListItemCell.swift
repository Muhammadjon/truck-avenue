//
//  FavoriteListItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class FavoritListItemCell: UITableViewCell {
    
    private let theme = ThemeManager.shared.currentTheme()
    
    public var favoriteTitleLable: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    
    public var favoriteDescriptionLabel: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubviews([favoriteTitleLable, favoriteDescriptionLabel])
        favoriteTitleLable.text = "title"
        favoriteDescriptionLabel.text = "description"
        self.selectionStyle = .none
        favoriteTitleLable.snp.makeConstraints { (make) in
            make.leading.equalTo(self.contentView).inset(16)
            make.trailing.equalTo(self.contentView).inset(-16)
            make.top.equalTo(self.contentView).inset(8)
            make.height.equalTo(30)
        }
        
        favoriteDescriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(favoriteTitleLable.snp.bottom)
            make.leading.equalTo(favoriteTitleLable)
            make.trailing.equalTo(favoriteTitleLable)
            make.bottom.equalTo(-4)
        }
        
        self.favoriteTitleLable.font = theme.appFontBold.withSize(14)
        
        self.favoriteDescriptionLabel.textColor = theme.labelGray
        self.favoriteDescriptionLabel.font = theme.appFont.withSize(12)
        self.favoriteDescriptionLabel.numberOfLines = 3
        self.favoriteDescriptionLabel.sizeToFit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setModel(_ model: FavoritListItemInput) {
        self.favoriteTitleLable.text = model.title
        self.favoriteDescriptionLabel.text = model.description
    }
}
