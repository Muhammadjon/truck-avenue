//
//  TruckListItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class TruckListItemCell: UITableViewCell {
    private let theme = ThemeManager.shared.currentTheme()
    public var truckImageView: UIImageView = {
        let imgv = UIImageView()
        imgv.clipsToBounds = true
        imgv.contentMode = UIView.ContentMode.scaleAspectFill
        return imgv
    }()
    
    public var truckTitle: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    
    public var truckNumber: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    
    private var model: TruckListItemInput?
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubviews([truckImageView, truckTitle, truckNumber])
        
        truckImageView.layer.cornerRadius = 6
    
        self.truckTitle.font = theme.appFontBold.withSize(16)
        
        self.truckNumber.textColor = theme.labelGray
        self.truckNumber.font = theme.appFont.withSize(14)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setModel(_ model: TruckListItemInput) {
        self.model = model
        
        self.truckTitle.text = model.title
        var mainText = ""
        if model.isMain {
            mainText = "(main)"
        }
        self.truckNumber.text = "\(model.model ?? "") \(mainText)"
       
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        truckImageView.snp.makeConstraints { (make) in
            make.leading.equalTo(8)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.width.equalTo(truckImageView.snp.height)
        }
        
        truckTitle.snp.makeConstraints { (make) in
            make.leading.equalTo(self.truckImageView.snp.trailing).inset(-16)
            make.trailing.equalTo(self.contentView).inset(8)
            make.top.equalTo(self.truckImageView)
            make.height.equalTo(30)
        }
        
        truckNumber.snp.makeConstraints { (make) in
            make.top.equalTo(truckTitle.snp.bottom)
            make.leading.equalTo(truckTitle)
            make.trailing.equalTo(truckTitle)
            make.bottom.equalTo(self.contentView).inset(8)
        }
        self.loader(show: truckImageView.image == nil)
        self.truckImageView.download(urlString: self.model?.fullURL ?? "", withCache: true, completion: { (isOK) in
            if !isOK {
                self.truckImageView.image = self.model?.image
            } else {
                self.loader(show: false)
                self.model?.image = self.truckImageView.image
            }
        })
    }
    
    private func loader(show: Bool) {
        if show {
            let x = (self.contentView.height - 16) / 2
            let y = x
            self.truckImageView.showLoader(center: CGPoint(x: x, y: y), false)
        } else {
            self.truckImageView.removeLoader()
        }
    }
}
