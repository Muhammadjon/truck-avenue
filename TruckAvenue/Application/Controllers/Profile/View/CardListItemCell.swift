//
//  CardListItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class CardListItemCell: UITableViewCell {
    
    private let cardNumberLabel: UILabel = UILabel()
    private let expireDateLabel: UILabel = UILabel()
    private let ccvLabel: UILabel = UILabel()
    private let logoImageView: UIImageView = UIImageView()
    private let bgView: UIView = UIView()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        self.addSubview(bgView)
        
        cardNumberLabel.font = theme.cardFont.withSize(14)
        expireDateLabel.textColor = theme.labelGray
        ccvLabel.textColor = theme.labelGray
        expireDateLabel.font = theme.cardFont.withSize(11)
        ccvLabel.font = theme.cardFont.withSize(11)
        
        bgView.layer.cornerRadius = 8
        bgView.backgroundColor = theme.backgroudColor
        bgView.addSubviews([cardNumberLabel, expireDateLabel, ccvLabel, logoImageView])
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setModel(_ model: CardListItemInput) {
        self.cardNumberLabel.text = "\(model.formattedNumber)"
        self.expireDateLabel.text = "\(model.expMonth)/\(model.expYear[2..<4])"

        if model.type == .visa {
            self.logoImageView.image = #imageLiteral(resourceName: "visa")
        } else {
            self.logoImageView.image = #imageLiteral(resourceName: "paypal")
        }
        
        self.ccvLabel.text = "cvv **\(model.cvv.last ?? "*")"
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        bgView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        cardNumberLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(8)
            make.top.equalTo(8)
            make.height.equalTo(20)
            make.width.equalTo(cardNumberLabel.widthByText(height: 20))
        }
        
        expireDateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(cardNumberLabel.snp.bottom).offset(8)
            make.leading.equalTo(cardNumberLabel.snp.leading)
            make.height.equalTo(14)
            make.width.equalTo(expireDateLabel.widthByText(height: 14))
        }
        
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.trailing.equalTo(-8)
            make.width.height.equalTo(32)
        }
        
        ccvLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(expireDateLabel.snp.trailing).offset(16)
            make.height.equalTo(16)
            make.width.equalTo(100)
            make.top.equalTo(expireDateLabel.snp.top)
        }
    }
}
