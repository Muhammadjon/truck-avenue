//
//  ProfileHeaderView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class HeaderActionView: UIView {
    public let titleLable: UILabel = {
       let lbl = UILabel()
        return lbl
    }()
    
    public let addButton: UIButton = {
        let btn = UIButton()
        btn.titleLabel?.textAlignment = .right
        btn.setTitleColor(theme.bigBlueTextColor, for: .normal)
        return btn
    }()
    
    public let bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = theme.separatorGray
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubviews([titleLable, addButton, bottomLine])
        self.backgroundColor = UIColor.white
        self.titleLable.font = theme.appFontBold.withSize(18)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        var x: CGFloat = 16
        var h: CGFloat = 20
        var y: CGFloat = (self.height - h) / 2
        var w: CGFloat = self.width / 3
        titleLable.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = self.width / 3
        x = self.width - w - 16
        self.addButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = 1
        x = 0
        w = self.width
        y = self.height - h
        self.bottomLine.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setInput(title: String, btnText: String) {
        titleLable.text = title.uppercased()
        addButton.setTitle(btnText.uppercased(), for: .normal)
    }
    
    public func addTarget(target: Any?, action: Selector, for event: UIControl.Event) {
        self.addButton.addTarget(target, action: action, for: event)
    }
}
