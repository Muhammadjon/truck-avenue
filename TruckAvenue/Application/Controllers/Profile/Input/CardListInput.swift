//
//  CardListInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class CardListInput {
    public var items: [CardListItemInput] = CardManager.shared.items
}
