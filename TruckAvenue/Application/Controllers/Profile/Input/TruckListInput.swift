//
//  TruckListInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class TruckListInput {
    public var items: [TruckListItemInput] = TruckManager.shared.items
}
