//
//  MainModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ProfileModel {
    public var input: ProfileInput = ProfileInput()
    
    public static let shared: ProfileModel = ProfileModel()
    
    private let apiWorker: MainApiWorker = MainApiWorker.shared
    private let userApiWorker: AuthenticationApiWorker = AuthenticationApiWorker.shared
    
    public init() {
        
    }
    
    public func loadFavoriteParkingList() {
        
    }
    
    public func loadCardList() {
        
    }
    
    public func updateUser(image: UIImage, username: String, email: String, phone_number: String,
                           completion: @escaping ((Bool, String) -> Void)) {
        userApiWorker.updateUser(avatar: image, username: username, email: email, phone_number: phone_number) { (isOK) in
            completion(!ApplicationNetwork.StatusDetector.hasError, ApplicationNetwork.StatusDetector.errorMessage ?? "")
        }
    }
    
    public func deleteCard(cardId id: Int, _ completion: @escaping ((Bool) -> Void)) {
        apiWorker.deleteCard(cardId: id, completion)
    }
    
    public func deleteTruck(truckId id: Int, _ completion: @escaping ((Bool) -> Void)) {
        apiWorker.deleteTruck(truckID: id, completion)
    }
}
