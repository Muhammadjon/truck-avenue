//
//  AddCardsViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 24/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class AddCardsViewController: BaseViewController {
    private let bgView: UIView = UIView()
    private let cardPageButton: UIButton = {
        let btn = UIButton()
        btn.tag = 101
        btn.addTarget(self, action: #selector(onMenuClick(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let paypalPageButton: UIButton = {
        let btn = UIButton()
        btn.tag = 102
        btn.addTarget(self, action: #selector(onMenuClick(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let headerView: UIStackView = UIStackView()
    private let model: AddCardModel = AddCardModel()
    private let cardHolderNameField: EditTextField = EditTextField()
    private let cardNumberField: EditTextField = EditTextField()
    private let expiryDateField: EditTextField = EditTextField()
    private let cvvField: EditTextField = EditTextField()
    
    private let addCardButton: UIButton = UIButton()
    
    private let cardHolderNameInput = EditTextFieldInput(title: "Card holder", description: "Enter the name of the card holder")
    
    private let cardNumberInput = EditTextFieldInput(title: "Card number", description: "Enter card number")
    
    private let expiryDateInput = EditTextFieldInput(title: "Expiry date", description: "")
    
    private let cvvInput = EditTextFieldInput(title: "CVV", description: "")
    
    public override func initializeData() {
        
        self.model.controller = self
        
        self.cardPageButton.titleLabel?.font = theme.appFont.withSize(18)
        self.cardPageButton.setTitle("Card".uppercased(), for: .normal)
        
        self.paypalPageButton.titleLabel?.font = theme.appFont.withSize(18)
        self.paypalPageButton.setTitle("Paypal".uppercased(), for: .normal)
        self.cardPageButton.setTitleColor(theme.bigBlueTextColor, for: .normal)
        self.paypalPageButton.setTitleColor(theme.labelGray, for: .normal)
        
        self.cardHolderNameField.setInput(input: cardHolderNameInput)
        self.cardHolderNameField.inputType = .text
        self.cardHolderNameField.delegate = self
        self.cardHolderNameField.bind(model.nameValue)
        
        self.cardNumberField.setInput(input: cardNumberInput)
        self.cardNumberField.inputType = .number
        self.cardNumberField.delegate = self
        self.cardNumberField.bind(model.cardNumberValue)
        
        self.expiryDateField.setInput(input: self.expiryDateInput)
        self.expiryDateField.inputType = .picker
        self.expiryDateField.inputField.textAlignment = .center
        self.expiryDateField.inputField.inputView = pickerView
        self.expiryDateField.inputField.doneAccessory = true
        self.expiryDateField.delegate = self
        self.expiryDateField.bind(model.expiryDate)
        
        self.cvvField.setInput(input: self.cvvInput)
        self.cvvField.inputType = .number
        self.cvvField.delegate = self
        self.cvvField.bind(model.ccvValue)
        
        self.title = "Add card"
        
        self.addCardButton.setTitleColor(theme.appWhite, for: .normal)
        self.addCardButton.setTitle("Add card", for: .normal)
        self.addCardButton.backgroundColor = theme.appWhite
        
        self.view.backgroundColor = theme.backgroudColor
        self.bgView.backgroundColor = .white
        self.bgView.addShadow(shadowColor: theme.appShadow)
    }
    
    public override func initialize() {
        super.initialize()
        
        self.headerView.addArrangedSubview(cardPageButton)
        self.headerView.addArrangedSubview(paypalPageButton)
        
        self.scrollView.addSubview(bgView)
        
        self.bgView.addSubviews([
            headerView, cardHolderNameField,
            cardNumberField, expiryDateField,
            cvvField, addCardButton])
        
        self.updateExpiryDateString()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var x: CGFloat = 16
        var y: CGFloat = 25
        var w: CGFloat = (self.view.width - 2 * x) / 2 - x
        var h: CGFloat = 20
        
        headerView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = 70
        w = self.view.width - 2 * x
        y = headerView.maxY + 25
        self.cardHolderNameField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = cardHolderNameField.maxY + 8
        self.cardNumberField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = w / 2 - 4
        y = self.cardNumberField.maxY + 8
        self.expiryDateField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = self.expiryDateField.maxX + 8
        self.cvvField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.expiryDateField.maxY + 8
        h = sizeCalculator.height(from: 60)
        x = 16
        w = self.view.width - 2 * x
        self.addCardButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        self.addCardButton.addGradient(left: theme.buttonBackgroundGradientColor.left, right: theme.buttonBackgroundGradientColor.right)
        self.addCardButton.clipsToBounds = true
        self.addCardButton.layer.cornerRadius = 4
        
        x = 0
        y = 0
        w = self.view.width
        h = self.addCardButton.maxY + 30
        self.bgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = h + 70
        self.scrollView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        self.addCardButton.addTarget(self, action: #selector(onAction), for: .touchUpInside)
    }
    
    @objc private func onMenuClick(_ sender: UIButton) {
        if sender.tag == 101 {
            self.cardPageButton.setTitleColor(theme.bigBlueTextColor, for: .normal)
            self.paypalPageButton.setTitleColor(theme.labelGray, for: .normal)
        } else if sender.tag == 102 {
            self.cardPageButton.setTitleColor(theme.labelGray, for: .normal)
            self.paypalPageButton.setTitleColor(theme.bigBlueTextColor, for: .normal)
        }
    }
    
    @objc private func onAction() {
        if  !(self.cardHolderNameField.checkInput("Not valid card holder") &&
            self.cardNumberField.checkInput("Not valid card number") &&
            self.cvvField.checkInput("Not valid cvv")) {
            
            return
        }
        self.showSpinner(text: "Please wait")
        self.model.addVisaCard { (isOK) in
            self.removeSpinner({
                if !isOK {
                    self.alertInfo(title: "Error", message: ApplicationNetwork.StatusDetector.errorMessage ?? "unknow error")
                }
            })
        }
    }
}

extension AddCardsViewController: EditTextFieldDelegate {
    public func onTyping(field: EditTextField, text: String) -> Bool {
        if let value = field.input.bindedValue as? StringValue {
            if value.maxLength < text.count {
                return false
            }
        }
        return true
    }
    
    public override func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    public override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return self.model.months.count
        }
        return self.model.years.count
    }
    
    public override func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return self.model.months[row]
        }
        return self.model.years[row]
    }
    
    public override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            self.model.selectedMonth = row
        } else if component == 1 {
            self.model.selectedYear = row
        }
        self.updateExpiryDateString()
    }
    
    internal func updateExpiryDateString() {
        let month = self.model.months[self.model.selectedMonth]
        let year = self.model.years[self.model.selectedYear]
        expiryDateField.inputField.text = "\(month) / \(year)"
    }
}
