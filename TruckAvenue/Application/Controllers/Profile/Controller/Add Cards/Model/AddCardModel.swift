//
//  AddCardModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 25/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class AddCardModel {
    public var cardNumberValue: CardNumberValue = CardNumberValue(16)
    public var expiryDate: StringValue = StringValue(5)
    public var nameValue: StringValue = StringValue(50)
    public var ccvValue: StringValue = StringValue(5)
    public var months: [String] = []
    public var years: [String] = []
    
    public var selectedMonth: Int = 0
    public var selectedYear: Int = 0
    
    private let apiWorker: MainApiWorker = MainApiWorker.shared
    
    public var controller: AddCardsViewController?
    
    public init() {
        cardNumberValue.isRequired = true
        expiryDate.isRequired = true
        nameValue.isRequired = true
        ccvValue.isRequired = true
        months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
        let year = Int(Date.currentYear) ?? 1
        for i in year..<(year+20) {
            years.append("\(i)")
        }
    }
    
    public func loadAllCards(_ completion: @escaping ((Bool) -> Void)) {
        
    }
    
    public func addVisaCard(_ completion: @escaping ((Bool) -> Void)) {
        let exYear = selectedYear + (Int(Date.currentYear) ?? 0)
        let exMonth = selectedMonth + 1
        apiWorker.addVisaCard(name: nameValue.text,
                              number: cardNumberValue.text,
                              exMonth: exMonth,
                              exYear: exYear,
                              cvv: Int(ccvValue.text) ?? 0) { (model) in
                                if model != nil {
                                    self.loadAllCards(completion)
                                    return
                                }
                                completion(false)
                                
        }
    }
}
