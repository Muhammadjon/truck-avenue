//
//  AddTruckController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class AddTruckController: BaseViewController {
    private let bgView: UIView = UIView()
    public let truckNumberInputField: EditTextField = EditTextField()
    public let truckModelInputField: EditTextField = EditTextField()
    private let truckPhotoLabel: UILabel = UILabel()
    private let truckPhotoOptionalLabel: UILabel = UILabel()
    public let truckPhotoImageView: UIImageView = UIImageView()
    private let pickImageButton: UIButton = UIButton()
    private let actionButton: UIButton = UIButton()
    private let makeMainLabel: UILabel = UILabel()
    private let mainSwitch: UISwitch = UISwitch()

    private var model: AddTruckViewModel? = nil
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDelegate.route.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    public override func initialize() {
        super.initialize()
        self.shouldHideKeyboardOnTapView = true
        if model == nil {
            model = AddTruckViewModel(controller: self)
            if self.model?.hasTruckType ?? false {
                self.model?.truckModel.value = self.model?.truckModelList[0].name ?? ""
            }
        }
        
        bgView.backgroundColor = theme.appWhite
        bgView.addShadow(shadowColor: theme.appShadow)
        
        self.view.addSubviews([bgView, actionButton])
        
        let truckNumberInput = EditTextFieldInput(title: "Number truck", description: "Enter truck number")
        truckNumberInputField.setInput(input: truckNumberInput)
        truckNumberInputField.inputType = .text
        truckNumberInputField.keyboardType = .asciiCapable
        truckNumberInputField.inputField.autocapitalizationType = .allCharacters
        
        let truckModelInput = EditTextFieldInput(title: "Truck model", description: "Enter truck model")
        truckModelInputField.setInput(input: truckModelInput)
        truckModelInputField.inputType = .text
        
        truckModelInputField.inputField.inputView = pickerView
        truckModelInputField.inputField.doneAccessory = true
        
        truckPhotoLabel.text = "Driver / Truck photo"
        truckPhotoLabel.font = theme.appFont.withSize(14)
        
        truckPhotoOptionalLabel.font = theme.appFont.withSize(14)
        truckPhotoOptionalLabel.text = "(Optional)"
        truckPhotoOptionalLabel.textColor = theme.labelGray
        if model?.isForRegister ?? false {
            actionButton.setTitle("Add truck".uppercased(), for: .normal)
        } else {
            actionButton.setTitle("Update Truck".uppercased(), for: .normal)
        }
        
        actionButton.addTarget(self, action: #selector(actionButtonAction), for: .touchUpInside)
        
        if let img = model?.image {
            truckPhotoImageView.image = img
        } else {
            truckPhotoImageView.image = #imageLiteral(resourceName: "no_imagephoto_icon")
        }
        
        truckPhotoImageView.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onImageClicked))
        truckPhotoImageView.isUserInteractionEnabled = true
        truckPhotoImageView.addGestureRecognizer(tapGestureRecognizer)
        
        mainSwitch.isOn = model?.isMain ?? false
        
        makeMainLabel.text = "Make main"
        makeMainLabel.font = theme.appFont.withSize(14)
        
//        mainSwitch.onClick(target: self, selector: #selector(onSwitchClick))
        mainSwitch.addTarget(self, action: #selector(onSwitchClick(_ :)), for: .touchUpInside)
        
        pickImageButton.setTitle("Pick image", for: .normal)
        pickImageButton.titleLabel?.font = theme.appFont.withSize(12)
        pickImageButton.setTitleColor(theme.iconBlue, for: .normal)
        pickImageButton.addTarget(self, action: #selector(onImageViewClicked), for: .touchUpInside)
        
        bgView.addSubviews([truckNumberInputField, truckModelInputField, truckPhotoLabel, truckPhotoOptionalLabel, truckPhotoImageView, makeMainLabel, mainSwitch, pickImageButton])
        
        self.bindFields()
    }
    
    @objc private func onSwitchClick(_ sender: UISwitch) {
        self.model?.isMain = mainSwitch.isOn
    }
    
    @objc private func onImageViewClicked() {
        self.model?.loadImageFromGallery()
    }
    
    @objc private func onImageClicked() {
        let vc = ImageViewerController()
        if let img = self.truckPhotoImageView.image {
            vc.setImage(img)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var x: CGFloat = 0
        var y: CGFloat = self.topHeight
        var w: CGFloat = self.view.width
        var h: CGFloat = sizeCalculator.height(from: 433)
        
        self.bgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = sizeCalculator.height(from: 33)
        x = sizeCalculator.width(from: 16)
        w = self.view.width - 2 * x
        h = 70
        
        self.truckNumberInputField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = truckNumberInputField.maxY + sizeCalculator.height(from: 20)

        self.truckModelInputField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = truckModelInputField.maxY + sizeCalculator.height(from: 8)
        w = self.view.width / 2
        h = 31
        self.makeMainLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = 51
        x = self.view.width - w - sizeCalculator.width(from: 16)
        self.mainSwitch.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = makeMainLabel.maxY + sizeCalculator.height(from: 10)
        x = sizeCalculator.width(from: 16)
        h = sizeCalculator.height(from: 20)
        w = truckPhotoLabel.widthByText(height: h)
        
        truckPhotoLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = truckPhotoLabel.maxX + sizeCalculator.width(from: 8)
        w = truckPhotoOptionalLabel.widthByText(height: h)
        truckPhotoOptionalLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = sizeCalculator.width(from: 16)
        y = truckPhotoLabel.maxY + sizeCalculator.height(from: 12)
        w = sizeCalculator.height(from: 80)
        h = w
        self.truckPhotoImageView.frame = CGRect(x: x, y: y, width: w, height: h)
        truckPhotoImageView.layer.cornerRadius = 4
        
        w = self.view.width
        h = sizeCalculator.height(from: 60)
        y = self.view.height - h
        x = 0
        actionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = sizeCalculator.width(from: 16)
        h = 30
        w = truckPhotoImageView.width
        y = truckPhotoImageView.maxY + 16
        pickImageButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        actionButton.addGradient(
            left: theme.buttonBackgroundGradientColor.left,
            right: theme.buttonBackgroundGradientColor.right)
    }
    
    @objc func actionButtonAction() {
        
        if !(model?.isForRegister ?? true) {
            self.makeMain()
            return
        }
        
        self.showSpinner(text: "Uploading info, please wait!")
        self.model?.addTruck { isOK in
            self.removeSpinner()
            if isOK {
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                if ApplicationNetwork.StatusDetector.hasError {
                    self.alertInfo(title: "Error", message: ApplicationNetwork.StatusDetector.errorMessage ?? "")
                }
            }
        }
    }
    
    private func makeMain() {
        self.showSpinner(text: "Updating")
        self.model?.updateTruck {
            self.removeSpinner()
            logger.log(message: "updated")
            self.navigationController?.popViewController(animated: true)
        }
//        self.model?.makeMain({ (isOK) in
//            self.removeSpinner()
//            TruckManager.shared.makeMain(self.model!.item!.id)
//        })
    }
    
    internal func bindFields() {
        if let binder = self.model?.truckNumber {
            truckNumberInputField.bind(binder)
            truckNumberInputField.isEnabled = binder.text.isEmpty
        }
        
        if let binder = self.model?.truckModel {
            truckModelInputField.bind(binder)
            truckModelInputField.isEnabled = self.model?.truckNumber.text.isEmpty ?? false
        }
    }
}

extension AddTruckController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.truckPhotoImageView.image = image
                self.model?.image = image
                self.model?.pickedImage = image
            }
        })
    }
    
    public func setModel(_ model: AddTruckViewModel) {
        self.model = model
        self.bindFields()
    }
}

extension AddTruckController {
    public override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.model?.truckModelList.count ?? 0
    }
    
    public override func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = self.model?.truckModelList[row].name ?? ""
        return title
    }
    
    public override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        truckModelInputField.inputField.text = self.model?.truckModelList[row].name ?? ""
    }
}
