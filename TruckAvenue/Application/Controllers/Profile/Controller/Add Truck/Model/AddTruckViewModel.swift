//
//  AddTruckViewModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class AddTruckViewModel {
    public var controller: AddTruckController
    public let imagePicker = UIImagePickerController()
    private let server: AuthenticationApiWorker = AuthenticationApiWorker.shared
    
    public var pickedImage: UIImage?
    
    public let truckModelList = TruckTypeManager.shared.getAll()
    public let hasTruckType = TruckTypeManager.shared.hasTypes()
    public var truckNumber: StringValue = StringValue(24)
    public var truckModel: StringValue = StringValue(100)
    public var isMain: Bool = false
    public var image: UIImage?
    public var isForRegister: Bool = true
    
    public var item: TruckListItemInput?
    
    public init(controller: AddTruckController) {
        self.controller = controller
        truckNumber.isRequired = true
        truckModel.isRequired = true
    }
    
    @objc public func loadImageFromGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = controller
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            controller.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    public func checkInputFields() -> Bool {
        return self.controller.truckNumberInputField.checkInput("Truck number not valid") &&
            self.controller.truckModelInputField.checkInput("Truck model not valid")
    }
    
    public func addTruck(_ completion: @escaping ((Bool) -> Void)) {
        if checkInputFields() {
            server.createTruck(image: self.image,
                               truckNumber: truckNumber.value,
                               truckModel: truckModel.value,
                               isMain: isMain) { isOK in
                                
                                completion(isOK)
            }
        } else {
            completion(false)
        }
    }
    
    public func makeMain(_ completion: @escaping ((Bool) -> Void)) {
        guard let id = item?.id else {
            return
        }
        
        server.makeTruckMain(truckID: id, completion)
    }
    
    public func updateTruck(_ completion: @escaping (() -> Void)) {
        guard let id = item?.id else {
            completion()
            return
        }
        
        server.updateTruck(id: id, image: self.pickedImage, truckNumber: nil, truckModel: nil, isMain: self.isMain) { (isOK) in
            logger.log(message: "\(isOK)")
            completion()
        }
    }
}

