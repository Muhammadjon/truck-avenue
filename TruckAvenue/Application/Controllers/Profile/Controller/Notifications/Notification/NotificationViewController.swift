//
//  NotificationViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 11/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class NotificationViewController: BaseViewController {

    private let bgView: UIView = UIView()
    
    private let dateLabel: UILabel = UILabel()
    private let titleLabel: UILabel = UILabel()
    private let descriptionLabel: UILabel = UILabel()
    
    private let imgView: UIImageView = UIImageView()
    
    public var input: NotificationItemInput?
    
    public override func initialize() {
        super.initialize()
        self.scrollView.addSubview(bgView)
        
        guard let _ = self.input else {
            fatalError("set input for notification controller")
        }
        
        bgView.addSubviews([dateLabel, titleLabel, imgView, descriptionLabel])
        imgView.download(urlString: input?.imageURL ?? "", placeholder: #imageLiteral(resourceName: "no_imagephoto_icon"), withCache: true)
    }
    
    public override func initializeData() {
        super.initializeData()
        
        descriptionLabel.numberOfLines = 0
        titleLabel.numberOfLines = 3
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        
        dateLabel.font = theme.appFont.withSize(12)
        dateLabel.textColor = theme.labelGray
        
        descriptionLabel.textColor = theme.appBlack
        descriptionLabel.font = theme.appFont.withSize(14)
        titleLabel.font = descriptionLabel.font
        
        self.descriptionLabel.text = input?.description
        self.dateLabel.text = input?.date
        self.titleLabel.text = input?.title
        self.imgView.image = #imageLiteral(resourceName: "notif_def_img")
        
        self.scrollView.backgroundColor = theme.backgroudColor
        self.bgView.backgroundColor = theme.appWhite
        self.bgView.addShadow(shadowColor: theme.appShadow)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var y: CGFloat = 8
        let x: CGFloat = 16
        let w: CGFloat = self.view.width - 2 * x
        var h: CGFloat = 16
        self.dateLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.dateLabel.maxY + 4
        h = 60
        self.titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = titleLabel.maxY + 4
        h = 150
        self.imgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        let height = self.input!.description.height(withConstrainedWidth: self.titleLabel.width, font: theme.appFont.withSize(14))
        
        self.descriptionLabel.frame = CGRect(x: x, y: imgView.maxY + 4, width: self.titleLabel.width, height: height + 30)
        
        self.bgView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: descriptionLabel.maxY + 4)
        
        h = descriptionLabel.maxY + 60
        
        self.scrollView.frame = self.view.bounds
        self.scrollView.contentSize = CGSize(width: self.view.width, height: h)
    }
}
