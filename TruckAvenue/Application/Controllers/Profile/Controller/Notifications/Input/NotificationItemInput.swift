//
//  NotificationItemInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 04/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class NotificationItemInput: BaseTableViewItemInputModel {
    public var title: String = """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus condimentum neque quis laoreet. Pellentesque faucibus eleifend    
    """
    
    public var description: String = """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dapibus condimentum neque quis laoreet. Pellentesque faucibus eleifend ligula eget hendrerit. Fusce tincidunt ipsum nec neque placerat, vel pretium nisl sodales. Aliquam tempor nec eros vel volutpat. Aenean enim nibh, ornare sed nisi eu, maximus sollicitudin quam. Maecenas quis pharetra lectus. Quisque dignissim massa quis risus condimentum maximus. Ut at lectus lorem. Ut placerat auctor libero non luctus. Maecenas vitae arcu maximus, viverra purus sit amet, egestas tortor. Quisque finibus lorem nec pretium pretium. Curabitur vitae nisl scelerisque eros porta scelerisque. Nunc nunc ante, blandit quis ipsum vitae, fringilla consectetur turpis. In suscipit nec mauris et luctus. Ut volutpat eu ante sit amet hendrerit.
    
    Pellentesque placerat faucibus ipsum non commodo. Cras volutpat ornare nulla a placerat. Etiam pellentesque varius lacus ac luctus. In sit amet consequat augue. Sed sit amet pharetra sapien. Aenean magna dui, suscipit id porttitor nec, ornare quis ex. Pellentesque a lacus vitae tellus auctor posuere at sed sem. Phasellus eget turpis quam. Fusce eu sapien vitae odio vulputate ornare in id felis. Suspendisse ipsum nibh, accumsan sit amet lacus sed, accumsan iaculis eros. Donec molestie diam at euismod congue. Nullam pellentesque sapien ut magna consequat, sed lacinia elit vulputate.
"""
    public var date: String = "03.05.2019"
    
    public var imagePath: String?
    public var imageURL: String {
        return "\(ApplicationNetwork.baseAPI)\(imagePath ?? "")"
    }
}
