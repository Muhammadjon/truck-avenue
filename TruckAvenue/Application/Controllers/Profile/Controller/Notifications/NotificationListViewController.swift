//  NotificationViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 04/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.

import UIKit

public class NotificationListViewContoller: BaseTableViewController<NotificationItemInput, NotificationItemCell> {
    private let model = NotificationsModel()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        items.append(NotificationItemInput())
        items.append(NotificationItemInput())
        items.append(NotificationItemInput())
        items.append(NotificationItemInput())
        self.title = "Notifications"
        self.showSpinner(text: "Loading")
        self.model.loadNotifications { isOK in
            self.items = self.model.notifications
            if !isOK {
                self.alertInfo(title: "error", message: "Cannot load notifications")
            }
            self.removeSpinner()
            self.tableView.reloadData()
        }
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! NotificationItemCell
        let item = items[indexPath.row]
        cell.setModel(item)
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NotificationViewController()
        vc.input = self.items[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
