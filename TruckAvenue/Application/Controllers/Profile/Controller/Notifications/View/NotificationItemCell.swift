//
//  NotificationItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 04/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class NotificationItemCell: BaseTableViewItemCell<NotificationItemInput> {
    
    private let dateLabel: UILabel = UILabel()
    private let descriptionLabel: UILabel = UILabel()
    private let imgView: UIImageView = UIImageView()
    private let theme: Theme = ThemeManager.shared.currentTheme()
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        descriptionLabel.numberOfLines = 0
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        
        dateLabel.font = theme.appFont.withSize(12)
        dateLabel.textColor = theme.labelGray
        
        descriptionLabel.textColor = theme.appBlack
        descriptionLabel.font = theme.appFont.withSize(14)
        self.contentView.addSubviews([dateLabel, descriptionLabel, imgView])
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        var y: CGFloat = 8
        let x: CGFloat = 16
        let w: CGFloat = self.contentView.width - 2 * x
        var h: CGFloat = 16
        self.dateLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.dateLabel.maxY + 4
        h = 60
        self.descriptionLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = descriptionLabel.maxY + 4
        h = self.contentView.height - y - 8
        self.imgView.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func setModel(_ model: NotificationItemInput) {
        self.dateLabel.text = model.date
        self.descriptionLabel.text = model.title
        self.imgView.download(urlString: model.imageURL, placeholder: #imageLiteral(resourceName: "no_imagephoto_icon"), withCache: true) { (isDownloaded) in
            print("loading state: \(isDownloaded)")
        }
    }
}
