//
//  NotificationsModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class NotificationsModel {
    private let apiWorker = MainApiWorker.shared
    public var notifications: [NotificationItemInput] = []
    
    public func loadNotifications(_ completion: @escaping ((Bool) -> Void)) {
        self.notifications.removeAll()
        self.apiWorker.loadNotifications { (model) in
            model?.notifications?.forEach({ (news) in
                let notif = NotificationItemInput()
                notif.title = news.title ?? ""
                notif.description = news.notificationDescription ?? ""
                notif.date = news.created ?? ""
                notif.imagePath = news.photo
                self.notifications.append(notif)
            })
            completion(model != nil)
        }
    }
}
