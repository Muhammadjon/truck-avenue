//
//  UserSettingsViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class UserSettingsViewController: BaseViewController {
    private let editLabel: UILabel = UILabel()
    private let bgView: UIView = UIView()
    private let avatarImageView: UIImageView = UIImageView()
    private let avatarShadow: UIView = UIView()
    
    private let phoneNumberLabel: UILabel = UILabel()
    private let usernameField: EditTextField = EditTextField()
    private let emailField: EditTextField = EditTextField()
    
    private let usernameInput: EditTextFieldInput = EditTextFieldInput(title: "Username", description: "Enter user full name")
    private let emailFieldInput: EditTextFieldInput = EditTextFieldInput(title: "Email", description: "Enter user working email")
    
    private let imagePicker: UIImagePickerController = UIImagePickerController()
    
    public var model: UserSettingsModel = UserSettingsModel()
    
    public override func initializeData() {
        super.initializeData()
        self.title = "User"
        self.addRightMenu(title: "save", image: #imageLiteral(resourceName: "icon_tick"), selector: #selector(save), target: self)
        phoneNumberLabel.font = theme.appFont.withSize(14)
        phoneNumberLabel.textColor = theme.labelGray
        
        editLabel.backgroundColor = theme.appBlack.withAlphaComponent(0.6)
        editLabel.textColor = theme.appWhite
        editLabel.text = "Edit"
        editLabel.font = theme.appFont.withSize(10)
        editLabel.textAlignment = .center

        avatarShadow.backgroundColor = theme.appWhite
        avatarShadow.addShadow(shadowColor: theme.appShadow)
        self.addSubview(bgView)
        self.bgView.addSubviews([avatarImageView, phoneNumberLabel, usernameField, emailField, avatarShadow])
        self.bgView.sendSubviewToBack(avatarShadow)
        self.avatarImageView.addSubview(editLabel)
        self.avatarImageView.onClick(target: self, selector: #selector(loadImageFromGallery))
        self.setUser()
    }
    
    public override func initialize() {
        super.initialize()

        self.bgView.addShadow(shadowColor: theme.appShadow)
        self.bgView.backgroundColor = theme.appWhite
        self.bgView.snp.makeConstraints { (make) in
            make.leading.top.equalTo(0)
            make.width.equalTo(self.view.snp.width)
            make.bottom.equalTo(self.emailField.snp.bottom).offset(30)
        }
        
        self.avatarImageView.contentMode = .scaleAspectFill
        self.avatarImageView.layer.cornerRadius = 40
        self.avatarShadow.layer.cornerRadius = self.avatarImageView.layer.cornerRadius
        
        self.avatarImageView.clipsToBounds = true
        self.avatarImageView.snp.makeConstraints { (make) in
            make.topMargin.equalTo(20)
            make.height.width.equalTo(80)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        self.avatarShadow.snp.makeConstraints { (make) in
            make.topMargin.equalTo(19)
            make.height.width.equalTo(82)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        self.editLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.leading.equalTo(0)
            make.trailing.equalTo(self.avatarImageView.snp.trailing)
            make.height.equalTo(20)
        }
        
        self.phoneNumberLabel.textAlignment = .center
        self.phoneNumberLabel.snp.makeConstraints { (make) in
            make.top.equalTo(avatarImageView.snp.bottom).offset(10)
            make.height.equalTo(20)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
    
        self.usernameField.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(75)
            make.top.equalTo(phoneNumberLabel.snp.bottom).offset(32)
        }
        
        self.emailField.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(75)
            make.top.equalTo(usernameField.snp.bottom).offset(8)
        }
    }
    
    @objc func save() {
        if usernameField.checkInput("Username not valid") &&
            emailField.checkInput("Email not valid") {
            self.showSpinner(text: "Please wait")
            self.model.updateUser { (isOK) in
                print("user updated result \(isOK)")
                self.removeSpinner {
                    if !isOK && ApplicationNetwork.StatusDetector.hasError {
                        self.alertInfo(title: "erorr", message: ApplicationNetwork.StatusDetector.errorMessage ?? "")
                        return
                    }
                }
                self.setUser()
            }
        }
    }
    
    private func setUser() {
        model.usernameValue.value = UserPreferences.username
        model.emailValue.value = UserPreferences.email

        self.usernameField.setInput(input: self.usernameInput)
        self.emailField.setInput(input: self.emailFieldInput)

        self.usernameField.bind(model.usernameValue)
        self.emailField.bind(model.emailValue)
        
        self.phoneNumberLabel.text = UserPreferences.login
        self.avatarImageView.download(urlString: UserPreferences.avatarURL, placeholder: #imageLiteral(resourceName: "no_imagephoto_icon")) { (isOK) in
            if !isOK {
                self.avatarImageView.image = #imageLiteral(resourceName: "no_imagephoto_icon")
            }
            self.model.userImage = self.avatarImageView.image
        }
    }
}

extension UserSettingsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc public func loadImageFromGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.avatarImageView.image = image
                self.model.userImage = image
            }
        })
    }
}
