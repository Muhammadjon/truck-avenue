//
//  UserSettingsModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class UserSettingsModel {
    public var usernameValue: StringValue = StringValue(50)
    public var emailValue: EmailValue = EmailValue(50)
    public var userImage: UIImage?
    
    private let apiWorker: AuthenticationApiWorker = AuthenticationApiWorker.shared
    
    public init() {
        usernameValue.isRequired = true
        emailValue.isRequired = true

    }
    
    public func updateUser(_ completion: @escaping ((Bool) -> Void)) {
        
        self.apiWorker.updateUser(avatar: userImage, username: usernameValue.text, email: emailValue.text, phone_number: UserPreferences.login) { (isOK) in
            completion(isOK)
        }
    }
}
