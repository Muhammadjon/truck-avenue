//
//  SettingsViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 02/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class SettingsViewController: BaseTableViewController<SettingsCellModel, SettingsTableCell> {
    private let input: SettingsInput = SettingsInput.shared
    private let headerView: UIView = UIView()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
           
        self.items = input.items
        self.title = "Settings"
        headerView.backgroundColor = theme.backgroudColor
        headerView.height = 40
        self.tableView.tableHeaderView = headerView
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.items[indexPath.row]
        switch item.settings {
        case .some(.logout):
            self.logout()
            break
        case .some(.notification):
            break
        case .some(.user):
            let vc = UserSettingsViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.row]
        if item.type == .description {
            return 50
        }
        return 40
    }
    
    private func logout() {
        self.alertInfo(title: "Warning!", message: "Do you want to log out") { (isOK) in
            if isOK {
                UserPreferences.logout()
            }
        }
    }
}
