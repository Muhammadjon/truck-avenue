//
//  SettingsTableCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 19/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class SettingsTableCell: BaseTableViewItemCell<SettingsCellModel> {
    public var sTitleLabel: UILabel = UILabel()
    public var switchView: UISwitch = UISwitch()
    public var sDescriptionLabel: UILabel = UILabel()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubviews([sTitleLabel, switchView, sDescriptionLabel])
        self.sTitleLabel.font = theme.appFont.withSize(13)
        self.sDescriptionLabel.font = theme.appFont.withSize(11)
        self.sDescriptionLabel.textColor = theme.labelGray
        
        self.sTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(4).priority(501)
            make.leading.equalTo(8)
            make.trailing.equalTo(-60)
            make.height.equalTo(20).priority(500)
        }
        
        self.sDescriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(sTitleLabel.snp.bottom).offset(4)
            make.height.equalTo(20)
            make.leading.equalTo(sTitleLabel.snp.leading)
            make.trailing.equalTo(sTitleLabel.snp.trailing)
        }
        
        self.switchView.snp.makeConstraints { (make) in
            make.top.equalTo(4)
            make.width.equalTo(50)
            //make.trailing.equalTo(0)
            make.leading.equalTo(sTitleLabel.snp.trailing)
            make.bottom.equalTo(-4)
        }
        
        self.sDescriptionLabel.sizeToFit()
        self.sDescriptionLabel.contentMode = .topLeft
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    public override func setModel(_ model: SettingsCellModel) {
        self.sTitleLabel.text = model.title
        self.sDescriptionLabel.text = model.description
        switch model.type {
        case .switch:
            switchView.isHidden = false
        default:
            switchView.isHidden = true
        }
    }
}
