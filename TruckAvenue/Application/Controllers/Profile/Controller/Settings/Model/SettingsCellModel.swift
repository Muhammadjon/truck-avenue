//
//  SettingsModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 19/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public enum SettingsItemType {
    case title
    case `switch`
    case description
}

public enum Settings {
    case user
    case notification
    case logout
}

public class SettingsCellModel: BaseTableViewItemInputModel {
    public var title: String = ""
    public var description: String = ""
    public var type: SettingsItemType = .title
    public var settings: Settings?
    
    public init(title: String, description: String, type: SettingsItemType) {
        self.title = title
        self.description = description
        self.type = type
    }
}
