//
//  SettingsInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class SettingsInput {
    public static let shared = SettingsInput()
    
    public var items: [SettingsCellModel] {
        let logout: SettingsCellModel = SettingsCellModel(title: "Logout", description: "press to log out", type: .description)
        logout.settings = .logout
        
        let notificationSwitch: SettingsCellModel = SettingsCellModel(title: "Notification", description: "", type: .switch)
        notificationSwitch.settings = .notification
        
        let profileSettings: SettingsCellModel = SettingsCellModel(title: "User", description: "Person settings (username, email, photo)", type: .description)
        profileSettings.settings = .user
        
        return [profileSettings, notificationSwitch, logout]
    }
}
