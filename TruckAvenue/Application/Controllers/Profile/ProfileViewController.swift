//
//  ProfileViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import MapKit

public class ProfileViewController: BaseViewController {
    private let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    private let profileImageShadow: UIView = UIView()
    
    private let editLabel: UILabel = UILabel()
    private let usernameLabel: UILabel = UILabel()
    private let emailLabel: UILabel = UILabel()
    private let userPhoneNumber: UILabel = UILabel()
    
    private let truckListItemIdentifier = "truck_list_item"
    private let favoriteListItemIdentifier = "favorite_list_item"
    private let cardListItemIdentifier = "card_list_item"
    private var model: ProfileModel = ProfileModel.shared
    
    private let imagePicker: UIImagePickerController = UIImagePickerController()
    
    private var input: ProfileInput {
        return model.input
    }
    private let tableView: UITableView = UITableView()
    
    private var pageView: PagerView = {
        let pw = PagerView(3)
        pw.images = [
            #imageLiteral(resourceName: "truck_icon"),
            #imageLiteral(resourceName: "star_icon"),
            #imageLiteral(resourceName: "card_icon")
        ]
        
        return pw
    }()
    
    public override func initialize() {
        super.initialize()
        editLabel.backgroundColor = theme.appBlack.withAlphaComponent(0.6)
        editLabel.textColor = theme.appWhite
        editLabel.text = "Edit"
        editLabel.font = theme.appFontBold.withSize(11)
        editLabel.textAlignment = .center
        
        profileImageView.addSubview(editLabel)
        profileImageShadow.backgroundColor = theme.appWhite
        profileImageShadow.addShadow(shadowColor: theme.appShadow)
        
        self.addSubviews([pageView, tableView, profileImageView, profileImageShadow, usernameLabel, userPhoneNumber, emailLabel])
        self.view.sendSubviewToBack(profileImageShadow)
        
        profileImageView.clipsToBounds = true
        if profileImageView.image == nil {
            profileImageView.image = #imageLiteral(resourceName: "no_imagephoto_icon")
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(loadImageFromGallery))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(tapGesture)
        
        usernameLabel.text = "Muhammadjon"
        usernameLabel.font = theme.appFont.withSize(16)
        
        userPhoneNumber.text = UserPreferences.login
        userPhoneNumber.font = theme.appFont.withSize(13)
        userPhoneNumber.textColor = theme.labelGray
        
        emailLabel.font = theme.appFont.withSize(13)
        emailLabel.textColor = theme.labelGray

        pageView.backgroundColor = theme.appWhite
        
        pageView.onSelect = { id in
            self.decidePage(id)
        }

        self.shouldHideKeyboardOnTapView = false
        
        usernameLabel.onClick(target: self, selector: #selector(onUserClick))
        userPhoneNumber.onClick(target: self, selector: #selector(onUserClick))
        emailLabel.onClick(target: self, selector: #selector(onUserClick))
    }
    
    public override func initializeData() {
        tableView.rowHeight = 80
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = theme.separatorGray
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.tableFooterView = UIView()
    }
    
    private func decidePage(_ id: Int) {
        self.tableView.reloadData()
        self.tableView.restore()
        switch id {
        case 0:
            if TruckManager.shared.getAll().isEmpty {
                self.tableView.setEmptyView(title: "No trucks", message: "Click add button to insert a truck")
            }
            break
        case 1:
            if MapViewModel.shared.getFavoriteParkingList().isEmpty {
                self.tableView.setEmptyView(title: "No favorite", message: "You don't have any favorite parking")
            }
            break
        case 2:
            if CardManager.shared.getAll().isEmpty {
                self.tableView.setEmptyView(title: "No payment method", message: "Click add to insert payment method")
            }
            break
        default:
            break
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addRightMenu(title: "Settings", image: #imageLiteral(resourceName: "settings_icon"), selector: #selector(onSettingsClicked), target: self)
        
        let imageView = UIImageView(image: UIImage(named: "notification_icon_blue"))
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.addRightMenu(view: imageView, selector: #selector(onNotificationClicked), target: self)
        
        self.navigationItem.title = "Profile"
        
        self.decidePage(pageView.selectedId)
        
        self.usernameLabel.text = UserPreferences.username
        self.emailLabel.text = UserPreferences.email

        self.tableView.reloadData()
        
        self.profileImageView.download(urlString: UserPreferences.avatarURL, placeholder: #imageLiteral(resourceName: "no_imagephoto_icon")) { (isOK) in
            if !isOK {
                //                self.profileImageView.image = #imageLiteral(resourceName: "no_imagephoto_icon")
            }
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.setRightBarButtonItems([], animated: false)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var y: CGFloat = self.topHeight + 100
        var x: CGFloat = 0
        var w: CGFloat = self.view.width
        var h: CGFloat = 50
        pageView.frame = CGRect(x: x, y: y, width: w, height: h)
        pageView.generateViews()
        
        y = pageView.maxY
        h = self.view.height - y
        tableView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = 60
        w = h
        y = (100 - w) / 2 + topHeight
        x = 16
        profileImageView.frame = CGRect(x: x, y: y, width: w, height: h)
        profileImageShadow.frame = CGRect(x: x - 1, y: y - 1, width: w + 2, height: h + 2)
        profileImageView.layer.cornerRadius = h / 2
        profileImageShadow.layer.cornerRadius = profileImageView.layer.cornerRadius
        
        editLabel.frame = profileImageView.bounds
        editLabel.height = 20
        editLabel.y = profileImageView.height - editLabel.height
//        editLabel.addGradient(left: theme.appBlack.withAlphaComponent(0), right: theme.appBlack.withAlphaComponent(0.6), CGPoint(x: 0.5, y: 0), CGPoint(x: 0.5, y: 1), locations: [NSNumber(floatLiteral: 0.0), NSNumber(floatLiteral: 0.2)])
        
        x = profileImageView.maxX + 8
        w = self.view.width - x
        h = 25
        usernameLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = 14
        y = usernameLabel.maxY + 4
        emailLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        y = emailLabel.maxY + 4
        userPhoneNumber.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    @objc public func onSettingsClicked() {
        let vc = SettingsViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc public func onNotificationClicked() {
        let vc = NotificationListViewContoller()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    @objc public func loadImageFromGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = HeaderActionView()
        switch pageView.selectedId {
        case 0:
            header.setInput(title: "trucks", btnText: "add truck")
            header.addTarget(target: self, action: #selector(addTruckAction), for: .touchUpInside)
        case 1:
            header.setInput(title: "favorites", btnText: "")
        case 2:
            header.setInput(title: "payments", btnText: "add payment")
            header.addTarget(target: self, action: #selector(addCardAction), for: .touchUpInside)
        default:
            break
        }
        
        return header
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch pageView.selectedId {
        case 0:
            return TruckManager.shared.getAll().count
        case 1:
            return MapViewModel.shared.getFavoriteParkingList().count
        case 2:
            return CardManager.shared.getAll().count
        default:
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch pageView.selectedId {
        case 0:
            var cell = tableView.dequeueReusableCell(withIdentifier: self.truckListItemIdentifier) as? TruckListItemCell
            if cell == nil {
                cell = TruckListItemCell(style: .default, reuseIdentifier: self.truckListItemIdentifier)
            }
            let item = TruckManager.shared.getAll()[indexPath.row]
            cell?.setModel(item)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: self.favoriteListItemIdentifier) as? FavoritListItemCell ?? FavoritListItemCell(style: .default, reuseIdentifier: self.favoriteListItemIdentifier)
            let item = MapViewModel.shared.getFavoriteParkingList()[indexPath.row]
            cell.favoriteTitleLable.text = item.title ?? ""
            cell.favoriteDescriptionLabel.text = item.parkingDescription ?? ""
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cardListItemIdentifier) as? CardListItemCell ?? CardListItemCell(style: .default, reuseIdentifier: self.cardListItemIdentifier)
            
            let item = CardManager.shared.getAll()[indexPath.row]
            cell.setModel(item)
            
            return cell
        default:
            break
        }
        return UITableViewCell()
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch pageView.selectedId {
        case 0:
            let item = TruckManager.shared.items[indexPath.row]
            self.openTruckInfo(item: item)
            break
        case 1:
            let p = MapViewModel.shared.getFavoriteParkingList()[indexPath.row]
            self.showSpinner(text: "Please wait !")
            MKMapSnapshotter.mapImage(longitude: p.longitude ?? "", lattitude: p.latitude ?? "", width: self.view.width, height: 200, pinImage: #imageLiteral(resourceName: "map_pin")) { (image) in
                self.removeSpinner()
                let vc = ParkingViewController(p.id!)
                if let img = image {
                    vc.setImage(img)
                }
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            break
        case 2:
            break
        default:
            break
        }
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if pageView.selectedId == 2 || pageView.selectedId == 0 {
            return true
        }
        return false
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            switch pageView.selectedId {
            case 0:
                // delete truck
                self.deleteTruck(row: indexPath.row)
                break
            case 2:
                // delete card
                self.deleteCard(row: indexPath.row)
                break
            default:
                return
            }
        }
    }
    
    private func deleteTruck(row: Int) {
        let truck = TruckManager.shared.getAll()[row]
        
        self.model.deleteTruck(truckId: truck.id) { (isOK) in
            if isOK {
                logger.log(message: "truck deletion is \(isOK)")
                _ = TruckManager.shared.delete(id: truck.id)
                UIView.reportUser("Your truck has been deleted")
                self.tableView.reloadData()
            } else {
                self.alertInfo(title: "Error", message: "There is an error while deleting the truck")
            }
        }
    }
    
    private func deleteCard(row: Int) {
        print("delete card")
        let card = CardManager.shared.getAll()[row]
        self.model.deleteCard(cardId: card.id) { (isOK) in
            if isOK {
                logger.log(message: "card deletion is \(isOK)")
                _ = CardManager.shared.delete(id: card.id)
                UIView.reportUser("You have deleted card \(card.number)")
                self.tableView.reloadData()
            } else {
                self.alertInfo(title: "Error", message: "There is an error while deleting the card")
            }
        }
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc private func addTruckAction() {
        let vc = AddTruckController()
        vc.hidesBottomBarWhenPushed = true
        vc.title = "Add Truck"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onUserClick() {
        let vc = UserSettingsViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func addCardAction() {
        let vc = AddCardsViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            let loader = LoaderController()
            loader.presentOnController(self.parent ?? self)
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.model.updateUser(image: image, username: UserPreferences.username, email: UserPreferences.email, phone_number: UserPreferences.login, completion: { (isOK, errorMessage) in
                    DispatchQueue.main.async {
                        loader.finish(isOK, completion: {                            
                            self.profileImageView.image = image
                        })
                    }
                })
            }
        })
    }
    
    public func openTruckInfo(item: TruckListItemInput) {
        let vc = AddTruckController()
        let vm = AddTruckViewModel(controller: vc)
        vm.truckModel.value = item.title ?? ""
        vm.truckNumber.value = item.model ?? ""
        vm.isMain = item.isMain
        vm.image = item.image
        vm.isForRegister = false
        vm.item = item
        vc.setModel(vm)
        vc.hidesBottomBarWhenPushed = true
        vc.title = "Update"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
