//
//  TapBarController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 30/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class TabBarController: UITabBarController, UITabBarControllerDelegate {

    var controllersList = [UIViewController]()
    
    var tabBarItems = [UITabBarItem]()

    public override func viewDidLoad() {
        initialize()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.white
    }
    
    private func initialize() {
        self.tabBar.tintColor = theme.iconBlue
        self.tabBar.backgroundColor = .white
        self.tabBar.unselectedItemTintColor = theme.labelGray
        
        delegate = self
        setControllers()
    }
    
    private func setControllers() {
        let theme = appDelegate.model.themeManager.currentTheme()
        let mainVC = MainViewController()
        let unselectedImage = #imageLiteral(resourceName: "search_icon").tintColor(color: theme.backgroudColor)
        let selectedImage = #imageLiteral(resourceName: "search_icon").tintColor(color: theme.iconBlue)
        let mainTabBar = UITabBarItem(title: "Search", image: unselectedImage, selectedImage: selectedImage)
        mainVC.tabBarItem = mainTabBar
        mainVC.parent?.title = "Search"
        
        let reservationVC = ReservationsViewController()
        let unselectedImageReserve = #imageLiteral(resourceName: "reservations_icon").tintColor(color: theme.backgroudColor)
        let selectedImageReserve = #imageLiteral(resourceName: "reservations_icon").tintColor(color: theme.iconBlue)
        let reservationTabBar = UITabBarItem(title: "Reservation", image: unselectedImageReserve, selectedImage: selectedImageReserve)
        reservationVC.tabBarItem = reservationTabBar
        reservationVC.parent?.title = "Reservation"
        
        let historyVC = HistoryViewController()
        let unselectedImageHistory = #imageLiteral(resourceName: "history_icon").tintColor(color: theme.backgroudColor)
        let selectedImageHistory = #imageLiteral(resourceName: "history_icon").tintColor(color: theme.iconBlue)
        let historyTabBar = UITabBarItem(title: "History", image: unselectedImageHistory, selectedImage: selectedImageHistory)
        historyVC.tabBarItem = historyTabBar
        historyVC.parent?.title = "History"
        
        let profileVC = ProfileViewController()
        let unselectedImageProfile = #imageLiteral(resourceName: "profile_icon").tintColor(color: theme.backgroudColor)
        let selectedImageProfile = #imageLiteral(resourceName: "profile_icon").tintColor(color: theme.iconBlue)
        let profileTabBar = UITabBarItem(title: "Profile", image: unselectedImageProfile, selectedImage: selectedImageProfile)
        profileVC.tabBarItem = profileTabBar
        profileVC.parent?.title = "Profile"
        
        self.tabBarItems = [mainTabBar, reservationTabBar, historyTabBar, profileTabBar]
        self.controllersList = [mainVC, reservationVC, historyVC, profileVC]
        self.viewControllers = self.controllersList
        
        self.viewControllers = controllersList.map { UINavigationController(rootViewController: $0)}
    }
}
