//
//  HistoryItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class HistoryItemCell: BaseTableViewItemCell<HistoryItemInputModel> {
    
    private let bgView = UIView()
    private let fromToDateLabel: UILabel = UILabel()
    private let priceLabel: UILabel = UILabel()
    private let titleLabel: UILabel = UILabel()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(bgView)
        bgView.addSubviews([fromToDateLabel, priceLabel, titleLabel])
        bgView.snp.makeConstraints { (make) in
            make.leading.top.equalTo(8)
            make.trailing.bottom.equalTo(-8)
        }
        
        fromToDateLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(8)
            make.top.equalTo(8)
            make.height.equalTo(20)
            make.width.lessThanOrEqualTo(2 * self.width / 3)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-8)
            make.top.equalTo(8)
            make.leading.equalTo(fromToDateLabel.snp.trailing)
            make.height.equalTo(fromToDateLabel.snp.height)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(fromToDateLabel.snp.bottom)
            make.leading.equalTo(8)
            make.width.equalTo(self.width - 32)
            make.bottom.equalTo(-8)
        }
        
        fromToDateLabel.font = theme.appFont.withSize(13)
        priceLabel.textAlignment = .right
        priceLabel.font = theme.appFontBold.withSize(13)
        titleLabel.font = theme.appFont.withSize(14)
        titleLabel.textColor = theme.labelGray
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func setModel(_ model: HistoryItemInputModel) {
        self.priceLabel.text = "$ \(model.amount ?? 0)"
        self.fromToDateLabel.text = "\(model.dateFrom ?? "")/\(model.dateTo ?? "")"
        self.titleLabel.text = model.description
    }
}
