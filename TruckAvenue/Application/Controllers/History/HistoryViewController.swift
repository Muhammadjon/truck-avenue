//
//  HistoryViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class HistoryViewController: BaseTableViewController<HistoryItemInputModel, HistoryItemCell> {
    private let input: HistoryViewControllerModel = HistoryViewControllerModel()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "History"
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showSpinner(text: "Loading history")
        input.loadInput {
            self.removeSpinner()
            self.items = self.input.input.history
            
            if self.items.isEmpty {
                self.tableView.setEmptyView(title: "No History", message: "Your history will be here")
            }
            
            self.tableView.reloadData()
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
