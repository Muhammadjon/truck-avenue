//
//  HistoryListInputModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class HistoryListInputModel {
    public var history: [HistoryItemInputModel] = []
    
    public init() {
        
    }
    
    public func clear() {
        history.removeAll()
    }
}
