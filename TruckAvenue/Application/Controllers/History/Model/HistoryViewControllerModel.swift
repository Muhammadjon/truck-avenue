//
//  HistoryViewControllerModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class HistoryViewControllerModel: BaseViewControllerModel {
    public var input: HistoryListInputModel = HistoryListInputModel()
    private let apiWorker: HistoryApiWorker = HistoryApiWorker.shared
    public override init() {
        super.init()
    }
    
    public override func loadInput(_ completion: @escaping (() -> Void)) {
        super.loadInput(completion)
        self.input.clear()
        apiWorker.getAllHistoryList { (_) in
            HistoryManager.shared.history.forEach({ (item) in
                let ri = HistoryItemInputModel()
                ri.amount = Float(item.model.totalPrice ?? "")
                ri.dateFrom = item.model.fromDate
                ri.dateTo = item.model.toDate
                ri.description = item.model.parking?.title
                
                self.input.history.append(ri)
            })
            completion()
        }
    }
}
