//
//  ReservationVCWorker.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ReservationViewControllerModel: BaseViewControllerModel {
    public var input: ReservationListInputModel = ReservationListInputModel()
    
    private let apiWorker: HistoryApiWorker = HistoryApiWorker.shared
    
    public override init() {
        super.init()
    }
    
    public override func loadInput(_ completion: @escaping (() -> Void)) {
        super.loadInput(completion)
        
        self.input.clear()
        
        apiWorker.getAllHistoryList { (_) in
            HistoryManager.shared.reservations.forEach({ (item) in
                let ri = ReservationItemCellInputModel()
                ri.amount = Float(item.model.totalPrice ?? "")
                ri.dateFrom = item.model.fromDate
                ri.dateTo = item.model.toDate
                ri.description = item.model.parking?.title
                ri.type = item.model.type
                self.input.reservations.append(ri)
            })
            completion()
        }
    }
}
