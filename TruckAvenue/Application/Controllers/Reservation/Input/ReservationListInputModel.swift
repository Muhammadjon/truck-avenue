//
//  ReservationListInputModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ReservationListInputModel {
    public var reservations: [ReservationItemCellInputModel] = []
    
    public init() {
        
    }
    
    public func clear() {
        self.reservations.removeAll()
    }
}
