//
//  ReservationItemCellModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ReservationItemCellInputModel: BaseTableViewItemInputModel {
    public var dateFrom: String?
    public var dateTo: String?
    public var amount: Float?
    public var description: String?
    
    public var type: HistoryType = .new
}
