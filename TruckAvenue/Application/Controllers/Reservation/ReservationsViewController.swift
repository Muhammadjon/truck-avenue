//
//  ReservationViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ReservationsViewController: BaseTableViewController<ReservationItemCellInputModel, ReservationItemCell> {
    
    private let input: ReservationViewControllerModel = ReservationViewControllerModel()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Reservation"
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadItems()
    }
    
    private func loadItems() {
        self.showSpinner(text: "Loading reservations")
        input.loadInput {
            self.removeSpinner()
            self.items = self.input.input.reservations
            
            if self.items.isEmpty {
                self.tableView.setEmptyView(title: "No Reservations", message: "Your resevations will be here")
            }
            self.tableView.reloadData()
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
