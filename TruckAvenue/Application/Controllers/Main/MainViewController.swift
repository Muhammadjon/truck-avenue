//
//  MainViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import MapKit
import FittedSheets

public class MainViewController: BaseViewController {
    private let mapView: MapView = MapView()
    private let currentLocationButton: FloatingActionButton = FloatingActionButton(0)
    private let sateliteModeButton: FloatingActionButton = FloatingActionButton(1)
    private let viewListButton: FloatingActionButton = FloatingActionButton(2)
    private var sheetController: SheetViewController?
    private let mapViewModel = MapViewModel.shared
    
    private let searchField: UITextField = UITextField()
    
    let microphoneView = UIImageView(x: 0, y: 0, w: 60, h: 24)
    
    private var speechListenerView: ListeningView?
    
    public override func initialize() {
        super.initialize()
        
        currentLocationButton.setImage(#imageLiteral(resourceName: "location_icon"))
        sateliteModeButton.setImage(#imageLiteral(resourceName: "satilate_off"))
        viewListButton.setImage(#imageLiteral(resourceName: "view_list_icon"))
        searchField.addShadow(shadowColor: theme.labelGray)
        
        microphoneView.image = #imageLiteral(resourceName: "mic_icon").tintColor(color: theme.iconGray)
        microphoneView.contentMode = .scaleAspectFit
        microphoneView.onClick(target: self, selector: #selector(onMicrophoneTapped))
        
        searchField.rightView = microphoneView
        searchField.placeholder = "Search on the map"
        searchField.rightViewMode = .always
        searchField.leftViewMode = .always
        searchField.leftView = UIView(x: 0, y: 0, w: 16, h: 8)
        searchField.backgroundColor = theme.appWhite
        searchField.tintColor = theme.labelGray
        
        self.searchField.delegate = self
        
        self.addSubviews([mapView, sateliteModeButton, viewListButton, currentLocationButton, searchField])
        
        
    }
    
    public override func initializeData() {
        super.initializeData()
        currentLocationButton.onClick = { tag in
            self.mapView.findMyLocation()
        }
        
        sateliteModeButton.onClick = { tag in
            if self.mapView.satilateModeSwitch() {
                self.sateliteModeButton.setImage(#imageLiteral(resourceName: "satilate_on"))
            } else {
                self.sateliteModeButton.setImage(#imageLiteral(resourceName: "satilate_off"))
            }
        }
        
        viewListButton.onClick = { tag in
            let vc = ParkingListViewController()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        mapView.didSelectPin = { pin in
            self.showSpinner(text: "Please wait ....")
            MKMapSnapshotter.mapImage(with: pin, width: self.view.width, height: 200, pinImage: #imageLiteral(resourceName: "map_pin"), { (image) in
                self.removeSpinner()
                guard let item = self.mapViewModel.find(parking: pin.id) else {
                    fatalError("The parking item is missing")
                }
                
                let vc = ParkingViewController(item.id ?? 0)
                
                if let img = image {
                    vc.setImage(img)
                }
                vc.onReviewClick = { targetController in
                    self.sheetController?.dismiss(animated: true, completion: {
                        targetController.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(targetController , animated: true)
                    })
                }
                
                vc.onReserve = { targetController in
                    self.sheetController?.dismiss(animated: true, completion: {
                        targetController.setPark(vc.imageView.image!, vc.parkingItem!)
                        targetController.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(targetController , animated: true)
                    })
                }
                
                self.sheetController = SheetViewController(controller: vc, sizes: [.fullScreen])
                self.sheetController?.blurBottomSafeArea = true
                
                if let sheetController = self.sheetController {
                    sheetController.blurBottomSafeArea = false
                    sheetController.topCornersRadius = 0
                    
                    sheetController.topCornersRadius = 15
                    
                    sheetController.dismissOnBackgroundTap = false
                    
                    sheetController.extendBackgroundBehindHandle = true
                    
                    sheetController.handleColor = self.theme.labelGray
                    
                    self.present(sheetController, animated: false, completion: nil)
                    self.sheetController?.willDismiss = { sheet in
                        self.mapView.deselectPin()
                    }
                }
            })
            
        }
        self.loadParkingList()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        appDelegate.route.blurishStatusBar()
        
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.mapView.frame = self.view.bounds
        var w: CGFloat = sizeCalculator.width(from: 56)
        var h: CGFloat = w
        var x: CGFloat = self.mapView.width - sizeCalculator.width(from: 16) - w
        var y: CGFloat = self.mapView.height - sizeCalculator.height(from: 22) - h - (self.tabBarController?.tabBar.frame.height ?? 0)
        
        self.currentLocationButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = y - h - sizeCalculator.width(from: 8)
        self.sateliteModeButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = y - h - sizeCalculator.width(from: 8)
        viewListButton.frame = CGRect(x: x, y: y, width: w, height: h)

        x = sizeCalculator.width(from: 8)
        y = sizeCalculator.height(from: 10) + self.topHeight
        w = self.mapView.width - 2 * x
        h = 48
        self.searchField.frame = CGRect(x: x, y: y, width: w, height: h)
        self.searchField.layer.cornerRadius = h / 2
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
        appDelegate.route.fillStatusBar()
    }

    
    internal func loadParkingList() {
        self.showSpinner(text: "Loading parking objects")
        self.mapViewModel.getParkingList { (model) in
            self.removeSpinner({
                if let _ = model {
                    self.populatePins()
                }
            })
        }
    }
    
    internal func filter(_ key: String) {
        logger.log(message: "filtering with key \(key)")
        self.mapViewModel.filter(key)
        self.mapView.clearPins()
        self.populatePins()
    }
    
    internal func populatePins() {
        self.mapViewModel.parkingList.forEach { (item) in
            self.mapView.addPin(item)
        }
    }
}


extension MainViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.filter(textField.text ?? "")
    }
}

extension MainViewController: SpeechListenerProtocol {
    
    @objc func onMicrophoneTapped() {
        microphoneView.tintColor(theme.iconBlue)
        self.parent?.tabBarController?.tabBar.isHidden = true
        speechListenerView = ListeningView()
        speechListenerView?.delegate = self
        
        self.parent?.addSubview(speechListenerView!)
        self.parent?.view.bringSubviewToFront(speechListenerView!)
        self.parent?.loadViewIfNeeded()
        
        self.speechListenerView?.frame = self.view.bounds
    }
    
    public func onSearch() {
        microphoneView.tintColor(theme.iconGray)
        self.filter(self.searchField.text ?? "")
        self.parent?.tabBarController?.tabBar.isHidden = false
        self.speechListenerView?.removeFromSuperview()
    }
    
    public func onSpeaking(text: String) {
        self.searchField.text = text
    }
}

