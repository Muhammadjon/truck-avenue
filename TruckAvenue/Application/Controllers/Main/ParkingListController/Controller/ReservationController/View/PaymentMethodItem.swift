//
//  PaymentMethodItem.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class PaymentMethodItemView: UIView {
    private let titleLabel = UILabel()
    private let rightTextLabel = UILabel()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubviews([titleLabel, rightTextLabel])
        titleLabel.font = theme.appFontBold.withSize(14)
        rightTextLabel.font = theme.appFont.withSize(14)
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(8)
            make.centerY.equalTo(self.snp.centerY)
        }
        
        rightTextLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.centerY.equalTo(self.snp.centerY)
        }
        titleLabel.text = "Payment method".uppercased()
        rightTextLabel.text = "add".uppercased()
        self.backgroundColor = theme.backgroudColor
        self.layer.cornerRadius = 6
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setCard(_ card: CardListItemInput) {
        self.titleLabel.text = card.fomattedMiniNumber
        self.titleLabel.font = theme.cardFont
        self.rightTextLabel.text = (card.type) == CardType.visa ? "VISA" : "PAYPAL"
    }
}
