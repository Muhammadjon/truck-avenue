//
//  ReservationController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 16/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ReservationController: BaseViewController {
    private let imageView: UIImageView = UIImageView()
    private let parkingTitleLabel: UILabel = UILabel()
    private let fromDateEditText: EditTextField = EditTextField()
    private let toDateEditText: EditTextField = EditTextField()
    private let fromDateToDateLabel: UILabel = UILabel()
    private let totalDateLabel: UILabel = UILabel()
    private let totalAmountLabel: UILabel = UILabel()
    private let separartorView: UIView = UIView()

    private let fromDateInput = EditTextFieldInput(title: "From", description: "")
    private let toDateInput = EditTextFieldInput(title: "To", description: "")
    
    private let datePicker: UIDatePicker = UIDatePicker()
    
    private let actionButton: UIButton = UIButton()
    
    private let paymentMethod: PaymentMethodItemView = PaymentMethodItemView()
    
    private let model = ReservationModel()
    
    public override func initializeData() {
        imageView.contentMode = .scaleAspectFill
        
        parkingTitleLabel.font = theme.appFontBold.withSize(18)
        
        fromDateToDateLabel.text = "26/05/19 – 30/05/19"
        
        totalDateLabel.text = "4 days"
        
        totalDateLabel.font = theme.appFont.withSize(13)
        fromDateToDateLabel.font = theme.appFont.withSize(13)
        
        totalAmountLabel.textAlignment = .center
        totalAmountLabel.font = theme.appFont.withSize(34)
        totalAmountLabel.text = "$30"
        
        datePicker.minimumDate = Date()
        
        datePicker.datePickerMode = .date

        fromDateEditText.inputType = .picker
        fromDateEditText.inputField.inputView = datePicker
        fromDateEditText.setInput(input: fromDateInput)
        fromDateEditText.inputField.doneAccessory = true
        
        toDateEditText.inputType = .picker
        datePicker.date = Date()
        toDateEditText.inputField.inputView = datePicker
        toDateEditText.setInput(input: toDateInput)
        toDateEditText.inputField.doneAccessory = true
        
        fromDateEditText.endEditing = { field in
            self.model.fromD = self.datePicker.date
            field.inputField.text = self.datePicker.date.toString()
            self.updateFields()
        }
        
        toDateEditText.endEditing = { field in
            self.model.toD = self.datePicker.date
            field.inputField.text = self.datePicker.date.toString()
            self.updateFields()
        }
        
        imageView.clipsToBounds = true
        separartorView.backgroundColor = theme.separatorGray
        separartorView.layer.cornerRadius = 0.5
        self.title = "Reservation"
        paymentMethod.backgroundColor = theme.inputFieldBackgroundColor
        
        actionButton.setTitle("Reserve".uppercased(), for: .normal)
        actionButton.layer.cornerRadius = 6
        actionButton.clipsToBounds = true
        actionButton.isEnabled = false
        actionButton.onClick(target: self, selector: #selector(onReserveAction))
        paymentMethod.onClick(target: self, selector: #selector(onClickCard))
        datePicker.addTarget(self, action: #selector(onDateChange(_:)), for: .valueChanged)
        self.showPrice(false)
        self.fromDateToDateLabel.adjustsFontSizeToFitWidth = true
    }
    
    @objc func onDateChange(_ sender: UIDatePicker) {
        print(sender.date.toString())
    }
    
    public override func initialize() {
        super.initialize()
        self.scrollView.addSubviews([
            imageView,
            parkingTitleLabel,
            fromDateEditText,
            toDateEditText,
            fromDateToDateLabel,
            totalDateLabel,
            totalAmountLabel,
            separartorView,
            actionButton,
            paymentMethod])
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var w: CGFloat = self.view.width
        var h: CGFloat = 200
        
        imageView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = imageView.maxY + 16
        h = 20
        x = 16
        w = w - 2 * x
        parkingTitleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = 16
        w = self.view.width / 2 - x - 4
        h = 60
        y = parkingTitleLabel.maxY + 16
        fromDateEditText.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = fromDateEditText.maxX + 8
        toDateEditText.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = fromDateEditText.maxY + 8
        x = 16
        h = 16
        w = fromDateEditText.width
        fromDateToDateLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = fromDateToDateLabel.maxY + 4
        totalDateLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = toDateEditText.x
        y = fromDateToDateLabel.y
        w = toDateEditText.width
        h = 36
        totalAmountLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = totalAmountLabel.maxY + 16
        x = 0
        h = 1
        w = self.view.width
        separartorView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = separartorView.maxY + 8
        x = 16
        h = 50
        w = self.view.width - 2 * x
        paymentMethod.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.paymentMethod.maxY + 24
        actionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        actionButton.addGradient(left: theme.buttonBackgroundGradientColor.left, right: theme.buttonBackgroundGradientColor.right)
        
        self.scrollView.frame = self.view.bounds
        self.scrollView.contentSize = CGSize(width: self.view.width, height: self.view.height + 30)
    }
    
    public func setPark(_ mapImage: UIImage, _ park: ParkingItemResponseModel) {
        self.imageView.image = mapImage
        self.model.parking = park
        self.parkingTitleLabel.text = park.title ?? "Parking Title"
    }
    
    private func showPrice(_ show: Bool) {
        self.fromDateToDateLabel.isHidden = !show
        self.totalDateLabel.isHidden = !show
        self.totalAmountLabel.isHidden = !show
        self.separartorView.isHidden = !show
        self.paymentMethod.isHidden = !show
        self.actionButton.isHidden = !show
        
    }
    
    internal func updateFields() {
        
        self.showPrice(self.model.isReady)
        if !self.model.isReady {
            return
        }
        self.fromDateToDateLabel.text = "\(self.model.fromDateMini) - \(self.model.toDateMini)"
        let nds = model.numberOfDays
        var ndss = "days"
        if nds == 1 {
            ndss = "day"
        }
        self.totalDateLabel.text = "\(nds) \(ndss)"
        self.totalAmountLabel.text = "$\(model.totalAmount)"
        
        if self.model.cardID == nil {
           actionButton.addGradient(left: theme.disabledButtonBackgroundGradientColor.left, right: theme.disabledButtonBackgroundGradientColor.right)
            actionButton.isEnabled = false
        } else {
            actionButton.addGradient(left: theme.buttonBackgroundGradientColor.left, right: theme.buttonBackgroundGradientColor.right)
            actionButton.isEnabled = true
        }
    }
    
    @objc func onClickCard() {
        let vc = CardListController()
        vc.onCardClick = { id in
            let card = CardManager.shared.getAll()[id]
            self.paymentMethod.setCard(card)
            self.model.cardID = card.id
            self.updateFields()
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onReserveAction() {
        self.showSpinner(text: "Payment on progress")
        self.model.reserve { (isOK, message) in
            self.removeSpinner()
            if !isOK {
                self.alertInfo(title: "Error", message: message ?? "")
            } else {
                self.alertInfo(title: "Success", message: "You have successfully ordered a place", handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
}
