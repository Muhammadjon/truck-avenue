//
//  ReservationModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 11/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ReservationModel {
    private let worker: MainApiWorker = MainApiWorker.shared
    public var fromD: Date?
    public var toD: Date?
    public var parking: ParkingItemResponseModel?
    public var fromDate: String {
        return fromD?.toString(format: "YYYY-MM-dd") ?? ""
    }
    
    public var toDate: String {
        return toD?.toString(format: "YYYY-MM-dd") ?? ""
    }
    
    public var fromDateMini: String {
        return fromD?.toString() ?? ""
    }
    
    public var toDateMini: String {
        return toD?.toString() ?? ""
    }
    
    public var numberOfDays: Int {
        if let f = fromD, let t = toD {
            let d1 = Calendar.current.startOfDay(for: f)
            let d2 = Calendar.current.startOfDay(for: t)
            let days = Calendar.current.dateComponents([.day], from: d1, to: d2)
            return days.day ?? 0
        }
        return 0
    }
    
    public var totalAmount: Int {
        if let prs = parking?.prices {
            return (numberOfDays * (prs.isEmpty ? 0 : Int(Float(prs[0].price ?? "0")!)))
        }
        return 0
    }
    
    public var cardID: Int?
    
    public var isReady: Bool {
        if let _ = fromD, let _ = toD {
            if numberOfDays <= 0 {
                
                return false
            }
            return true
        }
        return false
    }
    
    public init() {
        
    }
    
    public func reserve(_ completion: @escaping ((Bool, String?) -> Void)) {
        
        if isReady {
            worker.getParkingOrderId(parkingId: parking?.id ?? -1, fromDate: fromDate, toDate: toDate) { (isOK, id) in
                if isOK {
                    self.worker.payToOrder(order: id, from: self.cardID!, completion: { (model) in
                        HistoryManager.shared.shouldUpdate = true
                        completion(!ApplicationNetwork.StatusDetector.hasError, ApplicationNetwork.StatusDetector.errorMessage)
                    })
                } else {
                    completion(ApplicationNetwork.StatusDetector.hasError, ApplicationNetwork.StatusDetector.errorMessage)
                }
            }
        } else {
            completion(false, "You did not fill all blanks")
        }
    }
}
