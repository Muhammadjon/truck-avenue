//
//  GalleryTableViewCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 15/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class GalleryTableViewCell: UITableViewCell {
    
    public var galleryLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        return layout
    }()

    
    public var galleryCollectionView: UICollectionView {
        
        if let view = self.contentView.contains(tag: 101) {
            return view as! UICollectionView
        }
        
        let f = CGRect(x: 2, y: 2, width: self.width, height: self.height)
        
        self.galleryLayout.itemSize = CGSize(width: f.height, height: f.height)
        
        let c = UICollectionView(frame: f, collectionViewLayout: self.galleryLayout)
        c.tag = 101
        c.backgroundColor = theme.appWhite
        c.register(PhotoCell.self, forCellWithReuseIdentifier: self.cellIdentifier)
        c.showsHorizontalScrollIndicator = false
        c.delegate = self
        c.dataSource = self
    
        self.addSubview(c)
        return c
    }

    private let cellIdentifier: String = "cell_identifier"
    public var photos: [String] = []
    public var onPhotoClick: ((Int) -> Void)?
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = theme.appWhite
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented method")
    }
    
    private func initialize() {
        
    }
    
    public func setPhotos(_ photos: [String]) {
        self.photos = photos
        self.galleryCollectionView.reloadData()
    }
}

extension GalleryTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! PhotoCell
        cell.photoView.download(urlString: self.photos[indexPath.row], placeholder: #imageLiteral(resourceName: "no_imagephoto_icon"))
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let fun = self.onPhotoClick {
            fun(indexPath.row)
        }
    }
}
