//
//  PhotoItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 15/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class PhotoCell: UICollectionViewCell {
    public let photoView: UIImageView = UIImageView()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        photoView.image = #imageLiteral(resourceName: "no_imagephoto_icon")
        photoView.contentMode = .scaleAspectFill
        photoView.clipsToBounds = true
        photoView.layer.cornerRadius = 4
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 4
        self.addSubview(photoView)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented method")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let x: CGFloat = 4
        let y: CGFloat = 4
        let h: CGFloat = (self.contentView.height < self.contentView.width ? self.contentView.height : self.contentView.width) - 8
        let w: CGFloat = h
        photoView.frame = CGRect(x: x, y: y, width: w, height: h)
    }
}
