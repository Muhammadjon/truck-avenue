//
//  SimpleTableViewCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 12/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class SimpleTableViewCell: UITableViewCell {
    public let titleLabel: UILabel = UILabel()
    public let descriptionLabel: UILabel = UILabel()
    public let extraTitleLabel: UILabel = UILabel()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        titleLabel.text = "Title"
        descriptionLabel.text = "Description Description Description Description Description"
        extraTitleLabel.text = "(12)"
        
        titleLabel.font = theme.appFontBold.withSize(14)
        titleLabel.textColor = theme.appBlack
        titleLabel.sizeToFit()
        
        descriptionLabel.font = theme.appFont.withSize(12)
        descriptionLabel.sizeToFit()
        descriptionLabel.textColor = theme.labelGray
        descriptionLabel.numberOfLines = 2
        
        extraTitleLabel.textColor = theme.labelGray
        extraTitleLabel.font = theme.appFontBold.withSize(14)
        extraTitleLabel.sizeToFit()
        
        self.contentView.addSubviews([titleLabel, descriptionLabel, extraTitleLabel])
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.leading.equalTo(8)
            make.width.equalTo(self.contentView.width / 2 - 8).priority(10)
            make.height.equalTo(16)
        }
        
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.equalTo(8)
            make.trailing.equalTo(8)
            make.bottom.equalTo(8)
        }
        
        self.extraTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel)
            make.leading.equalTo(titleLabel.snp.trailing).offset(8)
            make.width.equalTo(self.contentView.width / 3)
            make.height.equalTo(titleLabel)
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    public func setModel(_ title: String, _ description: String) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description
        self.extraTitleLabel.text = ""
    }
}
