//
//  ReviewTableViewCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 12/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ReviewTableViewCell: UITableViewCell {
    private let titleLabel: UILabel = UILabel()
    private let descriptionTitleLabel: UILabel = UILabel()
    private let desTitleRightRateView: RateView = RateView(5)
    private let descriptionLabel: UILabel = UILabel()
    private let extraTitleLabel: UILabel = UILabel()
    private let allButton: UIButton = UIButton()
    
    public var onAllReviewClick: (() -> Void)?
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        titleLabel.text = "Reviews".uppercased()
        descriptionTitleLabel.text = "Muhammadjon Tohirov 20.04.2019"
        descriptionLabel.text = "Description Description Description Description Description, Description Description Description"
        extraTitleLabel.text = ""
        desTitleRightRateView.updateRate(rate: 3)
        
        titleLabel.font = theme.appFontBold.withSize(14)
        titleLabel.textColor = theme.appBlack
        titleLabel.sizeToFit()

        descriptionTitleLabel.font = theme.appFont.withSize(12)
        descriptionTitleLabel.sizeToFit()
        descriptionTitleLabel.textColor = theme.labelGray
        descriptionTitleLabel.numberOfLines = 1

        descriptionLabel.font = theme.appFont.withSize(12)
        descriptionLabel.sizeToFit()
        descriptionLabel.textColor = theme.labelGray
        descriptionLabel.numberOfLines = 2
        
        extraTitleLabel.textColor = theme.labelGray
        extraTitleLabel.font = theme.appFontBold.withSize(14)
        extraTitleLabel.sizeToFit()
        
        allButton.titleLabel?.textAlignment = .left
        allButton.titleLabel?.font = theme.appFont
        allButton.setTitleColor(theme.bigBlueTextColor, for: .normal)
        allButton.setTitle("All reviews".uppercased(), for: .normal)
        allButton.onClick(target: self, selector: #selector(onAllClick))
        
        self.contentView.addSubviews([titleLabel, descriptionLabel, extraTitleLabel, descriptionTitleLabel, desTitleRightRateView, allButton])
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.leading.equalTo(8)
            make.width.equalTo(self.contentView.width / 2 - 8).priority(10)
            make.height.equalTo(16)
        }
        
        self.descriptionTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.equalTo(8)
            make.trailing.equalTo(8)
            make.height.equalTo(16)
        }
        
        self.desTitleRightRateView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.width.equalTo(100)
            make.trailing.equalTo(descriptionTitleLabel).offset(-8)
            make.height.equalTo(10)
        }
        
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(descriptionTitleLabel.snp.bottom).offset(4)
            make.leading.equalTo(8)
            make.trailing.equalTo(8)
            make.bottom.equalTo(-20).priority(1)
        }
        
        self.extraTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel)
            make.leading.equalTo(titleLabel.snp.trailing).offset(8)
            make.width.equalTo(self.contentView.width / 3)
            make.height.equalTo(titleLabel)
        }
        
        self.allButton.snp.makeConstraints { (make) in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(4).priority(50)
            make.bottom.equalTo(-4).priority(1000)
            make.leading.equalTo(descriptionLabel)
            make.height.equalTo(16).priority(49)
            make.width.equalTo((self.allButton.titleLabel?.text ?? "").width(withConstrainedHeight: 16, font: theme.appFont) + 20)
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    @objc func onAllClick() {
        self.onAllReviewClick?()
    }
    
    public func setModel(_ title: String, _ description: String, rate: Int) {
        self.titleLabel.text = "Reviews".uppercased()
        self.descriptionTitleLabel.text = title
        self.descriptionLabel.text = description
        self.desTitleRightRateView.updateRate(rate: rate)
    }
}


