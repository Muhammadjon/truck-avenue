//
//  ParkingViewModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 15/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkingViewModel {
    private let worker: MainApiWorker = MainApiWorker.shared
    
    public func makeFavorit(_ id: Int, completion: @escaping ((ParkingFavoriteResponseModel?) -> Void)) {
        worker.makeParkingFavorite(parkingId: id) { (model) in
            completion(model)
        }
    }
    
    public func deleteFavorit(favoriteId id: Int, completion: @escaping ((Bool) -> Void)) {
        worker.deleteFavorite(favoritID: id, completion)
    }
    
}
