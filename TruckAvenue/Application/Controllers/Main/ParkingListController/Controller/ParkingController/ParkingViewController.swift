//
//  ParkingViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ParkingViewController: BaseViewController {
    private let favoritImageView: UIImageView = UIImageView()
    public let imageView: UIImageView = UIImageView()
    private let tableView: UITableView = UITableView()
    private let simpleCellIdentifier = "SimpleCellIdentifier"
    private let parkingCellIdentifier = "ParkingCellIdentifier"
    private let reviewCellIdentifier = "ReviewCellIdentifier"
    private let galleryCellIdentifier = "GalleryCellIdentifier"
    public var parkingItem: ParkingItemResponseModel?
    public var parkingId: Int?
    private let actionButton: UIButton = UIButton()
    private let footerView: UIView = UIView()
    fileprivate let model: ParkingViewModel = ParkingViewModel()
    public var onReviewClick: ((_ vc: ReviewController) -> Void)?
    public var onReserve: ((_ vc: ReservationController) -> Void)?
    
    private var isFavorit: Bool = false
    
    public init(_ id: Int) {
        super.init(nibName: nil, bundle: nil)
        self.parkingId = id
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        guard let id = self.parkingId else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.parkingItem = MapViewModel.shared.find(parking: id)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.separatorColor = theme.separatorGray
        favoritImageView.image = #imageLiteral(resourceName: "star_full").tintColor(color: theme.separatorGray)
        
        favoritImageView.onClick(target: self, selector: #selector(onFavorit))
        favoritImageView.contentMode = .scaleAspectFit
        footerView.height = 65
        footerView.backgroundColor = theme.appWhite
        actionButton.setTitleColor(theme.appWhite, for: .normal)
        footerView.addShadow(shadowColor: theme.appShadow, radius: 1, width: 0, height: 3, opacity: 0.4)
        footerView.addSubview(actionButton)
        
        actionButton.setTitle("Reserve".uppercased(), for: .normal)
        actionButton.titleLabel?.font = theme.appFont.withSize(15)
        actionButton.layer.cornerRadius = 4
        actionButton.clipsToBounds = true
        
        tableView.tableFooterView = footerView
        
        tableView.register(SimpleTableViewCell.self, forCellReuseIdentifier: simpleCellIdentifier)
        tableView.register(ParkItemCell.self, forCellReuseIdentifier: parkingCellIdentifier)
        tableView.register(ReviewTableViewCell.self, forCellReuseIdentifier: reviewCellIdentifier)
        tableView.register(GalleryTableViewCell.self, forCellReuseIdentifier: galleryCellIdentifier)
        
        self.scrollView.backgroundColor = theme.backgroudColor
        self.scrollView.addSubviews([imageView, tableView, favoritImageView])
        self.scrollView.addSubview(favoritImageView)
        
        self.actionButton.addTarget(self, action: #selector(reservationAction), for: .touchUpInside)
        
        self.isFavorit = parkingItem?.favorite?.is_favorite ?? false
        
        if self.isFavorit {
            self.favoritImageView.image =  #imageLiteral(resourceName: "star_full").tintColor(color: theme.bigBlueTextColor)
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.parkingItem = MapViewModel.shared.find(parking: parkingId!)
        
        if let _ = self.navigationController {
            
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationBar.prefersLargeTitles = false
                self.navigationItem.largeTitleDisplayMode = .never
            } else {
                
            }
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var w: CGFloat = self.view.width
        var h: CGFloat = 200
        self.imageView.frame = CGRect(x: x, y: y, width: w, height: h)

        
        x = 16
        y = 16
        self.favoritImageView.frame = CGRect(x: x, y: 16, width: 24, height: 24)
        
        x = 0
        y = imageView.maxY
        h = self.view.height - y
        self.tableView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        self.scrollView.frame = self.view.bounds
        self.scrollView.contentSize = CGSize(width: self.view.width, height: tableView.maxY + imageView.height + 70)
        self.tableView.height = self.scrollView.contentSize.height - self.imageView.height + 40
        self.tableView.backgroundColor = theme.backgroudColor
        
        x = 16
        w = w - 2 * x
        h = 50
        self.actionButton.frame = CGRect(x: x, y: 5, width: w, height: h)
        actionButton.addGradient(left: theme.buttonBackgroundGradientColor.left, right: theme.buttonBackgroundGradientColor.right)
        
    }
    
    @objc private func onFavorit() {
        guard let id = self.parkingItem?.id else {
            return
        }
        
        func onSuccessFinish() {
            self.toggleStar()
        }
        
        func makeFavorit() {
            model.makeFavorit(id) { (fmodel) in
                if let fm = fmodel {
                    MapViewModel.shared.find(parking: id)?.favorite?.id = fm.id
                    MapViewModel.shared.find(parking: id)?.favorite?.is_favorite = true
                    self.parkingItem?.favorite?.id = fm.id
                    self.parkingItem?.favorite?.is_favorite = true
                    onSuccessFinish()
                }
            }
        }
        
        func deFavorit() {
            guard let fid = self.parkingItem?.favorite?.id else {
                return
            }
            model.deleteFavorit(favoriteId: fid) { isOK in
                if isOK {
                    MapViewModel.shared.find(parking: id)?.favorite?.id = nil
                    MapViewModel.shared.find(parking: id)?.favorite?.is_favorite = false
                    self.parkingItem?.favorite?.id = nil
                    self.parkingItem?.favorite?.is_favorite = false
                    onSuccessFinish()
                }
            }
        }
        
        
        if isFavorit {
            deFavorit()
        } else {
            makeFavorit()
        }
    }
    
    fileprivate func toggleStar() {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = 0.0
        
        var toValue = CGFloat(Double.pi / 2)
        if isFavorit {
            self.favoritImageView.image = #imageLiteral(resourceName: "star_full").tintColor(color: theme.separatorGray)
            toValue = CGFloat(-Double.pi)
            
        } else {
            self.favoritImageView.image = #imageLiteral(resourceName: "star_full").tintColor(color: theme.bigBlueTextColor)
        }
        
        isFavorit = !isFavorit
        
        animation.toValue = toValue
        animation.duration = 0.33
        
        favoritImageView.layer.add(animation, forKey: nil)
    }
    
    @objc private func reservationAction() {
        let vc = ReservationController()
        self.onReserve?(vc)
        vc.setPark(self.imageView.image!, parkingItem!)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    public func setImage(_ img: UIImage) {
        self.imageView.image = img
    }
}

extension ParkingViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: parkingCellIdentifier, for: indexPath) as! ParkItemCell
            cell.selectionStyle = .none
            let inp = ParkItemInput()
            inp.rate = Int(parkingItem?.starsLen ?? 0)
            inp.rateLen = Int(parkingItem?.stars ?? 0)
            inp.title = parkingItem?.title ?? ""
            inp.description = parkingItem?.parkingDescription ?? ""
            cell.setModel(inp)
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: galleryCellIdentifier, for: indexPath) as! GalleryTableViewCell
            var photos: [String] = []
            let ps = parkingItem?.gallery
            ps?.forEach({ (p) in
                photos.append(p.photoURL)
            })
            cell.setPhotos(photos)
            cell.onPhotoClick = { id in
                let photo = photos[id]
                let vc = ImageViewerController()
                vc.setImage(url: photo)
                self.present(vc, animated: true, completion: {
                    logger.log(message: "presented")
                })
            }
            return cell
        }
        
        if indexPath.row == 2 {
            // Reviews
            let cell = tableView.dequeueReusableCell(withIdentifier: reviewCellIdentifier, for: indexPath) as! ReviewTableViewCell
            cell.selectionStyle = .none
            let comment = parkingItem?.comment
            let name = (comment?.user?.username ?? "")
            let date = (comment?.readableDate ?? "")
            let title = "\(name) \(date)"
            cell.setModel(title, parkingItem?.comment?.comment ?? "", rate: parkingItem?.comment?.rate ?? 1)
            
            cell.onAllReviewClick = {
                if let parking = self.parkingItem {
                    let vc = ReviewController()
                    vc.setPark(parking)
                    self.onReviewClick?(vc)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: simpleCellIdentifier, for: indexPath) as! SimpleTableViewCell
        cell.selectionStyle = .none
        switch indexPath.row {
        case 3:
            cell.setModel("Work Hours".uppercased(), parkingItem?.workingTime ?? "")
            break
        case 4:
            if parkingItem?.prices?.count ?? 0 == 0 {
                cell.setModel("Price".uppercased(), "no price")
                break
            }
            
            var prices: String = ""
            parkingItem?.prices?.forEach({ (price) in
                prices = prices + " $\(price.price ?? "0"), "
            })
            prices.removeLast(2)
            cell.setModel("Price".uppercased(), prices)
            break
        case 5:
            cell.setModel("Free Places".uppercased(), "\(parkingItem?.freePlaces ?? 0) available places".uppercased())
            cell.descriptionLabel.textColor = theme.appOrange
            break
        default:
            break
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        }
        
        if indexPath.row == 1 {
            if parkingItem?.gallery?.count == 0 {
                return 0
            }
            return 150
        }
        
        if indexPath.row == 2 {
            if (parkingItem?.comment) != nil {
                return 120
            }
            return 50
        }
        
        return 60
    }
}
