//
//  ReviewItemInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ReviewItemInput {
    public var title: String = ""
    public var date: String = ""
    public var rate: Int = 0
    public var username: String = ""
    public var description: String = ""
    public var phoneNumber: String = ""
    public init(title: String, date: String, rate: Int, username: String, description: String) {
        self.title = title
        self.date = date
        self.rate = rate
        self.username = username
        self.description = description
    }
    
    public init() {
        
    }
}
