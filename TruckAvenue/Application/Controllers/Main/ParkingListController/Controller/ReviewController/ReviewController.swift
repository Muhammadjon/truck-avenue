//
//  ReviewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ReviewController: BaseViewController {
    private let tableView: UITableView = UITableView()
    private let model: ReviewModel = ReviewModel()
    private var parkId: Int = -1
    private var rate: Int = 0
    private var commentString: String = ""
    private var ownReview: ReviewItemInput?
    
    public override func initializeData() {
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
    }
    
    public override func initialize() {
        super.initialize()
        self.addSubview(tableView)
        if parkId == -1 {
            return
        }
        self.showSpinner(text: "Please wait")
        
        self.title = "Reviews"
        
        model.loadComments(parkId) { errorMessage in
            self.removeSpinner()
//            if ApplicationNetwork.StatusDetector.hasError {
//                let msg = errorMessage ?? ""
//                self.alertInfo(title: "Error", message: msg)
//                return
//            }
            self.model.reviews.forEach({ (r) in
                if r.phoneNumber == UserPreferences.login {
                    self.ownReview = r
                }
            })
            self.tableView.reloadData()
        }
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.frame = self.view.bounds
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    public func setPark(_ park: ParkingItemResponseModel) {
        self.parkId = park.id ?? -1
    }
    
    private func isReady() -> Bool {
        
        return false
    }
}

extension ReviewController: UITableViewDelegate, UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.reviews.count + 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_review") as? ReviewWriteCell ?? ReviewWriteCell(style: .default, reuseIdentifier: "cell_review")
            
            cell.setReview(self.ownReview)
            
            cell.onComment = { star, comment, isOK in
                if isOK {
                    self.model.comment(id: self.parkId, comment: comment, rate: star, { (errorMessage) in
                        if ApplicationNetwork.StatusDetector.hasError {
                            self.alertInfo(title: "Error", message: "Please check your inputs, and don't forget to put star")
                        } else {
                            self.tableView.reloadData()
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                } else {
                    self.alertInfo(title: "Missing", message: "You did not fill all blanks")
                }
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ReviewItemCell ?? ReviewItemCell(style: .default, reuseIdentifier: "cell")
        cell.setModel(model.reviews[indexPath.row - 1])
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }
        return 100
    }
}
