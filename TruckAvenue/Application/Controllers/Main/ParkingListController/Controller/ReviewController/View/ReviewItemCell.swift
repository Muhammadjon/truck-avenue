//
//  ReviewItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ReviewItemCell: UITableViewCell {
    private let titleLabel: UILabel = UILabel()
    private let dateLabel: UILabel = UILabel()
    private let usernameLabel: UILabel = UILabel()
    private let starsView: RateView = RateView(5)
    private let descriptionLabel: UILabel = UILabel()
    
    private var input: ReviewItemInput?
    
    private let bgView: UIView = UIView()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        dateLabel.textAlignment = .right
        usernameLabel.textAlignment = .right
        
        titleLabel.font = theme.appFont.withSize(12)
        dateLabel.font = theme.appFont.withSize(12)
        usernameLabel.font = theme.appFont.withSize(10)
        descriptionLabel.font = theme.appFont.withSize(10)
        descriptionLabel.numberOfLines = 0
        self.selectionStyle = .none
        titleLabel.textColor = theme.appBlack
        descriptionLabel.textColor = theme.labelGray
        
        bgView.backgroundColor = theme.backgroudColor
        bgView.layer.cornerRadius = 6
        
        self.addSubview(bgView)
        self.bgView.addSubviews([titleLabel, dateLabel, usernameLabel, starsView, descriptionLabel])
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public func setModel(_ input: ReviewItemInput) {
        self.input = input
        var title = input.title
        if input.title.count > 10 {
            title = input.title[0..<10]
        }
        
        self.titleLabel.text = title
        self.usernameLabel.text = input.username
        self.descriptionLabel.text = input.description
        self.dateLabel.text = input.date
        self.starsView.updateRate(rate: input.rate)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        bgView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.bottom.equalTo(-4).priority(999)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(4)
            make.leading.equalTo(4)
            make.height.equalTo(16)
            make.width.equalTo(titleLabel.widthByText(height: 12))
        }
        
        starsView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.leading.equalTo(titleLabel.snp.leading)
            make.width.equalTo(100)
            make.height.equalTo(10)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-4)
            make.top.equalTo(titleLabel.snp.top)
            make.height.equalTo(10)
            make.width.equalTo(dateLabel.widthByText(height: 12))
        }
        
        usernameLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(dateLabel.snp.trailing)
            make.top.equalTo(dateLabel.snp.bottom).offset(4)
            make.height.equalTo(10)
            make.width.equalTo(usernameLabel.widthByText(height: 10))
        }
        
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(starsView.snp.bottom)
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(-4)
            make.bottom.equalTo(0)
        }
        descriptionLabel.sizeToFit()
    }
}
