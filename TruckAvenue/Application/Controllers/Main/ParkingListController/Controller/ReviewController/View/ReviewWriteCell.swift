//
//  ReviewCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ReviewWriteCell: UITableViewCell {
    private let bgView: UIView = UIView()
    private let starView: RateView = RateView(5, rateable: true)
    private let starViewInfoLabel: UILabel = UILabel()
    private let commentField: UITextView = UITextView()
    private let commentButton: UIButton = UIButton()
    
    private let placeholder = "Please write your opinion about the parking and we will review it to improve the service (maxium 255 characters)"
    
    private var star: Int = 0
    private var comment: String = ""
    
    public var onComment: ((_ star: Int, _ comment: String, _ isOK: Bool) -> Void)?

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(bgView)
        self.selectionStyle = .none
        starViewInfoLabel.text = "Tap to star to rate"
        starViewInfoLabel.font = theme.appFont.withSize(10)
        
        commentButton.setTitle("Comment", for: .normal)
        commentButton.titleLabel?.font = theme.appFont.withSize(12)
        starViewInfoLabel.textAlignment = .center
        bgView.layer.cornerRadius = 6
        bgView.backgroundColor = theme.backgroudColor
        
        commentField.text = placeholder
        commentField.textColor = theme.labelGray
        commentField.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        commentField.layer.cornerRadius = 6
        commentField.delegate = self
        
        commentField.font = theme.appFont.withSize(12)
        commentField.layer.borderColor = theme.labelGray.cgColor
        commentButton.setTitleColor(theme.bigBlueTextColor, for: .normal)
        
        starView.didStarSelected = { s in
            self.star = s
        }
        
        commentButton.onClick(target: self, selector: #selector(onCommentClick))
        
        self.bgView.addSubviews([starView, starViewInfoLabel, commentField, commentButton])
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()

        bgView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.bottom.equalTo(-4).priority(999)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        self.starView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.width.equalTo(120)
            make.height.equalTo(15)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        self.starViewInfoLabel.snp.makeConstraints { (make) in
            make.top.equalTo(starView.snp.bottom).offset(4)
            make.height.equalTo(10)
            make.width.equalTo(starViewInfoLabel.widthByText(height: 10))
            make.centerX.equalTo(self.snp.centerX)
        }
        
        self.commentField.snp.makeConstraints { (make) in
            make.top.equalTo(self.starViewInfoLabel.snp.bottom).offset(4)
            make.leading.equalTo(4)
            make.centerX.equalTo(self.snp.centerX)
            make.bottom.equalTo(self.commentButton.snp.top).offset(-4)
        }
        
        self.commentButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.commentField.snp.bottom).offset(4)
            make.width.equalTo(120)
            make.height.equalTo(30)
            make.bottom.equalTo(-4)
            make.centerX.equalTo(self.snp.centerX)
        }
    }
    
    public func setReview(_ review: ReviewItemInput?) {
        if let r = review {
            self.starView.updateRate(rate: r.rate)
            self.commentField.text = r.description
            self.starView.rateable = false
            self.commentField.isEditable = false
            self.commentButton.isEnabled = false
            self.commentButton.setTitle("Commented", for: .normal)
            self.commentButton.setTitleColor(theme.labelGray, for: .normal)
        }
    }
    
    @objc func onCommentClick() {
        if self.star == 0 || self.comment.isEmpty {
            self.onComment?(self.star, self.comment, false)
        }
        
        self.onComment?(self.star, self.comment, true)
    }
}

extension ReviewWriteCell: UITextViewDelegate {
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView.textColor == theme.labelGray {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0.1176470588, green: 0.1176470588, blue: 0.1176470588, alpha: 1)
            return true
        }
        
        return true
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = self.placeholder
            textView.textColor = theme.labelGray
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n" || text.count == 255) {
            textView.resignFirstResponder()
            return false
        }
        
        self.comment = "\(textView.text ?? "")\(text)"
        
        return true
    }
}
