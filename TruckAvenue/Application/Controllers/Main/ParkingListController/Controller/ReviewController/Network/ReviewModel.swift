//
//  ReviewNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ReviewModel {
    private let worker = MainApiWorker.shared
    public var reviews: [ReviewItemInput] = []
    
    public func loadComments(_ id: Int, _ completion: @escaping ((_ errorMessage: String?) -> Void)) {
        worker.loadComentsOf(park: id) { (model) in
            self.reviews.removeAll()
            
            model?.comments?.forEach({ (comment) in
                let date = comment.created?.getDateAndTimeAsString() ?? ("01.06.2019", "00:00")
                let review = ReviewItemInput(title: (comment.comment ?? ""), date: "\(date.1) \(date.0)", rate: comment.rate ?? 1, username: comment.user?.username ?? "", description: comment.comment ?? "")
                review.phoneNumber = comment.user?.phoneNumber ?? ""
                self.reviews.append(review)
            })
            
            completion(ApplicationNetwork.StatusDetector.errorMessage)
        }
    }
    
    public func comment(id: Int, comment: String, rate: Int, _ completion: @escaping ((_ errorMessage: String?) -> Void)) {
        worker.comment(id: id, comment: comment, rate: rate) { (model) in
            if let m = model {
                MapViewModel.shared.getParkingList({ (_) in
                    let date = m.created?.getDateAndTimeAsString() ?? ("01.06.2019", "00:00")
                    let review = ReviewItemInput(title: (m.comment ?? ""), date: "\(date.1) \(date.0)", rate: m.rate ?? 1, username: m.user?.username ?? "", description: m.comment ?? "")
                    review.phoneNumber = m.user?.phoneNumber ?? ""
                    self.reviews.append(review)
                    
                    completion(nil)
                })
                return
            }
            let errm = ApplicationNetwork.StatusDetector.errorMessage
            let errh = ApplicationNetwork.StatusDetector.hasError
            
            completion(errh ? errm : nil)
        }
    }
}
