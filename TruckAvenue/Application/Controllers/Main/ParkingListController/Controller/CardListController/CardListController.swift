//
//  File.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class CardListController: BaseViewController {
    private let tableView: UITableView = UITableView()
    public var onCardClick: ((_ id: Int) -> Void)?
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.leading.equalTo(0)
            make.trailing.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
            self.navigationItem.largeTitleDisplayMode = .never
            self.definesPresentationContext = true
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension CardListController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CardManager.shared.items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CardListItemCell ?? CardListItemCell(style: .default, reuseIdentifier: "cell")
        
        let item = CardManager.shared.getAll()[indexPath.row]
        cell.setModel(item)
        return cell

    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        logger.log(message: "card \(indexPath.row)")
        self.onCardClick?(indexPath.row)
        self.navigationController?.popViewController(animated: true)
    }
}

