//
//  ParkListInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkListInput {
    
    /// items list
    public var items: [ParkItemInput] = []
    
    public init() {
        
    }
}
