//
//  ParkItemInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkItemInput: BaseTableViewItemInputModel {
    /// parking id
    public var id: Int = -1
    
    /// parking title
    public var title: String = ""
    
    /// parking description
    public var description: String = ""
    
    /// parking rate
    public var rate: Int = 0
    
    /// number of user rated the parking
    public var rateLen: Int = 0
}
