//
//  ParkingListViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import MapKit

public class ParkingListViewController: BaseTableViewController<ParkItemInput, ParkItemCell>, UISearchBarDelegate {
    private var searchController: UISearchController?
    private let model: ParkingListViewModel = ParkingListViewModel()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        model.loadInput()
        self.items = model.input.items
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchBar.tintColor = UIColor.black
        self.title = "Parking List"
        
        searchController?.searchBar.searchBarStyle = .default
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        searchController?.dimsBackgroundDuringPresentation = false
        
        searchController?.searchBar.delegate = self
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .always
            self.navigationItem.searchController = searchController
            self.navigationItem.hidesSearchBarWhenScrolling = true
            self.definesPresentationContext = true
        } else {
            self.tableView.tableHeaderView = self.searchController?.searchBar
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    @objc private func onKeyboardShow(_ notification: Notification) {
        appDelegate.route.clearStatusBar()
    }
    
    @objc private func onKeyboardHide(_ notification: Notification) {
        appDelegate.route.fillStatusBar()
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.model.filter(searchText)
        if searchText.isEmpty {
            self.items = model.allItemsInput.items
        } else {
            self.items = model.input.items
        }
        self.tableView.reloadData()
    }
}

extension ParkingListViewController {
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = model.input.items[indexPath.row]
        let parkingItem = MapViewModel.shared.find(parking: item.id)
        if let pi = parkingItem {
            let vc = ParkingViewController(pi.id ?? 0)
            self.showSpinner(text: "Please wait ...")
            MKMapSnapshotter.mapImage(longitude: pi.longitude ?? "", lattitude: pi.latitude ?? "", width: self.view.width, height: 200, pinImage: #imageLiteral(resourceName: "map_pin")) { (image) in
                if let img = image {
                    vc.setImage(img)
                }
                self.removeSpinner()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
