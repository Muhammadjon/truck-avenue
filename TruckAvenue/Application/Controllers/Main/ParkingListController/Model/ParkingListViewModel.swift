//
//  ParkingListViewModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkingListViewModel {
    public var input: ParkListInput = ParkListInput()
    public private(set) var allItemsInput: ParkListInput = ParkListInput()
    public init() {
        
    }
    
    public func loadInput() {
        let list = MapViewModel.shared.parkingList
        list.forEach { (item) in
            let inp = ParkItemInput()
            inp.title = item.title ?? ""
            inp.rate = Int(item.starsLen ?? 0)
            inp.rateLen = Int(item.stars ?? 0)
            inp.id = item.id ?? -1
            inp.description = item.parkingDescription ?? "No description"
            input.items.append(inp)
            allItemsInput.items.append(inp)
        }
    }
    
    public func filter(_ key: String) {
        self.input.items = allItemsInput.items.filter { (item) -> Bool in
            let hasInDescription: Bool = item.description.contains(key)
            let hasInTitle: Bool = item.title.contains(key)
            return hasInDescription || hasInTitle
        }
    }
}
