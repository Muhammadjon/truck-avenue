//
//  ParkItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ParkItemCell: BaseTableViewItemCell<ParkItemInput> {
    private let titleLabel: UILabel = UILabel()
    private let descrionLabel: UILabel = UILabel()
    private let rateView: RateView = RateView(5)
    private let reviewLabel: UILabel = UILabel()
    private let theme: Theme = ThemeManager.shared.currentTheme()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        titleLabel.font = theme.appFontBold.withSize(14)
        descrionLabel.font = theme.appFont.withSize(12)
        reviewLabel.font = theme.appFont.withSize(10)
        descrionLabel.numberOfLines = 3
        reviewLabel.text = "0"
        
        self.addSubviews([titleLabel, descrionLabel, rateView, reviewLabel])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let calculator = SizeCalculator.shared
        var x: CGFloat = 8
        var y: CGFloat = 4
        var w: CGFloat = self.contentView.width
        var h: CGFloat = calculator.height(from: 20)
        titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = titleLabel.maxY + 4
        h = self.contentView.height - y - 22
        w = self.contentView.width - 2 * x
        descrionLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = reviewLabel.widthByText(height: h)
        x = self.contentView.width - 10 - w
        h = calculator.height(from: 20)
        y = self.contentView.height - 22
        reviewLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = calculator.width(from: 100)
        h = 20
        x = contentView.width - w - reviewLabel.width - 8
        
        rateView.frame = CGRect(x: x, y: y, width: w, height: h)
        descrionLabel.sizeToFit()
    }
    
    public override func setModel(_ model: ParkItemInput) {
        self.titleLabel.text = model.title
        self.descrionLabel.text = model.description
        self.rateView.updateRate(rate: model.rate)
        self.reviewLabel.text = "\(model.rateLen)"
    }
}
