//
//  PaymentMethodItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 17/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class PaymentMethodItemCell: UITableViewCell {
    
    private let cardTitleLabel: UILabel = UILabel()
    private let cardNumberLabel: UILabel = UILabel()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    private func initialize() {
        self.contentView.addSubviews([cardTitleLabel, cardNumberLabel])
        
        self.cardTitleLabel.font = theme.appFont.withSize(16)
        self.cardTitleLabel.textAlignment = .left
        
        self.cardNumberLabel.font = theme.appFont.withSize(14)
        self.cardNumberLabel.textColor = theme.labelGray
        self.cardNumberLabel.textAlignment = .right
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        var x: CGFloat = 8
        let h: CGFloat = 20
        let y: CGFloat = (self.contentView.height - h) / 2
        let w: CGFloat = (self.contentView.width - 2 * x) / 2
        
        cardTitleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = self.contentView.width - x - w
        
        cardNumberLabel.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    public func setInput(title: String, cardNumber: String) {
        self.cardTitleLabel.text = title
        self.cardNumberLabel.text = cardNumber
    }
}
