//
//  MapView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 30/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import MapKit

public class MapView: UIView, MKMapViewDelegate, CLLocationManagerDelegate {
    
    private var mpView: MKMapView = MKMapView()
    
    private var locationManager: CurrentLocationManager = CurrentLocationManager.shared
    
    private var currentLocationFocused: Bool = false
    
    public var input: MapViewInput = MapViewInput()
    
    public var didSelectPin: ((ParkingAnnotation) -> Void)?
    
    public var activeAnnotation: ParkingAnnotation? {
        return input.activeAnnotation
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        mpView.mapType = .standard
        mpView.delegate = self
        self.locationManager.delegate = self
        self.addSubview(mpView)
        
        self.mpView.showsUserLocation = true
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        mpView.frame = self.bounds
    }

    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let _ = annotation as? ParkingAnnotation {
            let view = MKAnnotationView()
            view.image = #imageLiteral(resourceName: "map_pin")
            return view
        }
        
        return nil
    }
    
    public func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let ann = view.annotation as? ParkingAnnotation {
            view.image = #imageLiteral(resourceName: "map_pin_selected")
            self.focus(to: ann)
            self.input.activeAnnotation = ann
            self.didSelectPin?(ann)
        }
    }
    
    public func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if let _ = view.annotation as? ParkingAnnotation {
            view.image = #imageLiteral(resourceName: "map_pin")
            self.input.activeAnnotation = nil
        }
    }
    
    public func deselectPin() {
        self.mpView.deselectAnnotation(self.input.activeAnnotation, animated: true)
    }
    
    public func addPin(_ id: Int, _ lon: Double, _ lat: Double, _ title: String) {
        let location = CLLocationCoordinate2D(latitude: lat,longitude: lon)
        
        let annotation = ParkingAnnotation()
        
        annotation.coordinate = location
        annotation.title = title
        annotation.id = id
        self.input.addAnnotation(annotation)
        mpView.addAnnotation(annotation)
    }
    
    public func addPin(_ itemModel: ParkingItemResponseModel) {
        let lon = Double(itemModel.longitude ?? "0") ?? 0
        let lat = Double(itemModel.latitude ?? "0") ?? 0
        let title = itemModel.title ?? ""
        self.addPin(itemModel.id ?? 0, lon, lat, title)
    }
    
    public func setCurrentLocation() {
        if !currentLocationFocused {
            self.findMyLocation()
            self.currentLocationFocused = true
        }
    }
    
    public func focus(to annotation: MKAnnotation) {
        let lat = annotation.coordinate.latitude
        let lon = annotation.coordinate.longitude
        self.focus(to: self.generateRegion(lon, lat))
    }
    
    public func focus(to region: MKCoordinateRegion) {
        mpView.setRegion(region, animated: true)
    }
    
    public func findMyLocation() {
        let lat = CurrentLocationManager.shared.lat
        let lon = CurrentLocationManager.shared.lon
        self.focus(to: self.generateRegion(lon, lat))
    }
    
    public func satilateModeSwitch() -> Bool {
        self.input.satilateModel = !self.input.satilateModel
        self.mpView.mapType = self.input.satilateModel ? .satellite : .standard
        return self.input.satilateModel
    }
    
    private func generateRegion(_ lon: Double, _ lat: Double) -> MKCoordinateRegion {
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        return region
    }
    
    public func clearPins() {
        self.input.activeAnnotation = nil
        mpView.removeAnnotations(self.input.annotations)
    }
}

extension MapView: CurrentLocationProtocol {
    public func locationChange(lon: Double, lat: Double) {
        self.setCurrentLocation()
    }
}
