//
//  PinView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 30/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import MapKit

public class ParkingAnnotation: NSObject, MKAnnotation {
    public var id: Int = -1
    public var title: String? = nil
    public var locatioName: String? = nil
    public var discipline: String? = nil
    public var coordinate: CLLocationCoordinate2D
    
    public override init() {
        coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 0)!, longitude: CLLocationDegrees(exactly: 0)!)
    }
    
    public init(id: Int,
                title: String,
                locationName: String,
                discipline: String,
                coordinate: CLLocationCoordinate2D) {
        self.id = id
        self.title = title
        self.locatioName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        super.init()
    }
    
    
}
