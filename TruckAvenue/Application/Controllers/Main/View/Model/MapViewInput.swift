//
//  MapViewInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class MapViewInput {
    public var annotations: [ParkingAnnotation] = []
    public var activeAnnotation: ParkingAnnotation?
    public var satilateModel: Bool = false
    
    public init() {
        
    }
    
    public func addAnnotation(_ annotation: ParkingAnnotation) {
        self.annotations.append(annotation)
    }
}
