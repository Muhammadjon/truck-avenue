//
//  ListeningView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import Speech

public protocol SpeechListenerProtocol {
    func onSearch()
    func onSpeaking(text: String)
}

public class ListeningView: UIView {
    private let listeningLabel: UILabel = UILabel()
    private let searchButton: UIButton = UIButton()
    public var delegate: SpeechListenerProtocol?
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    private let cancelButton: UIImageView = UIImageView()
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    
    public override init(frame: CGRect) {
        super.init(frame: frame)

        self.addSubviews([listeningLabel, searchButton])

        listeningLabel.textColor = .white
        listeningLabel.font = theme.appFontBold.withSize(30)
        listeningLabel.textAlignment = .center
        listeningLabel.text = "Listening ..."
        
        searchButton.clipsToBounds = true
        searchButton.backgroundColor = .white
        searchButton.setTitle("search", for: .normal)
        searchButton.setTitleColor(UIColor().color(hex: "333333"), for: .normal)
        searchButton.titleLabel?.font = theme.appFontBold.withSize(20)
        searchButton.layer.cornerRadius = 6
        searchButton.isEnabled = false
        
        self.initialize()
        
        searchButton.onClick(target: self, selector: #selector(onSearch))
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func initialize() {
        speechRecognizer?.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { (status) in
            var isButtonEnabled = false
            switch status {
            case .authorized:
                print("authorized")
                isButtonEnabled = true
            case .denied:
                print("denied")
            case .notDetermined:
                print("not determined")
            case .restricted:
                print("strictly denied")
            @unknown default:
                print("unknown status")
            }
            
            OperationQueue.main.addOperation() {
                self.searchButton.isEnabled = isButtonEnabled
            }
        }
        
        self.startRecording()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        self.addGradient(left: UIColor.black.withAlphaComponent(0.5), right: UIColor.black.withAlphaComponent(0.1), CGPoint(x: 0.5, y: 1), CGPoint(x: 0.5, y: 0))
        
        var x: CGFloat = 8
        var y: CGFloat = 150
        var h: CGFloat = 50
        var w: CGFloat = self.width - 2 * x
        listeningLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = 200
        h = 50
        x = (self.width - w) / 2
        y = self.height - h - 32
        searchButton.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    @objc func onSearch() {
        self.stop()
        self.isHidden = true
        delegate?.onSearch()
    }
    
    func stop() {
        audioEngine.stop()
        recognitionRequest?.endAudio()
        searchButton.isEnabled = false
    }
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                let text = result?.bestTranscription.formattedString
                let order = text?.split(separator: " ").last
                
                if self.filterInput(String(order as! Substring)) {
                    return
                }
                
                self.listeningLabel.text = text
                self.delegate?.onSpeaking(text: text ?? "")
                isFinal = (result?.isFinal)!
            }
            
            if (error != nil || isFinal) {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.searchButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        listeningLabel.text = "Listening ..."
        
    }
    
    private func filterInput(_ text: String) -> Bool {
        if ListenerKeywords.find.contains(text) {
            self.onSearch()
            return true
        }
        
        return false
    }
}

extension ListeningView: SFSpeechRecognizerDelegate {
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            searchButton.isEnabled = true
        } else {
            searchButton.isEnabled = false
        }
    }
}
