//
//  MapViewModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 09/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class MapViewModel {
    
    private let apiWorker: MainApiWorker = MainApiWorker.shared
    
    public static let shared: MapViewModel = MapViewModel()
    
    public var parkingList: [ParkingItemResponseModel] = []
    
    public private(set) var allParkingList: [ParkingItemResponseModel] = []
    
    private init() {
        
    }
    
    public func getParkingList(_ completion: @escaping ((ParkingListResponseModel?) -> Void)) {
        self.parkingList.removeAll()
        self.allParkingList.removeAll()
        
        apiWorker.getParkingList { (model) in
            if let model = model {
                self.parkingList = model.parking ?? []
                self.allParkingList = model.parking ?? []
            }
            completion(model)
            MapViewModel.shared.allParkingList = model?.parking ?? []
            FavoriteManager.shared.favoriteParkingList = self.getFavoriteParkingList()
        }
    }
    
    public func getFavoriteParkingList() -> [ParkingItemResponseModel] {
        var result: [ParkingItemResponseModel] = []
        self.parkingList.forEach { (item) in
            if let f = item.favorite {
                if f.is_favorite {
                    result.append(item)
                }
            }
        }
        return result
    }
    
    public func find(parking id: Int) -> ParkingItemResponseModel? {
        var item: ParkingItemResponseModel? = nil
        
        self.parkingList.forEach { (parking) in
            if parking.id == id {
                item = parking
                return
            }
        }
        
        return item
    }
    
    public func filter(_ key: String) {
        if key.isEmpty {
            self.parkingList = self.allParkingList
            return
        }
        
        self.parkingList = self.allParkingList.filter { (item) -> Bool in
            let hasInDescription = (item.parkingDescription ?? "").contains(key)
            let hasInTitle = (item.title ?? "").contains(key)
            let hasInAddress = (item.address ?? "").contains(key)
            
            return hasInDescription || hasInTitle || hasInAddress
        }
    }
}
