//
//  MainApiRouter.swift
//  TruckAvenue
//
//  Created by muhammadjon on 09/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Alamofire

public enum MainApiRouter: URLRequestConvertible {
    case getParkingList
    case getTruckList
    case getTruckTypeList
    case deleteTruck(id: Int)
    case addCardVisa(name: String, number: String, exMonth: Int, exYear: Int, cvv: Int)
    case getCardList
    case deleteCard(id: Int)
    case getNotifications
    case getParkingComments(id: Int)
    case parkingComment(id: Int, comment: String, rate: Int)
    case getParkingOrderId(parkingId: Int, fromDate: String, toDate: String)
    case payToOrder(orderId: Int, cardID: Int)
    case paymentList
    case makeFavoritParking(id: Int)
    /// defavorite api 
    case removeFavoriteId(_ id: Int)
    /// makeMainTruck id: Truck ID
    case makeMainTruck(id: Int)

    case getApprovedOrderList
    case getNewOrderList
    case getFinishedOrderList
    case getAllOrderList
    
    var method: HTTPMethod {
        switch self {
        case .addCardVisa, .parkingComment, .getParkingOrderId, .payToOrder, .makeFavoritParking:
            return .post
        case .removeFavoriteId, .deleteCard, .deleteTruck:
            return .delete
        case .makeMainTruck:
            return .put
        default:
            return .get
        }
    }
    
    public var address: String {
        switch self {
        case .getParkingList:
            return ApplicationNetwork.API.Main.parkingListAPI
        case .getTruckList:
            return ApplicationNetwork.API.Main.getTruckList
        case .getTruckTypeList:
            return ApplicationNetwork.API.Other.getTruckTypeList
        case let .deleteTruck(id):
            return "\(ApplicationNetwork.API.Main.deleteTruck)\(id)/"
        case .addCardVisa:
            return ApplicationNetwork.API.Card.addCardVisaAPI
        case .getCardList:
            return ApplicationNetwork.API.Card.cardListAPI
        case let .deleteCard(id):
            return "\(ApplicationNetwork.API.Card.deleteCardAPI)\(id)"
        case .getNotifications:
            return ApplicationNetwork.API.Notification.getNotificationList
        case let .getParkingComments(id):
            return "\(ApplicationNetwork.API.Comments.getCommentsAPI)\(id)/get_parking/"
        case .parkingComment:
            return ApplicationNetwork.API.Comments.commentAPI
        case .getParkingOrderId:
            return ApplicationNetwork.API.Reservation.getOrderId
        case .payToOrder:
            return ApplicationNetwork.API.Reservation.pay
        case .paymentList:
            return ApplicationNetwork.API.Reservation.paymentList
        case .makeFavoritParking:
            return ApplicationNetwork.API.Comments.makeFavoritParking
        case let .removeFavoriteId(id):
            return "\(ApplicationNetwork.API.Comments.removeFavoriteId)\(id)/"
        case let .makeMainTruck(id):
            return "\(ApplicationNetwork.API.Main.makeTruckMain)\(id)/"
        case .getApprovedOrderList:
            return ApplicationNetwork.API.Reservation.approvedOrderList
        case .getNewOrderList:
            return ApplicationNetwork.API.Reservation.newOrderList
        case .getFinishedOrderList:
            return ApplicationNetwork.API.Reservation.finishedOrderList
        case .getAllOrderList:
            return ApplicationNetwork.API.Reservation.allOrderList
        }
    }
    
    private var body: Data? {
        var body: [String: String] = [:]
        switch self {
        case .getParkingList:
            break
        case let .addCardVisa(name, number, exMonth, exYear, cvv):
            body["cardholder_name"] = name
            body["number"] = number
            body["exp_month"] = "\(exMonth)"
            body["exp_year"] = "\(exYear)"
            body["cvv"] = "\(cvv)"
        case let .parkingComment(id, comment, rate):
            body["parking"] = "\(id)"
            body["comment"] = comment
            body["rate"] = "\(rate)"
        case let .getParkingOrderId(parkingId, fromDate, toDate):
            body["parking"] = "\(parkingId)"
            body["from_date"] = fromDate
            body["to_date"] = toDate
        case let .payToOrder(orderID, cardID):
            body["order_id"] = "\(orderID)"
            body["card_id"] = "\(cardID)"
        case let .makeFavoritParking(id):
            body["parking"] = "\(id)"
        case .makeMainTruck:
            body["is_active"] = "true"
        default:
            body = [:]
        }
        
        
        do {
            return try JSONEncoder().encode(body)
        } catch let error {
            print(error)
            return nil
        }
    }
    
    
    public func asURLRequest() throws -> URLRequest {
        let url = try address.asURL()
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        request.set(value: .applicationJSON, toField: .contentType)
        
        let token = UserPreferences.token
        if token != "" {
            request.set(value: "Token \(token)", toField: .authorization)
        }
        
        request.httpBody = body
            
        return request
    }
}
