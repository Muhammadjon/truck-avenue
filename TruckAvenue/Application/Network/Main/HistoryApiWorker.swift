//
//  HistoryApiWorker.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Alamofire

public class HistoryApiWorker {
    public static let shared: HistoryApiWorker = HistoryApiWorker()
    
    public func getFinishedOrderList() {
        Alamofire.request(MainApiRouter.getFinishedOrderList).response { (response) in
            
        }
    }
    
    public func getApprovedList() {
        Alamofire.request(MainApiRouter.getApprovedOrderList).response { (response) in
            
        }
    }
    
    public func getNewList() {
        Alamofire.request(MainApiRouter.getNewOrderList).response { (response) in
            
        }
    }
    
    public func getAllHistoryList(completion: @escaping (([HistoryItem]?) -> Void)) {
        Alamofire.request(MainApiRouter.getAllOrderList).response { (response) in
            if !HistoryManager.shared.shouldUpdate {
                var items: [HistoryItem] = []
                
                HistoryManager.shared.items.forEach({ (i) in
                    items.append(i.model)
                })
                
                completion(items)
                
                return
            }

            ApplicationNetwork.getModel(HistoryResponseModel.self, from: response, completion: { (model) in
                HistoryManager.shared.deleteAll()
                HistoryManager.shared.shouldUpdate = false
                model?.history?.forEach({ (hi) in
                    let item = HistoryItemInput(hi)
                    HistoryManager.shared.insert(item: item)
                })
                
                completion(model?.history)
            })
        }
    }
}
