//
//  FavoritParkingNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 15/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkingFavoriteResponseModel: Codable {
    public let id: Int?
    
    public init(id: Int?) {
        self.id = id
    }
}
