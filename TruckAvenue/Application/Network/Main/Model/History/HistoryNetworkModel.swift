//
//  HistoryNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class HistoryResponseModel: Codable {
    public let history: [HistoryItem]?
    
    public init(history: [HistoryItem]?) {
        self.history = history
    }
}

// MARK: - History

public enum HistoryType: Int {
    case new = 0
    case finished
    case approved
    
    public static func indentify(type: String) -> HistoryType {
        if type == "New Request" {
            return .new
        }
        
        if type == "Approved" {
            return .approved
        }
        
        return .finished
    }
}

public class HistoryItem: Codable {
    public let id: Int?
    public let fromDate, toDate, created, totalPrice: String?
    public let parking: ParkingItemResponseModel?
    public let userTruck: TruckItemResponseModel?
    public var status: String?
    
    public var type: HistoryType {
        return HistoryType.indentify(type: status ?? "")
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case fromDate = "from_date"
        case toDate = "to_date"
        case created
        case totalPrice = "total_price"
        case parking
        case userTruck = "user_truck"
        case status
    }
    
    public init(id: Int?, fromDate: String?, toDate: String?, created: String?, totalPrice: String?, parking: ParkingItemResponseModel?, userTruck: TruckItemResponseModel?, status: String?) {
        self.id = id
        self.fromDate = fromDate
        self.toDate = toDate
        self.created = created
        self.totalPrice = totalPrice
        self.parking = parking
        self.userTruck = userTruck
        self.status = status
    }
}
