//
//  CardNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 25/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class AddVisaNetworkResponse: Codable {
    public let id: Int
    public let cardholderName, number, expMonth, expYear: String
    public let cvv: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case cardholderName = "cardholder_name"
        case number
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case cvv
    }
    
    public init(id: Int, cardholderName: String, number: String, expMonth: String, expYear: String, cvv: String) {
        self.id = id
        self.cardholderName = cardholderName
        self.number = number
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvv = cvv
    }
}

public class CardNetworkListResponse: Codable {
    public let cards: [CardNetworkResponseItem]?
    
    public init(cards: [CardNetworkResponseItem]?) {
        self.cards = cards
    }
}


public class CardNetworkResponseItem: Codable {
    public let id: Int?
    public let type, cardholderName, number, expMonth: String?
    public let expYear, cvv, cardid, token: String?
    public let clientip: String?
    
    enum CodingKeys: String, CodingKey {
        case id, type
        case cardholderName = "cardholder_name"
        case number
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case cvv
        case cardid = "card_id"
        case token
        case clientip = "client_ip"
    }
    
    public init(id: Int?, type: String?, cardholderName: String?, number: String?, expMonth: String?, expYear: String?, cvv: String?, cardid: String?, token: String?, clientip: String?) {
        self.id = id
        self.type = type
        self.cardholderName = cardholderName
        self.number = number
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvv = cvv
        self.cardid = cardid
        self.token = token
        self.clientip = clientip
    }
}
