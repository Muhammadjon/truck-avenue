//
//  ParkingOrderNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 13/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkingOrderResponseModel: Codable {
    public let id: Int?
    public let parking: ParkingItemResponseModel?
    public let userTruck: TruckItemResponseModel?
    public let fromDate, toDate, totalPrice, created: String?
    
    enum CodingKeys: String, CodingKey {
        case id, parking
        case userTruck = "user_truck"
        case fromDate = "from_date"
        case toDate = "to_date"
        case totalPrice = "total_price"
        case created
    }
    
    public init(id: Int?, parking: ParkingItemResponseModel?, userTruck: TruckItemResponseModel?, fromDate: String?, toDate: String?, totalPrice: String?, created: String?) {
        self.id = id
        self.parking = parking
        self.userTruck = userTruck
        self.fromDate = fromDate
        self.toDate = toDate
        self.totalPrice = totalPrice
        self.created = created
    }
}

public class PayToOrderResponseModel: Codable {
    public let order: ParkingOrderResponseModel
    public let card: CardNetworkResponseItem
    
    public init(order: ParkingOrderResponseModel, card: CardNetworkResponseItem) {
        self.order = order
        self.card = card
    }
}
