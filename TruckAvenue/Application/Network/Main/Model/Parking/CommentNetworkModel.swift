//
//  CommentNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 09/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class CommentListResponseModel: Codable {
    public let comments: [CommentResponseModel]?
    
    public init(comments: [CommentResponseModel]?) {
        self.comments = comments
    }
}

public class CommentResponseModel: Codable {
    public let id, parking: Int?
    public let user: UserDetailResponseModel?
    public let comment: String?
    public let rate: Int?
    //"created": "2019-07-10T17:00:59.009741Z"
    public let created: String?
    
    public init(id: Int?, parking: Int?, user: UserDetailResponseModel?, comment: String?, rate: Int?, created: String?) {
        self.id = id
        self.parking = parking
        self.user = user
        self.comment = comment
        self.rate = rate
        self.created = created
    }
}
