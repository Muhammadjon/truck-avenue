//
//  ParkingItemResponseModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 09/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ParkingListResponseModel: Codable {
    public let parking: [ParkingItemResponseModel]?
   
    enum CodingKeys: String, CodingKey {
        case parking
    }
    
    public init(parking: [ParkingItemResponseModel]) {
        self.parking = parking
    }
}

public class ParkingItemResponseModel: Codable {
    public let id: Int?
    public let title, parkingDescription, address, longitude: String?
    public let latitude, workingTime: String?
    public let freePlaces, ourTax: Int?
    public let logo, photo: String?
    public let phones: [ParkingItemResponsePhone]?
    public let prices: [ParkingItemResponsePrice]?
    public let comment: ParkingItemResponseComments?
    public let gallery: [ParkingItemResponseGalley]?
    public var stars: Float?
    public var starsLen: Float?
    public var favorite: ParkingFavoriteResponse?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case parkingDescription = "description"
        case address, longitude, latitude
        case workingTime = "working_time"
        case freePlaces = "free_places"
        case ourTax = "our_tax"
        case logo, photo, phones, prices
        case comment = "comments"
        case gallery = "gallery"
        case stars
        case starsLen = "stars_len"
        case favorite
    }
    
    public init(id: Int?, title: String?, parkingDescription: String?, address: String?, longitude: String?, latitude: String?, workingTime: String?, freePlaces: Int?, ourTax: Int?, logo: String?, photo: String?, phones: [ParkingItemResponsePhone]?, prices: [ParkingItemResponsePrice]?, comment: ParkingItemResponseComments?, gallery: [ParkingItemResponseGalley]?, stars: Float?, starsLen: Float?, favorite: ParkingFavoriteResponse?) {
        self.id = id
        self.title = title
        self.parkingDescription = parkingDescription
        self.address = address
        self.longitude = longitude
        self.latitude = latitude
        self.workingTime = workingTime
        self.freePlaces = freePlaces
        self.ourTax = ourTax
        self.logo = logo
        self.photo = photo
        self.phones = phones
        self.prices = prices
        self.comment = comment
        self.gallery = gallery
        self.stars = stars
        self.starsLen = starsLen
        self.favorite = favorite
    }
}

public class ParkingFavoriteResponse: Codable {
    public var id: Int?
    public var is_favorite: Bool
    
    public init(id: Int?, is_favorite: Bool) {
        self.id = id
        self.is_favorite = is_favorite
    }
}

public class ParkingItemResponseComments: Codable {
    public let user: ParkingItemResponseUser?
    public let comment, created: String?
    public let rate: Int?
    
    public init(user: ParkingItemResponseUser?, comment: String?, created: String?, rate: Int?) {
        self.user = user
        self.comment = comment
        self.created = created
        self.rate = rate
    }
    
    public var readableDate: String? {
        return (created ?? "").getDateAndTimeAsString().date
    }
}

// MARK: - Galleria
public class ParkingItemResponseGalley: Codable {
    public let id: Int?
    public let photo: String?
    
    public init(id: Int?, photo: String?) {
        self.id = id
        self.photo = photo
    }
    
    public var photoURL: String {
        return "\(ApplicationNetwork.baseAPI)\(photo ?? "")"
    }
}

// MARK: - Phone
public class ParkingItemResponsePhone: Codable {
    public let id: Int?
    public let title, phone: String?
    
    public init(id: Int?, title: String?, phone: String?) {
        self.id = id
        self.title = title
        self.phone = phone
    }
}

// MARK: - Price
public class ParkingItemResponsePrice: Codable {
    public let id: Int?
    public let price: String?
    
    public init(id: Int?, price: String?) {
        self.id = id
        self.price = price
    }
}

public class ParkingItemResponseUser: Codable {
    public let id: Int?
    public let username, email: String?
    
    public init(id: Int?, username: String?, email: String?) {
        self.id = id
        self.username = username
        self.email = email
    }
}
