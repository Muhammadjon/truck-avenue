//
//  NotificationNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public struct NotificationListResponse: Codable {
    public let notifications: [NotificationResponse]?
    
    public init(notifications: [NotificationResponse]?) {
        self.notifications = notifications
    }
}

// MARK: - Notification
public struct NotificationResponse: Codable {
    public let id: Int?
    public let photo, title, notificationDescription, created: String?
    
    enum CodingKeys: String, CodingKey {
        case id, photo, title
        case notificationDescription = "description"
        case created
    }
    
    public init(id: Int?, photo: String?, title: String?, notificationDescription: String?, created: String?) {
        self.id = id
        self.photo = photo
        self.title = title
        self.notificationDescription = notificationDescription
        self.created = created
    }
}
