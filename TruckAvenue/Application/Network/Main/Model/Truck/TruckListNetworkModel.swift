//
//  TruckListResponsModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class TruckListResponseModel: Codable {
    public let trucks: [TruckItemResponseModel]?
    
    public init(trucks: [TruckItemResponseModel]?) {
        self.trucks = trucks
    }
}

public class UpdateTruckItemResponseModel: Codable {
    public let truck: TruckItemResponseModel?
}

// MARK: - Truck
public class TruckItemResponseModel: Codable {
    public let id: Int?
    public let truckNumber, model, photo: String?
    public let isActive: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id
        case truckNumber = "truck_number"
        case model, photo
        case isActive = "is_active"
    }
    
    public init(id: Int?, truckNumber: String?, model: String?, photo: String?, isActive: Bool?) {
        self.id = id
        self.truckNumber = truckNumber
        self.model = model
        self.photo = photo
        self.isActive = isActive
    }
}
