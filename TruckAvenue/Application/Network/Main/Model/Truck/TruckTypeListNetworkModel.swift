//
//  TruckTypeListResponseModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 12/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class TruckTypeListResponseModel: Codable {
    public let trucks: [TruckTypeResponseItem]?
    
    public init(trucks: [TruckTypeResponseItem]?) {
        self.trucks = trucks
    }
}

// MARK: - Truck
public class TruckTypeResponseItem: Codable {
    public let id: Int?
    public let model: String?
    
    public init(id: Int?, model: String?) {
        self.id = id
        self.model = model
    }
}
