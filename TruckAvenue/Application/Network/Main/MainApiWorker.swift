//
//  MainApiWorker.swift
//  TruckAvenue
//
//  Created by muhammadjon on 09/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Alamofire

public class MainApiWorker {
    public static let shared: MainApiWorker = MainApiWorker()
    
    public func getParkingList(_ completion: @escaping ((ParkingListResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.getParkingList).response { (response) in
            ApplicationNetwork.getModel(ParkingListResponseModel.self, from: response.data, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func getTruckList(_ completion: @escaping ((TruckListResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.getTruckList).response { (response) in
            ApplicationNetwork.getModel(TruckListResponseModel.self, from: response.data, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func getTruckTypeList(_ completion: @escaping ((TruckTypeListResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.getTruckTypeList).response { (response) in
            ApplicationNetwork.getModel(TruckTypeListResponseModel.self, from: response.data, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func addVisaCard(name: String, number: String, exMonth: Int, exYear: Int, cvv: Int,
                            _ completion: @escaping ((AddVisaNetworkResponse?) -> Void)) {
        Alamofire.request(MainApiRouter.addCardVisa(
            name: name,
            number: number,
            exMonth: exMonth,
            exYear: exYear,
            cvv: cvv)).response { (response) in
                ApplicationNetwork.getModel(AddVisaNetworkResponse.self, from: response, completion: { (model) in
                    completion(model)
                })
        }
    }
    
    public func loadAllCards(_ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(MainApiRouter.getCardList).response { (response) in
            ApplicationNetwork.getModel(CardNetworkListResponse.self, from: response, completion: { (model) in
                if let model = model {
                    logger.log(MainApiWorker.self, message: "payment methods loaded", type: LoggingType.error)
                    _ = CardManager.shared.deleteAll()
                    model.cards?.forEach({ (item) in
                        let input = CardListItemInput(model: item)
                        CardManager.shared.insert(item: input)
                    })
                    completion(true)
                    return
                }
                logger.log(MainApiWorker.self, message: "not payment method", type: LoggingType.error)
                completion(false)
            })
        }
    }
    
    public func loadNotifications(_ completion: @escaping ((NotificationListResponse?) -> Void)) {
        Alamofire.request(MainApiRouter.getNotifications).response { (response) in
            ApplicationNetwork.getModel(NotificationListResponse.self, from: response, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func loadComentsOf(park id: Int, _ completion: @escaping ((CommentListResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.getParkingComments(id: id)).response { (response) in
            ApplicationNetwork.getModel(CommentListResponseModel.self, from: response, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func comment(id: Int, comment: String, rate: Int, _ completion: @escaping ((CommentResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.parkingComment(id: id, comment: comment, rate: rate)).response { (response) in
            ApplicationNetwork.getModel(CommentResponseModel.self, from: response, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func getParkingOrderId(parkingId: Int, fromDate: String, toDate: String, completion: @escaping ((Bool, Int) -> Void)) {
        Alamofire.request(MainApiRouter.getParkingOrderId(parkingId: parkingId, fromDate: fromDate, toDate: toDate)).response { (response) in
            ApplicationNetwork.getModel(ParkingOrderResponseModel.self, from: response, completion: { (model) in
                if let m = model {
                    completion(true, m.id ?? 0)
                    return
                }
                completion(false, -1)
            })
        }
    }
    
    public func payToOrder(order id: Int, from cardId: Int, completion: @escaping ((PayToOrderResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.payToOrder(orderId: id, cardID: cardId)).response { (response) in
            ApplicationNetwork.getModel(PayToOrderResponseModel.self, from: response, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func getPaymentList() {
        Alamofire.request(MainApiRouter.paymentList).response { (response) in
            
        }
    }
    
    public func makeParkingFavorite(parkingId id: Int, _ completion: @escaping ((ParkingFavoriteResponseModel?) -> Void)) {
        Alamofire.request(MainApiRouter.makeFavoritParking(id: id)).responseJSON { (response) in
            ApplicationNetwork.getModel(ParkingFavoriteResponseModel.self, from: response.data, completion: { (model) in
                completion(model)
            })
        }
    }
    
    public func deleteFavorite(favoritID id: Int, _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(MainApiRouter.removeFavoriteId(id)).response { (response) in
            let c = response.response?.statusCode ?? 204
            completion( c <= 299 && c >= 200)
        }
    }
    
    public func deleteCard(cardId id: Int, _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(MainApiRouter.deleteCard(id: id)).response { (response) in
            let c = response.response?.statusCode ?? 204
            completion( c <= 299 && c >= 200)
        }
    }
    
    public func deleteTruck(truckID id: Int, _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(MainApiRouter.deleteTruck(id: id)).response { (response) in
            let c = response.response?.statusCode ?? 204
            completion( c <= 299 && c >= 200)
        }
    }
}
