//
//  AuthenticationApiRouter.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Alamofire

public enum AuthenticationApiRouter: URLRequestConvertible {
    case registration(phone: String, name: String, email: String)
    case login(phone: String)
    case confirmation(otp: String, phone: String)
    case createTruck
    case userDetail
    
    var method: HTTPMethod {
        switch self {
        case .confirmation, .login, .registration, .createTruck:
            return .post
        case .userDetail:
            return .get
        }
    }
    
    public var address: String {
        switch self {
        case .registration:
            return ApplicationNetwork.API.Authentication.registrationAPI
        case .login:
            return ApplicationNetwork.API.Authentication.loginAPI
        case .confirmation:
            return ApplicationNetwork.API.Authentication.codeConfirmationAPI
        case .createTruck:
            return ApplicationNetwork.API.Authentication.createTruckAPI
        case .userDetail:
            return ApplicationNetwork.API.Authentication.getUserDetailAPI
        }
    }
    
    private var body: Data? {
        var body: [String: String] = [:]
        switch self {
        case let .registration(phone, name, email):
            body = [
                "phone": phone,
                "name": name,
                "email": email,
            ]
            
        case let .login(phone):
            body = [
                "phone": phone
            ]
        case let .confirmation(otp, phone):
            body = [
                "phone": phone,
                "code": otp
            ]
        default:
            body = [:]
        }
        
        do {
            return try JSONEncoder().encode(body)
        } catch let error {
            print(error)
            return nil
        }
    }
    
    private var multipartFormData: Data? {
        
        return nil
    }
    
    public func asURLRequest() throws -> URLRequest {
        let url = try address.asURL()
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        request.set(value: .applicationJSON, toField: .contentType)
        
        let token = UserPreferences.token
        if token != "" {
            request.set(value: "Token \(token)", toField: .authorization)
        }
        
        request.httpBody = body
        
        return request
    }
}
