//
//  AuthenticationApiWorker.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class AuthenticationApiWorker {
    public static let shared: AuthenticationApiWorker = AuthenticationApiWorker()
    
    private init() {
        
    }
    
    public func registerUser(phone: String, email: String, username: String,
                             _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(AuthenticationApiRouter.registration(phone: phone, name: username, email: email)).response { (response) in
            
            ApplicationNetwork.getModel(RegistrationNetworkModel.self, from: response.data, completion: { (model) in
                if let _ = model {
                    UserPreferences.login = phone
                    logger.log(AuthenticationApiWorker.self, message: "Registering user successfull")
                    completion(true)
                    return
                }
                logger.log(AuthenticationApiWorker.self, message: "unsuccessfull registration", type: .error)
                completion(false)
            })
        }
    }
    
    public func loginUser(phone: String, _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(AuthenticationApiRouter.login(phone: phone)).response { (response) in
            ApplicationNetwork.getModel(LoginResponseModel.self, from: response.data, completion: { (model) in
                if let _ = model {
                    UserPreferences.login = phone
                    logger.log(AuthenticationApiWorker.self, message: "User logged in successfull")
                    completion(true)
//                    appDelegate.model.loadPrerequisites({ (isOK) in
//                        if ApplicationNetwork.StatusDetector.status == 401 {
//                            UserPreferences.logout()
//                            return
//                        }
//                        
//                        completion(isOK)
//                    })
                    return
                }
                logger.log(AuthenticationApiWorker.self, message: "unsuccessfull registration", type: .error)
                completion(false)
            })
        }
    }
    
    public func confirmationCode(phone: String, code: String, _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(AuthenticationApiRouter.confirmation(otp: code, phone: phone)).response { (response) in
            ApplicationNetwork.getModel(ConfirmCodeResponseModel.self, from: response.data, completion: { (model) in
                if let model = model {
                    UserPreferences.token = model.token
                    UserPreferences.login = phone
                    logger.log(AuthenticationApiWorker.self, message: "Code confirmation successfull")
                    appDelegate.model.loadPrerequisites({ (isOK) in
                        if ApplicationNetwork.StatusDetector.status == 401 {
                            UserPreferences.logout()
                            return
                        }
                        
                        completion(isOK)
                    })
                    return
                }
                completion(false)
            })
        }
    }
    
    public func getUserDetails(_ completion: @escaping (() -> Void)) {
        Alamofire.request(AuthenticationApiRouter.userDetail).response { (response) in
            ApplicationNetwork.getModel(UserDetailResponseModel.self, from: response, completion: { (model) in
                if let model = model {
                    UserPreferences.username = model.username ?? ""
                    UserPreferences.avatarURL = model.avatarURL
                    UserPreferences.email = model.email ?? ""
                    logger.log(AuthenticationApiWorker.self, message: "User Details Loaded")
                }
                completion()
            })
        }
    }
    
    public func updateUser(avatar: UIImage?, username: String, email: String, phone_number: String,
                           _ completion: @escaping ((Bool) -> Void)) {
        let headers: HTTPHeaders = [
            "Authorization": "Token \(UserPreferences.token)",
            "Accept": "application/json",
            "Content-Type" :"application/json"
        ]
        
        let url = URL(string: ApplicationNetwork.API.Authentication.updateUserDetailAPI)

        Alamofire.upload(multipartFormData: { (mfData) in
            if let image = avatar?.jpegData(compressionQuality: 0.1) {
                mfData.append(image, withName: "avatar", fileName: "avatar.jpg", mimeType: "image/jpeg")
            }
            if
                let username = username.data(using: .utf8),
                let email = email.data(using: .utf8),
                let phone = phone_number.data(using: .utf8) {
             
                mfData.append(username, withName: "username")
                mfData.append(phone, withName: "phone_number")
                mfData.append(email, withName: "email")
            }
        }, usingThreshold: UInt64(2),
           to: url!,
           method: .put,
           headers: headers) { (multipartResult) in
            switch multipartResult {
            case let .success(request, _, _):
                request.uploadProgress(closure: { (process) in
                    logger.log("updating user", message: "\(process.fractionCompleted)")
                })
                
                request.responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        ApplicationNetwork.getModel(UserDetailResponseModel.self, from: response.data, completion: { (model) in
                            if let model = model {
                                ApplicationNetwork.StatusDetector.error.value = false
                                ApplicationNetwork.StatusDetector.error.reason = nil
                                ApplicationNetwork.StatusDetector.error.status = 200

                                UserPreferences.username = model.username ?? ""
                                UserPreferences.avatarURL = model.avatarURL
                                UserPreferences.email = model.email ?? ""
                                logger.log(AuthenticationApiWorker.self, message: "updating user successfull")
                                completion(true)
                            } else {
                                completion(false)
                            }
                        })
                        break
                    case let .failure(error):
                        logger.log(AuthenticationApiWorker.self, message: error.localizedDescription)
                        ApplicationNetwork.StatusDetector.error.value = true
                        ApplicationNetwork.StatusDetector.error.reason = error.localizedDescription
                        ApplicationNetwork.StatusDetector.error.status = 400
                        completion(false)
                        break
                    }
                })
                break
            case let .failure(error):
                logger.log(AuthenticationApiWorker.self, message: error.localizedDescription)
                ApplicationNetwork.StatusDetector.error.value = true
                ApplicationNetwork.StatusDetector.error.reason = error.localizedDescription
                ApplicationNetwork.StatusDetector.error.status = 400
                completion(false)
                break
            }
        }
    }
    
    public func makeTruckMain(truckID id: Int, _ completion: @escaping ((Bool) -> Void)) {
        Alamofire.request(MainApiRouter.makeMainTruck(id: id)).response { (response) in
            let code = response.response?.statusCode ?? 404
            completion(code < 300 && code >= 200)
        }
    }
    
    public func updateTruck(id: Int,
                            image: UIImage?,
                            truckNumber: String?,
                            truckModel: String?,
                            isMain: Bool?, _ completion: @escaping ((Bool) -> Void)) {
        let headers: HTTPHeaders = [
            "Authorization": "Token \(UserPreferences.token)",
            "Accept": "application/json",
            "Content-Type" :"application/json"
        ]
        
        let url = URL(string: "\(ApplicationNetwork.API.Authentication.createTruckAPI)\(id)/")
        
        Alamofire.upload(multipartFormData: { (mfData) in
            
            if let imageData = image?.jpegData(compressionQuality: 0.2) {
                mfData.append(imageData, withName: "photo", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            if let numberData = truckNumber?.data(using: .utf8) {
                mfData.append(numberData, withName: "truck_number")
            }
            
            if let modelData = truckModel?.data(using: .utf8) {
                mfData.append(modelData, withName: "model")
            }
            
            if let m = isMain {
                let mainString = m ? "true" : "false"
                if let isMainData = mainString.data(using: .utf8) {
                    mfData.append(isMainData, withName: "is_active")
                }
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold,
           to: url!,
           method: .put,
           headers: headers) { (multipartResult) in
            switch multipartResult {
            case let .success(request, _, _):
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress \(progress.fractionCompleted)")
                })
                
                request.responseJSON(completionHandler: { (response) in
                    print(response.result)
                    switch response.result {
                    case .success:
                        ApplicationNetwork.getModel(UpdateTruckItemResponseModel.self, from: response.data, completion: { (model) in
                            if let m = model {
                                let id = m.truck?.id ?? -1
                                let t = TruckListItemInput(id: id)
                                if let truck = m.truck {
                                    t.setItems(truck)
                                    if image != nil {
                                        t.image = image
                                    }
                                    
                                    TruckManager.shared.update(item: t)
                                }
                                TruckManager.shared.makeMain(id)
                            }
                            completion(true)
                        })
                        print("uploaded successfully")
                    case let .failure(error):
                        completion(false)
                        print("could not upload \(error)")
                    }
                })
                break
            case .failure(let error):
                print(error)
                completion(true)
                break
            }
        }
    }
    
    public func createTruck(image: UIImage?,
                            truckNumber: String,
                            truckModel: String,
                            isMain: Bool, _ completion: @escaping ((Bool) -> Void)) {
        let headers: HTTPHeaders = [
            "Authorization": "Token \(UserPreferences.token)",
            "Accept": "application/json",
            "Content-Type" :"application/json"
        ]
        
        let url = URL(string: ApplicationNetwork.API.Authentication.createTruckAPI)
        
        Alamofire.upload(multipartFormData: { (mfData) in
            //mfData
            if let imageData = image?.jpegData(compressionQuality: 0.2) {
                mfData.append(imageData, withName: "photo", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            if let numberData = truckNumber.data(using: .utf8) {
                mfData.append(numberData, withName: "truck_number")
            }
            
            if let modelData = truckModel.data(using: .utf8) {
                mfData.append(modelData, withName: "model")
            }
            
            let mainString = isMain ? "true" : "false"
            
            if let isMainData = mainString.data(using: .utf8) {
                mfData.append(isMainData, withName: "is_active")
            }
            
        }, to: url!,
           headers: headers) { (multipartResult) in
            switch multipartResult {
            case let .success(request, _, _):
                
                request.uploadProgress(closure: { (progress) in
                    print("upload progress \(progress.fractionCompleted)")
                })
                
                request.responseJSON(completionHandler: { (response) in
                    print(response.result)
                    switch response.result {
                    case .success:
                        ApplicationNetwork.getModel(TruckItemResponseModel.self, from: response.data, completion: { (model) in
                            if let m = model {
                                TruckManager.shared.append(response: m)
                                if m.isActive ?? false {
                                    TruckManager.shared.makeMain(m.id!)
                                }
                            }
                            
                            completion(true)
                        })
                        print("uploaded successfully")
                    case let .failure(error):
                        completion(false)
                        print("could not upload \(error)")
                    }
                })
                break
            case .failure(let error):
                print(error)
                completion(true)
                break
            }
        }
    }
}
