//
//  UserDetailResponseModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 18/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

// MARK: - UserDetailResponseModel
public class UserDetailResponseModel: Codable {
    public let id: Int?
    public let avatar, username, phoneNumber, email: String?
    
    public var avatarURL: String {
        return "\(ApplicationNetwork.baseAPI)\(avatar ?? "")"
    }
    
    enum CodingKeys: String, CodingKey {
        case id, avatar, username
        case phoneNumber = "phone_number"
        case email
    }
    
    public init(id: Int?, avatar: String?, username: String?, phoneNumber: String?, email: String?) {
        self.id = id
        self.avatar = avatar
        self.username = username
        self.phoneNumber = phoneNumber
        self.email = email
    }
}
