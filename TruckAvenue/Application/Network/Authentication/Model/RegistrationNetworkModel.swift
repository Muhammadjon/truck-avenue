//
//  RegistrationNetworkModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public struct RegistrationNetworkModel: Codable {
    public let detail: String
    
    public init(detail: String) {
        self.detail = detail
    }
}

public class LoginResponseModel: Codable {
    public let detail: String
    
    public init(detail: String) {
        self.detail = detail
    }
}


public class ConfirmCodeResponseModel: Codable {
    public let token: String
    
    public init(token: String) {
        self.token = token
    }
}
