//
//  ApplicationApiList.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class ApplicationNetwork {
    public static let baseAPI: String = "https://tweak.group"
    public static let appName: String = "truck_avenue"
    
    public class API {
        public class Other {
            public static let getTruckTypeList = "\(baseAPI)/\(appName)/api/trucks"
        }
        
        public class Authentication {
            public static let registrationAPI = "\(baseAPI)/\(appName)/api/user/register"
            public static let loginAPI = "\(baseAPI)/\(appName)/api/user/login"
            public static let codeConfirmationAPI = "\(baseAPI)/\(appName)/api/user/confirm"
            public static let createTruckAPI = "\(baseAPI)/\(appName)/api/user/truck/"
            public static let updateUserDetailAPI = "\(baseAPI)/\(appName)/api/user/update"
            public static let getUserDetailAPI = "\(baseAPI)/\(appName)/api/user/detail"
        }
        
        public class Main {
            public static let parkingListAPI = "\(baseAPI)/\(appName)/api/parking/list"
            public static let favoriteTrucksAPI = "\(baseAPI)/\(appName)/api/user/favorite/"
            public static let getTruckList = "\(baseAPI)/\(appName)/api/user/truck/"
            /// delete truck method = DELETE
            public static let deleteTruck = "\(baseAPI)/\(appName)/api/user/truck/"
            /// make main truck method = PUT
            public static let makeTruckMain = "\(baseAPI)/\(appName)/api/user/truck/"
        }
        
        public class Card {
            /// post request
            public static let addCardVisaAPI = "\(baseAPI)/\(appName)/api/user/payment/add/visa"
            
            /// get request
            public static let cardListAPI = "\(baseAPI)/\(appName)/api/user/payment/card/list"
            
            /// delete card
            public static let deleteCardAPI = "\(baseAPI)/\(appName)/api/user/payment/card/delete/"
        }
        
        public class Notification {
            /// get request
            public static let getNotificationList = "\(baseAPI)/\(appName)/api/notifications"
        }
        
        public class Comments {
            /// get request
            public static let getCommentsAPI = "\(baseAPI)/\(appName)/api/parking/comment/"
            
            /// post request
            public static let commentAPI = "\(baseAPI)/\(appName)/api/parking/comment/"
            
            /// post make favorite
            public static let makeFavoritParking = "\(baseAPI)/\(appName)/api/user/favorite/"
            
            /// delete remove favorite
            //api/user/favorite/21/
            public static let removeFavoriteId = "\(baseAPI)/\(appName)/api/user/favorite/"
        }
        
        public class Reservation {
            public static let getOrderId = "\(baseAPI)/\(appName)/api/parking/order/create"
            public static let pay = "\(baseAPI)/\(appName)/api/user/payment/paid"
            public static let paymentList = "\(baseAPI)/\(appName)/api/user/payment/paid"
            public static let newOrderList = "\(baseAPI)/\(appName)/api/user/order/history/new/list"
            public static let finishedOrderList = "\(baseAPI)/\(appName)/api/user/order/history/finished/list"
            public static let allOrderList = "\(baseAPI)/\(appName)/api/user/order/history/all/list"
            public static let approvedOrderList = "\(baseAPI)/\(appName)/api/user/order/history/approved/list"
        }
    }
    
    class StatusDetector {
        public static var error: (value: Bool, reason: String?, status: Int) = (false, nil, -1)
        
        public static var hasError: Bool {
            return error.value
        }
        
        public static var errorMessage: String? {
            return error.reason
        }
        
        public static var status: Int {
            return error.status
        }
        
        public static func check(_ data: Data?) -> Data? {
            guard let data = data else {
                self.error.value = false
                self.error.reason = "No data error"
                self.error.status = -1
                return nil
            }
            
            do {
                let json = JSON(data)
                let errorMessage = json["error"]["message"].string
                let status = json["status"].int
                
                if status == 401 {
                    logger.log(message: "The user needs back to login page")
                }
                
                if let msg = errorMessage {
                    self.error.value = true
                    self.error.status = status ?? -1
                    self.error.reason = msg
                    logger.log(ApplicationNetwork.self, message: "\nstatus: \(status ?? 0)\nmessage: \(msg)")
                    
                }
                
                return try json["result"].rawData()
            } catch {
                return nil
            }
        }
    }
    
    public static func getModel<T>(_ type: T.Type, from response: DefaultDataResponse, completion: @escaping ((T?) -> Void)) where T: Decodable {
        self.getModel(type, from: response.data, completion: completion)
    }
    
    public static func getModel<T>(_ type: T.Type, from data: Data?, completion: @escaping ((T?) -> Void)) where T : Decodable {
        if let resultData = StatusDetector.check(data) {
            do {
                let model = try JSONDecoder().decode(type, from: resultData)
                logger.log(ApplicationNetwork.self, message: String(data: resultData, encoding: .utf8) ?? "", type: .special)
                completion(model)
            } catch {
                logger.log(ApplicationNetwork.self, message: error.localizedDescription, type: .networkError)
                logger.log(ApplicationNetwork.self, message: String(data: resultData, encoding: .utf8) ?? "", type: .networkError)
                completion(nil)
            }
        } else {
            logger.log(ApplicationNetwork.self, message: "no data from response", type: .networkError)
            completion(nil)
        }
    }
    
    public static func printError(_ controller: UIViewController) {
        if ApplicationNetwork.StatusDetector.hasError {
            controller.alertInfo(title: "Error", message: ApplicationNetwork.StatusDetector.errorMessage ?? "")
        }
    }
}
