//
//  UserPreferences.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class UserPreferences {
    enum UserPreferncesKeys: String {
        case token = "USER_ACCESS_TOKEN"
        case login = "USER_LOGIN_PHONE_NUMBER"
        case username = "USER_USERNAME"
        case avatarURL = "USER_AVATAR_URL"
        case email = "USER_EMAIL"
    }
    
    public static var isSignedIn: Bool {
        return !(token.isEmpty && login.isEmpty)
    }
    
    public static var username: String {
        set {
            UserDefaults.standard.set(newValue, forKey: UserPreferncesKeys.username.rawValue)
        }
        get {
            return UserDefaults.standard.value(forKey: UserPreferncesKeys.username.rawValue) as? String ?? ""
        }
    }
    
    public static var avatarURL: String {
        set {
            UserDefaults.standard.set(newValue, forKey: UserPreferncesKeys.avatarURL.rawValue)
        }
        get {
            return UserDefaults.standard.value(forKey: UserPreferncesKeys.avatarURL.rawValue) as? String ?? ""
        }
    }
    
    public static var email: String {
        set {
            UserDefaults.standard.set(newValue, forKey: UserPreferncesKeys.email.rawValue)
        }
        get {
            return UserDefaults.standard.value(forKey: UserPreferncesKeys.email.rawValue) as? String ?? ""
        }
    }
    
    public static var token: String {
        set {
            UserDefaults.standard.set(newValue, forKey: UserPreferncesKeys.token.rawValue)
        }
        get {
            return UserDefaults.standard.value(forKey: UserPreferncesKeys.token.rawValue) as? String ?? ""
        }
    }
    
    public static var login: String {
        set {
            UserDefaults.standard.set(newValue, forKey: UserPreferncesKeys.login.rawValue)
        }
        get {
            return UserDefaults.standard.value(forKey: UserPreferncesKeys.login.rawValue) as? String ?? ""
        }
    }
    
    public static func logout() {
        UserPreferences.token = ""
        appDelegate.route.navigate(to: .authentication)
    }
}
