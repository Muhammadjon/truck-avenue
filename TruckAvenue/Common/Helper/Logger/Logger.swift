//
//  Logger.swift
//  TruckAvenue
//
//  Created by muhammadjon on 18/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public enum LoggingType: String {
    case log = "log"
    case error = "error"
    case warning = "warning"
    case special = "special"
    case networkError = "network error"
}

public class Logger {
    public var tag: String = "application"
    private var message: String = ""
    public var type: LoggingType = .log
    
    public static var shared: Logger = Logger()
    
    public func log(_ className: AnyClass, message: String, type: LoggingType = .log) {
        self.type = type
        log(String(describing: className.self), message: message)
    }
    
    public func log(_ tag: String? = nil, message: String, type: LoggingType = .log) {
        self.type = type
        if let tag = tag {
            self.tag = tag
        }
        self.message = message
        self.print_()
    }
    
    private func print_() {
        print("\(self.type)\n\(self.tag): \(message)")
        self.tag = "application"
        self.type = .log
    }
}
