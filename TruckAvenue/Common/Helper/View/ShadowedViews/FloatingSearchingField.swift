//
//  FloatingSearchingField.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class FloatingSearchingField: UIView {
    private let bgView: UIView = UIView()
    private let textField: UITextField = UITextField()
    private let rightImageView: UIImageView = UIImageView()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(bgView)
        bgView.addSubviews([textField, rightImageView])
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let w = self.width - 4
        let h = self.height - 4
        let x: CGFloat = 2
        let y: CGFloat = 2
        bgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        
    }
}
