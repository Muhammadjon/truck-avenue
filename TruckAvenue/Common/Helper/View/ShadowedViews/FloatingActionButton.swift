//
//  ShadowedView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 31/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class FloatingActionButton: UIView {
    private let bgView: UIView = UIView()
    private let imageView: UIImageView = UIImageView()
    
    public var onClick: ((Int) -> Void)? = nil
    
    public convenience init(_ tag: Int) {
        self.init(frame: .zero)
        self.tag = tag
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onButtonClick))
        self.bgView.addGestureRecognizer(tapGesture)
        self.bgView.backgroundColor = UIColor.white
        self.bgView.addShadow(shadowColor: UIColor.gray)
        imageView.contentMode = .scaleAspectFit
        self.addSubview(bgView)
        bgView.addSubview(imageView)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        bgView.frame = self.bounds
        bgView.x = 2
        bgView.y = 2
        bgView.width = bgView.width - 2 * bgView.x
        bgView.height = bgView.height - 2 * bgView.y
        
        let w: CGFloat = bgView.width / 2
        let h: CGFloat = w
        let x = (bgView.width - w) / 2
        let y = (bgView.height - h) / 2
        imageView.frame = CGRect(x: x, y: y, width: w, height: h)
        bgView.layer.cornerRadius = bgView.width / 2
    }
    
    public func setImage(_ image: UIImage) {
        self.imageView.image = image
    }
    
    @objc private func onButtonClick() {
        self.onClick?(self.tag)
    }
}
