//
//  EditTextFieldInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class EditTextFieldInput {
    public var title: String = ""
    public var description: String = ""
    public var text: String = ""
    public var placeholder: String = ""
    public var prefix: String?
    
    internal var bindedValue: Value?
    
    public init() {
        
    }
    
    public init(title: String, description: String) {
        self.title = title
        self.description = description
    }
}
