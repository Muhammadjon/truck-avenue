//
//  EditTextField.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public enum EditTextFieldInpuType {
    case password
    case phoneNumber
    case text
    case email
    case picker
    case number
}

public protocol EditTextFieldDelegate {
    func onTyping(field: EditTextField, text: String) -> Bool
}

public class EditTextField: UIView {
    
    public var delegate: EditTextFieldDelegate?
    
    private let bgView: UIView = UIView()
    private let underlineView: UIView = UIView()
    private let titleLabel: UILabel = UILabel()
    private let descriptionLabel: UILabel = UILabel()
    
    private var rightView: UIView = UIView()
    private var imageView: UIImageView?
    private var rightViewActionClosure: (() -> Void)?
    
    public let inputField: UITextField = UITextField()
    public var beginEditing: ((EditTextField) -> Void)?
    public var endEditing: ((EditTextField) -> Void)?
    public var shouldReturn: ((EditTextField) -> Bool)?
    
    public var inputType: EditTextFieldInpuType = .text {
        didSet {
            switch inputType {
            case .password:
                inputField.isSecureTextEntry = true
                self.rightImage = #imageLiteral(resourceName: "eye_open_icon")
                self.rightViewActionClosure = {
                    if self.inputField.isSecureTextEntry {
                        self.rightImage = #imageLiteral(resourceName: "eye_close_icon")
                        self.inputField.isSecureTextEntry = false
                    } else {
                        self.inputField.isSecureTextEntry = true
                        self.rightImage = #imageLiteral(resourceName: "eye_open_icon")
                    }
                }
            case .picker:
                self.rightImage = #imageLiteral(resourceName: "drow_down_arrow")
                self.rightViewActionClosure = {
                    self.inputField.becomeFirstResponder()
                }
                break
            case .email:
                keyboardType = .emailAddress
                break
            case .phoneNumber:
                inputField.doneAccessory = true
                keyboardType = .phonePad
            case .number:
                inputField.doneAccessory = true
                keyboardType = .numberPad
            default:
                break
            }
        }
    }
    
    public var placeholder: String = "" {
        didSet {
            self.inputField.placeholder = self.placeholder
        }
    }
    
    public var fieldInputView: UIView? {
        didSet {
            self.inputField.inputView = self.fieldInputView
        }
    }
    
    private var theme = appDelegate.model.themeManager.currentTheme()
    
    public private(set) var input: EditTextFieldInput = EditTextFieldInput() {
        didSet {
            self.titleLabel.text = input.title
            self.descriptionLabel.text = input.description
            self.inputField.text = input.text
        }
    }
    
    public var text: String {
        return inputField.text ?? ""
    }
    
    public var rightImage: UIImage? = nil {
        didSet {
            if let image = self.rightImage {
                if rightView.subviews.count >= 1 {
                    let imageView = rightView.subviews[0] as? UIImageView
                    DispatchQueue.main.async {
                        imageView?.image = image
                        self.setNeedsLayout()
                    }
                    return
                }
                imageView = UIImageView(image: image)
                imageView?.contentMode = .scaleAspectFit
                rightView.addSubview(imageView!)
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(rightViewAction))
                rightView.addGestureRecognizer(tapGestureRecognizer)
                self.setNeedsLayout()
            }
        }
    }
    
    public var errorMessage: String? = nil {
        didSet {
            if let error = self.errorMessage {
                descriptionLabel.isHidden = false
                underlineView.isHidden = false
                descriptionLabel.textColor = theme.inputFieldErrorColor
                underlineView.backgroundColor = theme.inputFieldErrorColor
                descriptionLabel.text = error
            } else {
                descriptionLabel.text = input.description
                underlineView.isHidden = true
                descriptionLabel.textColor = theme.inputFieldDescriptionColor
                underlineView.backgroundColor = theme.inputFieldUnderlineColor
            }
        }
    }
    
    public var keyboardType: UIKeyboardType = UIKeyboardType.default {
        didSet {
            inputField.keyboardType = self.keyboardType
        }
    }
    
    public var isEnabled: Bool = true {
        didSet {
            self.inputField.isEnabled = self.isEnabled
            self.inputField.isUserInteractionEnabled = self.isEnabled
            if !self.isEnabled {
                self.inputField.textColor = UIColor.gray
            }
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.updateFrame(bounds: self.bounds)
    }
    
    private func initialize() {
        bgView.backgroundColor = theme.inputFieldBackgroundColor
        bgView.layer.cornerRadius = 4
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(activate))
        bgView.addGestureRecognizer(tapGesture)
        bgView.isUserInteractionEnabled = true
        self.addSubview(bgView)
        
        underlineView.layer.cornerRadius = 1
        underlineView.backgroundColor = theme.inputFieldUnderlineColor
        bgView.addSubview(underlineView)
        
        titleLabel.isUserInteractionEnabled = true
        titleLabel.font = theme.inputFieldTitleFont
        titleLabel.textColor = theme.inputFieldTitleColor
        titleLabel.addGestureRecognizer(tapGesture)
        bgView.addSubview(titleLabel)
        
        inputField.font = theme.inputFieldFont
        inputField.delegate = self
        bgView.addSubview(inputField)
        
        descriptionLabel.font = theme.inputFieldTitleFont
        descriptionLabel.textColor = theme.inputFieldDescriptionColor
        bgView.addSubview(descriptionLabel)
        
        rightView.layer.cornerRadius = bgView.layer.cornerRadius
        bgView.addSubview(rightView)
        
        inputField.textColor = #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1)

    }
    
    private func updateFrame(bounds: CGRect) {
        var x: CGFloat = 0
        var y: CGFloat = 0
        var w: CGFloat = bounds.width
        var h: CGFloat = bounds.height - 20
        
        bgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = bounds.height * 0.05
        x = 8
        w = bgView.bounds.width - x * 2
        h = bounds.height * 0.18
        titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        if self.input.title.isEmpty {
            self.titleLabel.height = 0
        }
        
        y = titleLabel.frame.maxY + bounds.height * 0.02
        h = bgView.bounds.height - y - 4
        inputField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = bgView.frame.width
        h = 2
        x = 0
        y = bgView.frame.height - 2
        underlineView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = bounds.height * 0.18
        x = 8
        y = underlineView.frame.maxY + bounds.height * 0.05
        w = (bgView.bounds.width - x * 2)
        descriptionLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        if self.input.description.isEmpty {
            self.descriptionLabel.height = 0
            self.inputField.height = self.inputField.height + h
            self.underlineView.y = self.inputField.maxY
            self.bgView.height = self.underlineView.maxY
        }
        
        h = bgView.frame.height
        w = h
        x = bgView.frame.width - w
        y = 0
        rightView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        if let _ = self.rightImage {
            let width = rightView.frame.width - 2
            w = width > 24 ? 24 : width
            h = w
            x = (width - w) / 2
            y = (width - h) / 2
            imageView!.frame = CGRect(x: x, y: y, width: w, height: h)
        }
        
        deactivate()
        
    }
    
    public func setInput(input: EditTextFieldInput) {
        self.input = input
        self.inputField.placeholder = input.placeholder
        if let prefix = input.prefix {
            self.inputField.text = prefix
        }
    }
    
    @objc public func activate() {
        if !self.isEnabled {
            return
        }
        
        if let _ = errorMessage {
            self.inputField.text = ""
            self.errorMessage = nil
            self.input.bindedValue?.value = ""
        }
        if !inputField.isFirstResponder {
            inputField.becomeFirstResponder()
        }
        self.underlineView.isHidden = false
        self.underlineView.backgroundColor = theme.inputFieldUnderlineColor
        self.descriptionLabel.textColor = theme.inputFieldDescriptionColor
    }
    
    @objc public func deactivate() {
        self.underlineView.isHidden = true
        self.descriptionLabel.textColor = UIColor.black.withAlphaComponent(0.5)
        if inputField.isFirstResponder {
            inputField.resignFirstResponder()
        }
    }
    
    @objc func rightViewAction() {
        self.rightViewActionClosure?()
    }
    
    public func bind(_ value: Value) {
        self.input.bindedValue = value
        self.inputField.text?.append(value.value)
    }
    
    public func checkInput(_ errorMsg: String? = nil) -> Bool {
        let result = input.bindedValue?.isValid() ?? false
        if !(result) {
            self.errorMessage = errorMsg ?? "not valid input"
        }
        return result
    }
}

extension EditTextField: UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activate()
        self.beginEditing?(self)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.deactivate()
        self.input.bindedValue?.value = textField.text ?? ""
        self.endEditing?(self)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.shouldReturn?(self) ?? true {
            self.endEditing(true)
            return false
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b") == -92
        
        var realText = "\(textField.text ?? "")\(string)"
        if isBackSpace {
            if var txt = textField.text {
                if let pref = input.prefix {
                    if txt == pref {
                        return false
                    }
                }
                
                if !txt.isEmpty {
                    let _ = txt.removeLast()
                }
                realText = txt
            }
        }
        
        self.input.bindedValue?.value = realText
        
        return true && (self.delegate?.onTyping(field: self, text: realText) ?? true)
    }
}
