//
//  RateView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 03/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class RateView: UIView {
    private var numberOfStars: Int = 5
    
    private var starImage: UIImage = #imageLiteral(resourceName: "star_empty")
    
    private let theme = ThemeManager.shared.currentTheme()
    
    public var rateable: Bool = false
    
    public var didStarSelected: ((_ selectedStarOrder: Int) -> Void)?
    
    public convenience init(_ numberOfStars: Int, rateable: Bool = false) {
        self.init(frame: .zero)
        self.numberOfStars = numberOfStars
        self.rateable = rateable
        self.generateStars()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("not implemented method")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.frameForStars()
        
    }
    
    private func generateStars() {
        for i in 0..<numberOfStars {
            let image = self.createStar(tag: i + 100)
            self.addSubview(image)
            if rateable {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onStarClick(_:)))
                image.isUserInteractionEnabled = true
                
                image.addGestureRecognizer(tapGesture)
            }
        }
    }
    
    @objc public func onStarClick(_ gesture: UITapGestureRecognizer) {
        if !self.rateable {
            return
        }
        
        if let imageView = gesture.view as? UIImageView {
            let n = imageView.tag - 100
            updateRate(rate: n + 1)
        }
    }
    
    private func frameForStars() {
        var x: CGFloat = 0
        
        let w: CGFloat = self.width / CGFloat(numberOfStars + 2)
        let h = w
        let y: CGFloat = (self.height - h) / 2
        for i in 0..<numberOfStars {
            if let image = (self.viewWithTag(i + 100) as? UIImageView) {
                
                image.frame = CGRect(x: x, y: y, width: w, height: h)
            }
            x = x + w + 5
        }
    }
    
    private func createStar(tag: Int) -> UIImageView {
        let image = UIImageView(image: starImage.tintColor(color: theme.labelGray))
        image.tag = tag
        
        return image
    }
    
    public func updateRate(rate: Int) {
        
        let r = rate > numberOfStars ? numberOfStars : rate
        self.didStarSelected?(r)
        for i in 0..<numberOfStars {
            
            guard let imageView = (self.viewWithTag(i + 100) as? UIImageView) else {
                fatalError("not an image view error")
            }
            
            if i < r {
                imageView.image = imageView.image?.tintColor(color: theme.appOrange)
            } else {
                imageView.image = imageView.image?.tintColor(color: theme.labelGray)
            }
        }
    }
}
