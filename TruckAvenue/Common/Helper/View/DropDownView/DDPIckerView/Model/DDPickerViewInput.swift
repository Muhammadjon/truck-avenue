//
//  DDPickerViewInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class DDPickerViewInput {
    public var data: [[DDPickerViewItemInput]] = []
    
    public var selectedItem: DDPickerViewItemInput? {
        return nil
    }
    
    public var numberOfColumns: Int {
        return data.count
    }
    
    public init(data: [[DDPickerViewItemInput]]) {
        self.data = data
    }
    
    public init() {
        
    }
    
    public func columnInput(column id: Int) -> [DDPickerViewItemInput] {
        return data[id]
    }
    
    public func itemCountFor(column id: Int) -> Int {
        return columnInput(column: id).count
    }
    
    public func item(column: Int, row: Int) -> DDPickerViewItemInput {
        return columnInput(column: column)[row]
    }
    
    public func setSelected(item: DDPickerViewItemInput) {
        self.data.forEach { (row) in
            row.forEach({ (i) in
                i.isSelected = false
                if i.id == item.id {
                    i.isSelected = true
                }
            })
        }
    }
    
    public func selectedRows() -> [(column: Int, row: Int)] {
        var rc: [(column: Int, row: Int)] = []
        
        for c in 0..<data.count {
            for r in 0..<data[c].count {
                if data[c][r].isSelected {
                    rc.append((c, r))
                }
            }
        }
        return rc
    }
}
