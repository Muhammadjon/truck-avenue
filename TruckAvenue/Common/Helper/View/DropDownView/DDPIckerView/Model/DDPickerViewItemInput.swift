//
//  Input.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class DDPickerViewItemInput {
    public var text: String = ""
    public var isSelected: Bool = false
    public var id: Int = -1
    
    public init(text: String) {
        self.text = text
    }
}
