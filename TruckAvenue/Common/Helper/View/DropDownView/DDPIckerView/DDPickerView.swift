
//
//  File.swift
//  TruckAvenue
//
//  Created by muhammadjon on 06/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class DDPickerView: DDView {
    private let pickerView: UIPickerView = UIPickerView()
    private let okButton: UIButton = UIButton()
    
    public var pickerInput: DDPickerViewInput = DDPickerViewInput()
    
    public var selected: ((DDPickerViewItemInput?) -> Void)?
    
    convenience init(_ controller: UIViewController) {
        self.init(controller: controller)
        
    }
    
    convenience init(_ controller: UIViewController, root: UIView) {
        self.init(controller: controller)
        self.rootView = root
    }
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        okButton.backgroundColor = .white
        okButton.setTitleColor(UIColor.blue.withAlphaComponent(0.8), for: .normal)
        okButton.setTitle("Select", for: .normal)
        okButton.addTarget(self, action: #selector(onSelect), for: .touchUpInside)
        okButton.clipsToBounds = true
        
        self.addSubviews([pickerView, okButton])
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let x: CGFloat = 0
        var y: CGFloat = 0
        let w: CGFloat = self.width
        var h: CGFloat = 40
        okButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = self.height - h
        y = okButton.maxY
        pickerView.frame = CGRect(x: x, y: y, width: w, height: h)
        okButton.layer.cornerRadius = self.layer.cornerRadius
    }
    
    public func setInput(_ input: DDPickerViewInput) {
        self.pickerInput = input
        input.selectedRows().forEach { (item) in
            self.pickerView.selectRow(item.row, inComponent: item.column, animated: true)
        }
        pickerView.dataSource = self
        pickerView.delegate = self
        self.pickerView.reloadInputViews()
    }
    
    @objc private func onSelect() {
        self.selected?(self.pickerInput.selectedItem)
        self.hide()
    }
    
    public override func hide() {
        super.hide()
    }
}

extension DDPickerView: UIPickerViewDataSource, UIPickerViewDelegate {
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item = self.pickerInput.item(column: component, row: row)
        return  item.text
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return self.pickerInput.numberOfColumns
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerInput.itemCountFor(column: component)
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerInput.setSelected(item: self.pickerInput.item(column: component, row: row))
    }
}
