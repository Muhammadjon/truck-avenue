//
//  DDView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 06/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class DDView: UIView {
    private var controller: UIViewController? = nil
    public var isActive: Bool = false
    public var rootView: UIView? = nil
    
    public convenience init(controller: UIViewController) {
        self.init(frame: .zero)
        self.controller = controller
        self.tag = 1002
        controller.view.addSubviewWithAnimation(self, options: [.transitionCrossDissolve])
        self.addShadow(shadowColor: UIColor.gray)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.isHidden = true
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public func present(root view: UIView) {
        self.rootView = view
        self.isActive = true
        self.isHidden = false
        self.layoutSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if let view = self.rootView {
            guard let controller = self.controller else {
                fatalError("no controller")
            }
            
            self.layer.cornerRadius = 4
            
            var x: CGFloat = view.x
            var y: CGFloat = view.maxY + 1
            var w: CGFloat = view.width
            if w < 180 {
                w = 200
            }
            
            let h: CGFloat = 220
            let d = controller.view.height - (y + h)
            let t = controller.view.width - (x + w)
            if d < 0 {
                y = y + (d)
            }
            
            if t < 0 {
                x = x + (t)
            }
            
            self.frame = CGRect(x: x, y: y, width: w, height: h)
        }
    }
    
    public func hide() {
        self.isActive = false
        self.isHidden = true
    }
}
