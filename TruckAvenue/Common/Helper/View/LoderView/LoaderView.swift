//
//  LoaderView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import Lottie

public class LoaderView: UIView {
    private let animationView: AnimationView = AnimationView()
    private var currentProgress: AnimationProgressTime = 0
    private let animation = Animation.named("animation-w81-h81")
    public var isSuccess: Bool = false
    
    public var onFinish: (() -> Void)?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(animationView)
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.animationSpeed = 2
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        animationView.frame = self.bounds
    }
    
    public func start() {
        animationView.play(fromProgress: 0, toProgress: 0.28, loopMode: .loop) { (isFinished) in
            self.currentProgress = self.animationView.currentProgress
            if self.isSuccess {
                self.showSuccess()
            } else {
                self.showFailure()
            }
        }
    }
    
    public func finish(_ success: Bool) {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async {
            self.stop(success)
        }
    }
    
    private func stop(_ success: Bool) {
        DispatchQueue.main.async {
            self.isSuccess = success
            self.animationView.pause()
        }
    }
    
    private func showSuccess() {
        self.animationView.play(fromProgress: self.currentProgress, toProgress: 0.47, loopMode: .playOnce, completion: { (isOK) in
            self.animationView.removeFromSuperview()
            self.onFinish?()
        })
    }
    
    private func showFailure() {
        let p = self.currentProgress + 0.5
        self.animationView.play(fromProgress: p, toProgress: 0.97, loopMode: .playOnce, completion: { (isOK) in
            print("finished failure")
            self.animationView.removeFromSuperview()
            self.onFinish?()
        })
    }
}
