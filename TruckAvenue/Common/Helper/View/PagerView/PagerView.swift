//
//  UIView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class PagerView: UIView {
    
    public var numberOfItems: Int = 3
    
    private var pages: [PagerItem] = []
    
    public var onSelect: ((_ id: Int) -> Void)?
    
    public var images: [UIImage] = []
    
    public private(set) var selectedId: Int = 0
    
    public convenience init(_ n: Int) {
        self.init()
        self.numberOfItems = n
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public func generateViews() {
        if images.count != numberOfItems {
            fatalError("provide with images for tabs")
        }
        
        let w: CGFloat = self.width / CGFloat(numberOfItems)
        let h = self.height
        var x: CGFloat = 0
        let y: CGFloat = 0
        for i in 0..<numberOfItems {
            let page = PagerItem()
            page.tag = i + 10
            if i == 0 {
                page.isSelected = true
            }
            page.originalImage = images[i]
            pages.append(page)
            
            page.selectAction = { tag in
                self.select(tag: tag)
            }
        }
        
        self.addSubviews(pages)
        
        pages.forEach { (page) in
            page.snp.makeConstraints({ (make) in
                make.height.equalTo(h)
                make.width.equalTo(w)
                make.top.equalTo(y)
                make.leading.equalTo(x)
                x = x + w
            })
        }
    }
    
    public func select(tag: Int) {
        self.pages.forEach { (page) in
            page.isSelected = false
            if page.tag == tag {
                page.isSelected = true
                self.selectedId = tag - 10
                self.onSelect?(self.selectedId)
            }
        }
    }
}
