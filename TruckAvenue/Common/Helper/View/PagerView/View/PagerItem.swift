//
//  PagerItem.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import SnapKit

public class PagerItem: UIView {
    
    public var isSelected: Bool = false {
        didSet {
            imageView.image = image
            if self.isSelected {
                underline.backgroundColor = .black
            } else {
                underline.backgroundColor = .clear
            }
        }
    }
    
    public var selectAction: ((Int) -> Void)?
    
    public var imageView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = ContentMode.scaleAspectFit
        return imgView
    }()
    
    public var originalImage: UIImage = UIImage(named: "star_empty")! {
        didSet {
            self.imageView.image = image
        }
    }
    
    public var image: UIImage {
        let img = originalImage
        if isSelected {
            return img.tintColor(color: UIColor.black)
        } else {
            return img.tintColor(color: UIColor.gray)
        }
    }
    
    public var underline: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(underline)
        self.addSubview(imageView)
        
        imageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.center.equalTo(self)
        }
        
        underline.snp.makeConstraints { (make) in
            make.width.equalTo(self)
            make.height.equalTo(2)
            make.bottom.equalTo(0)
        }
        
        self.imageView.image = self.image
        
        self.isSelected = false
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onSelect))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    @objc func onSelect() {
        self.isSelected = true
        self.selectAction?(self.tag)
    }
}

