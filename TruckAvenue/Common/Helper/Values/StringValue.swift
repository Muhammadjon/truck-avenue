//
//  StringValue.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class StringValue: Value {
    public var maxLength: Int = -1
    
    public var text: String {
        if hasError {
            return ""
        }
        
        return value
    }
    
    public override func onChangeValue() {
        isValid()
        
    }
    
    @discardableResult
    public override func isValid() -> Bool {
        hasError = false

        if value.count > maxLength && maxLength != -1 {
            hasError = true
        }
        
        if isRequired {
            if value.isEmpty {
                hasError = true
            }
        }
        
        return !hasError
    }
    
    public init(_ len: Int) {
        self.maxLength = len
    }
    
    public override init() {
        self.maxLength = -1
    }
}
