//
//  EmailValue.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class EmailValue: StringValue {
    
    public override init(_ len: Int) {
        super.init(len)
        
    }
    
    public override func onChangeValue() {
        super.onChangeValue()
        isValid()
    }

    @discardableResult
    public override func isValid() -> Bool {
        let valid = super.isValid()
        
        return valid && isValidEmail()
    }
    
    public func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        let isOKEmail = regex.firstMatch(in: text, options: [], range: NSRange(location: 0, length: text.count)) != nil
        logger.log(message: "isOK Email \(isOKEmail)")
        return isOKEmail
    }
}
