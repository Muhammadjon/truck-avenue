//
//  CardNumberValue.swift
//  TruckAvenue
//
//  Created by muhammadjon on 25/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class CardNumberValue: StringValue {
    
    public override init(_ len: Int) {
        super.init(len)
        
    }
    
    public override func onChangeValue() {
        super.onChangeValue()
        isValid()
    }
    
    @discardableResult
    public override func isValid() -> Bool {
        let valid = super.isValid()
        
        return valid && isValidCardNumber()
    }
    
    public func isValidCardNumber() -> Bool {
        return self.value.removeSpaces().count == 16
    }
}
