//
//  Value.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class Value {
    public var value: String = "" {
        didSet {
            if value == "" {
                hasError = false
            }
            self.onChangeValue()
        }
    }
    
    public var isRequired: Bool = false
    
    public var hasError: Bool = false
    
    public func onChangeValue() {
        
    }
    
    @discardableResult
    public func isValid() -> Bool {
        return true
    }
}
