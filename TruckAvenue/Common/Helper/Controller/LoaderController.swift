//
//  MAlertController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class LoaderController: BaseTransparentController {
    
    private let loaderView: LoaderView = LoaderView()
    private let titleLabel: UILabel = UILabel()
    
    private let bgView: UIView = UIView()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.background.addSubview(bgView)
        
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        
        bgView.backgroundColor = .white
        bgView.layer.cornerRadius = 4
        titleLabel.text = "Loading ..."
        self.bgView.addSubviews([loaderView, titleLabel])
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var h: CGFloat = self.view.height / 3
        var w: CGFloat = 2 * self.view.width / 3
        var x: CGFloat = (self.view.width - w) / 2
        var y: CGFloat = (self.view.height - h) / 2
        
        self.bgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = 0
        y = 4
        w = self.bgView.width
        h = self.bgView.height / 4
        self.loaderView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.loaderView.maxY + 4
        h = 30
        self.titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
//        self.titleLabel.sizeToFit()
        self.bgView.height = self.titleLabel.maxY
    }
    
    public override func presentOnController(_ controller: UIViewController) {
        super.presentOnController(controller)
        self.loaderView.start()
    }

    public func finish(_ isOK: Bool, completion: @escaping (() -> ())) {
        self.loaderView.finish(isOK)
        self.loaderView.onFinish = {
            self.dismiss(animated: true, completion: completion)
        }
    }
}
