//
//  ImageViewerController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 07/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class ImageViewerController: UIViewController {
    private let imageView: UIImageView = UIImageView()
    private let bottomView: UIView = UIView()
    private let nameLabel: UILabel = UILabel()
    private let topView: UIView = UIView()
    private let backImageView: UIImageView = UIImageView()
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.addSubview(imageView)
        self.addSubviews([topView, bottomView])
        self.bottomView.addSubview(nameLabel)
        self.topView.addSubview(backImageView)
        self.view.backgroundColor = theme.backgroudColor
//        topView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
//        bottomView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        self.imageView.contentMode = .scaleAspectFit
        self.backImageView.image = #imageLiteral(resourceName: "icon_back").tintColor(color: UIColor.black)
        self.backImageView.contentMode = .scaleAspectFit
        self.nameLabel.textColor = .black
        self.nameLabel.text = ""
        self.nameLabel.textAlignment = .left
        
        self.backImageView.onClick(target: self, selector: #selector(onBackClick))
    }
    
    @objc func onBackClick() {
        self.dismiss(animated: true) {
            logger.log(message: "dismissed")
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public func setImage(_ image: UIImage) {
        self.imageView.image = image
    }
    
    public func setImage(url image: String) {
        self.imageView.download(urlString: image)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageView.frame = self.view.bounds
        
        var y: CGFloat = self.topHeight
        var x: CGFloat = 0
        var h: CGFloat = 50
        var w: CGFloat = self.view.width
        
        self.topView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = 8
        y = 10
        h = 20
        w = 20
        self.backImageView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.view.height - 50
        x = 0
        w = self.view.width
        h = 50
        self.bottomView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = 5
        x = 16
        w = self.view.width - 2 * x
        h = 40
        self.nameLabel.frame = CGRect(x: x, y: y, width: w, height: h)
    }
}
