//
//  BaseTransparentController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 29/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

open class BaseTransparentController: UIViewController {
    
    public typealias Completion = (() -> Void)
    
    open var transparencyColor: UIColor = UIColor.black.withAlphaComponent(0.4)
    
    fileprivate var presentCompletion: Completion?
    fileprivate var dismissCompletion: Completion?
    
    public let background = UIView()
    
    open var hidesBackgroundTap: Bool? {
        didSet {
            if let _ = hidesBackgroundTap {
                self.background.isUserInteractionEnabled = true
                self.background.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BaseTransparentController.customDismiss)))
            } else {
                self.background.isUserInteractionEnabled = false
                self.background.gestureRecognizers?.removeAll()
            }
        }
    }
    
    open var transparency: CGFloat {
        get {
            return self.background.alpha
        }
        set {
            self.background.alpha = newValue
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.modalPresentationStyle = .overCurrentContext
        self.initialize()
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateFrame()
    }
    
    fileprivate func initialize() {
        background.isUserInteractionEnabled = false
        background.backgroundColor = .clear
        
        self.view.backgroundColor = .clear
        self.view.addSubview(background)
        
    }
    
    fileprivate func updateFrame() {
        //        self.background.frame = self.view.bounds
        //        self.background.y = self.view.maxY
    }
    
    open func callPresentCompletion(_ presentCompletion: @escaping Completion) {
        self.presentCompletion = presentCompletion
    }
    
    open func callDismissCompletion(_ dismissCompletion: @escaping Completion) {
        self.dismissCompletion = dismissCompletion
    }
    
    open func presentOnController(_ controller: UIViewController) {
        self.background.frame = self.view.bounds
        self.background.y = self.view.maxY
        self.background.frame = .zero
        //        self.background.x = self.view.width / 2
        //        self.background.y = self.view.height / 2
        
        controller.present(self, animated: false) {
            
            self.presentCompletion?()
            
            DispatchQueue.main.async {
                self.presentAnimation()
            }
        }
    }
    
    private func presentAnimation() {
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        UIView.transition(with: self.background, duration: 0.3, options: .beginFromCurrentState, animations: {
            
            self.view.layoutIfNeeded()
            self.view.setNeedsDisplay()
            
            self.background.frame = self.view.bounds
            
            self.view.backgroundColor = self.transparencyColor
        }) { (isOK) in
            
            print("presentation finished")
        }
    }
    
    private func hideAnimation(_ completion: @escaping (() -> ())) {
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        UIView.transition(with: self.background, duration: 0.3, options: .beginFromCurrentState, animations: {
            
            self.view.layoutIfNeeded()
            self.view.setNeedsDisplay()
            self.background.frame = .zero
            self.view.backgroundColor = UIColor.clear
        }) { (isOK) in
            completion()
            print("dismiss finished")
        }
    }
    
    open override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.hideAnimation {
            DispatchQueue.main.async {
                super.dismiss(animated: false, completion: completion)
                self.dismissCompletion?()
            }
        }
    }
    
    @objc open func customDismiss() {
        self.dismissCompletion?()
        self.dismissWithCompletion(true, completion: nil)
    }
    
    open func dismissWithCompletion(_ animated: Bool, completion: (() -> Void)?) {
        self.dismiss(animated: animated, completion: completion)
    }
    
}
