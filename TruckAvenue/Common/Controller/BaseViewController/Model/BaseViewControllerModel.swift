//
//  BaseViewControllerModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class BaseViewControllerModel {
    public init() {
        
    }
    
    public func loadInput(_ completion: @escaping (() -> Void)) {
        
    }
}
