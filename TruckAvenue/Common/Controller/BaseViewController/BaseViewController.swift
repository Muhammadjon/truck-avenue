//
//  BaseViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class BaseViewController: UIViewController {
    public var theme: Theme = appDelegate.model.themeManager.currentTheme()
    public var sizeCalculator = SizeCalculator.shared
    
    internal let scrollView: UIScrollView = UIScrollView()
    
    public let pickerView: UIPickerView = UIPickerView()
    private var tapGesture: UITapGestureRecognizer?
    
    public var shouldHideKeyboardOnTapView: Bool = true {
        didSet {
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapView(_:)))
            tapGesture?.delegate = self
            if let tapGesture = self.tapGesture {
                if self.shouldHideKeyboardOnTapView {
                    self.view.addGestureRecognizer(tapGesture)
                } else {
                    self.view.removeGestureRecognizer(tapGesture)
                }
            }
        }
    }
    
    public var scrollViewEnabled: Bool = false {
        didSet {
            scrollView.isHidden = !scrollViewEnabled
            if scrollView.isHidden {
                scrollView.frame = .zero
            } else {
                scrollView.frame = self.view.bounds
            }
            self.view.setNeedsLayout()
        }
    }
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.enableBackSwipe()
        
        self.initialize()
    }
    
    @objc func onTapView(_ gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    public func initialize() {
        self.view.backgroundColor = theme.backgroudColor
        self.view.addSubview(scrollView)
        appDelegate.route.navigationController?.interactivePopGestureRecognizer?.delegate = self
        appDelegate.route.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.initializeData()
    }
    
    public func initializeData() {
        
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    internal func addToScrollView(_ view: UIView) {
        self.scrollView.addSubview(scrollView)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.scrollView.frame = self.view.bounds
    }
    
    @objc public func pop() {
        appDelegate.route.navigationController?.popViewController(animated: true)
    }
    
    @objc public func onKeyboardShow(_ notification: Notification) {
        
    }
    
    @objc public func onKeyboardHide(_ notification: Notification) {
        
    }
}

extension BaseViewController: UIGestureRecognizerDelegate {
    internal func enableBackSwipe() {
        appDelegate.route.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    internal func disableBackSwipe() {
        appDelegate.route.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

}


extension BaseViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row)"
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("selected: \(row)")
    }
}
