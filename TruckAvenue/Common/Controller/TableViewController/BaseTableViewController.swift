//
//  TableViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class BaseTableViewController<ModelItem, CellItem>: UITableViewController where ModelItem: BaseTableViewItemInputModel, CellItem: BaseTableViewItemCell<ModelItem> {
    
    internal let footerView = UIView()
    
    internal let theme: Theme = ThemeManager.shared.currentTheme()
    
    public var items: [ModelItem] = [] {
        didSet {
            if items.count == 0 {
                self.tableView.tableFooterView = UIView()
            } else {
                if footerView.viewWithTag(1234) == nil {
                    let shadowView = UIView(x: 0, y: -4, w: self.tableView.width, h: 20)
                    shadowView.backgroundColor = self.theme.appWhite
                    shadowView.addShadow(shadowColor: theme.appShadow, radius: 1, width: 0, height: 3, opacity: 0.4)
                    shadowView.tag = 1234
                    footerView.height = 60
                    footerView.addSubview(shadowView)
                }
                
                footerView.backgroundColor = self.theme.backgroudColor
                self.tableView.tableFooterView = footerView
            }
        }
    }

    internal let cellIdentifier = "item_cell"
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CellItem.self, forCellReuseIdentifier: self.cellIdentifier)
        
        self.tableView.backgroundColor = theme.appWhite
        
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        self.tableView.separatorColor = theme.separatorGray
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = theme.backgroudColor
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellItem
        let item = items[indexPath.row]
        cell?.setModel(item)
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
