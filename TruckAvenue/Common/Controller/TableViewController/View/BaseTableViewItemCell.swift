//
//  BaseTableViewItemCell.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class BaseTableViewItemCell<T>: UITableViewCell where T: BaseTableViewItemInputModel {
    private var input: T? = nil
    
    public func setModel(_ model: T) {
        self.input = model
    }
}
