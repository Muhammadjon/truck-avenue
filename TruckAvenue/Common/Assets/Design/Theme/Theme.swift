//
//  Theme.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public enum Theme: Int {
    case theme1, theme2
    
    //D9D9D9
    var separatorGray: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "D9D9D9")
        case .theme2:
            return UIColor().color(hex: "D7D7D7")
        }
    }
    
    var appOrange: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "DE9C47")
        case .theme2:
            return UIColor().color(hex: "DE9E4A")
        }
    }
    
    var appStatusBarColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "F3F3F3")
        case .theme2:
            return UIColor().color(hex: "F3F3F3")
        }
    }
    
    var appShadow: UIColor {
        
        switch self {
        case .theme1:
            return UIColor.black.withAlphaComponent(0.3)
        case .theme2:
            return UIColor.black.withAlphaComponent(0.1)
        }
    }
    
    var appFont: UIFont {
        switch self {
        case .theme1:
            return UIFont.systemFont(ofSize: 12)
        case .theme2:
            return UIFont.systemFont(ofSize: 12)
        }
    }
    
    var cardFont: UIFont {
        switch self {
        case .theme1:
            return UIFont(name: "ocr-aregular", size: 16) ?? UIFont(name: "OCRA", size: 16) ?? appFont
        case .theme2:
            return UIFont(name: "ocr-aregular", size: 16) ?? UIFont(name: "OCRA", size: 16) ?? appFont
        }
    }
    
    var appFontBold: UIFont {
        switch self {
        case .theme1:
            return UIFont.boldSystemFont(ofSize: 12)
        case .theme2:
            return UIFont.boldSystemFont(ofSize: 12)
        }
    }
    
    var appWhite: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "ffffff")
        case .theme2:
            return UIColor().color(hex: "fafafa")
        }
    }
    
    var appBlack: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "363636")
        case .theme2:
            return UIColor().color(hex: "000000")
        }
    }
    
    var backgroudColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "f4f4f4")
        case .theme2:
            return UIColor().color(hex: "fafafa")
        }
    }
    
    var mainColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "ffffff")
        case .theme2:
            return UIColor().color(hex: "000000")
        }
    }
    
    var textColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "000000").withAlphaComponent(0.87)
        case .theme2:
            return UIColor().color(hex: "363636")
        }
    }
    
    var cellDateColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "000000").withAlphaComponent(0.48)
        case .theme2:
            return UIColor().color(hex: "000000").withAlphaComponent(0.48)
        }
    }
    
    var bottomBarSelectedColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "135BB9")
        case .theme2:
            return UIColor().color(hex: "135BB9").withAlphaComponent(0.8)
        }
    }
    
    var bottomBarColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "ffffff")
        case .theme2:
            return UIColor().color(hex: "ffffff")
        }
    }
    
    var disabledButtonBackgroundGradientColor: (left: UIColor, right: UIColor) {
        switch self {
        case .theme1:
            let left = UIColor().color(hex: "c2c2c2")
            let right = UIColor().color(hex: "a1a1a1")
            return (left, right)
        case .theme2:
            let left = UIColor().color(hex: "c2c2c2")
            let right = UIColor().color(hex: "a1a1a1")
            return (left, right)
        }
    }
    
    var buttonBackgroundGradientColor: (left: UIColor, right: UIColor) {
        switch self {
        case .theme1:
            let left = UIColor().color(hex: "1D6BF2")
            let right = UIColor().color(hex: "1318A7")
            return (left, right)
        case .theme2:
            let left = UIColor().color(hex: "fafafa")
            let right = UIColor().color(hex: "1D6BF2")
            return (left, right)
        }
    }
    
    var buttonTextColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "ffffff")
        case .theme2:
            return UIColor().color(hex: "135BB9")
        }
    }
    
    var bigBlueTextColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "135BB9")
        case .theme2:
            return UIColor().color(hex: "ffffff")
        }
    }
    
    var inputFieldTitleFont: UIFont {
        switch self {
        case .theme1:
            return UIFont.systemFont(ofSize: 12)
        case .theme2:
            return UIFont.systemFont(ofSize: 10)
        }
    }
    
    var inputFieldFont: UIFont {
        return inputFieldTitleFont.withSize(14)
    }
    
    var inputFieldTitleColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "135BB9")
        case .theme2:
            return UIColor().color(hex: "ffffff")
        }
    }
    
    var inputFieldUnderlineColor: UIColor {
        return inputFieldTitleColor
    }
    
    var inputFieldDescriptionColor: UIColor {
        return inputFieldTitleColor
    }
    
    var inputFieldBackgroundColor: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "000000").withAlphaComponent(0.07)
        case .theme2:
            return UIColor().color(hex: "000000").withAlphaComponent(0.02)
        }
    }
    
    var inputFieldErrorColor: UIColor {
        switch self {
        case .theme1:
            return UIColor.red.withAlphaComponent(0.7)
        case .theme2:
            return UIColor.red.withAlphaComponent(0.8)
        }
    }
    
    var labelBigBold: UIFont {
        switch self {
        case .theme1:
            return UIFont.boldSystemFont(ofSize: 28)
        case .theme2:
            return UIFont.boldSystemFont(ofSize: 20)
        }
    }
    
    var labelGray: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "8D8D8D")
        case .theme2:
            return UIColor.gray
        }
    }
    
    var iconGray: UIColor {
        switch self {
        case .theme1:
            return UIColor().color(hex: "727272")
        case .theme2:
            return UIColor.lightGray
        }
    }
    
    var iconBlue: UIColor {
        return self.bottomBarSelectedColor
    }
}
