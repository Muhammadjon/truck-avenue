//
//  ThemeManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation



public class ThemeManager {
    public let themeKey = "SelectedThemeKey"
    
    public static let shared: ThemeManager = ThemeManager()
    
    private init() {
        
    }
    
    public func currentTheme() -> Theme {
        let storedTheme = UserDefaults.standard.integer(forKey: self.themeKey)
        let thm = Theme(rawValue: storedTheme)
        
        guard let theme = thm else {
            return .theme2
        }
        
        return theme
    }
    
    public func setCurrentTheme(_ theme: Theme) {
        UserDefaults.standard.set(theme.rawValue, forKey: themeKey)
        UserDefaults.standard.synchronize()
        
    }
}
