//
//  SizeCalculator.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class SizeCalculator {
    public static let shared: SizeCalculator = SizeCalculator()
    
    public var designSize: CGSize = CGSize(width: 360, height: 640)
    public let deviceFrame: CGRect = UIScreen.main.bounds
    
    private init() {
        
    }
    
    public func width(from w: CGFloat) -> CGFloat {
        return (w / designSize.width) * deviceFrame.width
    }
    
    public func height(from h: CGFloat) -> CGFloat {
        return (h / designSize.height) * deviceFrame.height
    }
}
