//
//  FavoriteManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 16/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class FavoriteManager: MManager {
    public static var shared = FavoriteManager()
    
    public typealias Model = FavoriteItem
    
    public var favoriteParkingList: [ParkingItemResponseModel] = []
    
    private init() {
        
    }
    
    public var items: [FavoriteItem] = []
    
    public func insert(item: FavoriteItem) {
        self.items.append(item)
    }
    
    public func find(parkingId id: Int) -> FavoriteItem? {
        var result: FavoriteItem?
        items.forEach { (f) in
            if f.parkingId == id {
                result = f
            }
        }
        return result
    }
    
    public func deleteAll() -> Bool {
        self.items.removeAll()
        return true
    }
}
