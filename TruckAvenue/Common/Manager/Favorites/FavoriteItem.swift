//
//  FavoriteItem.swift
//  TruckAvenue
//
//  Created by muhammadjon on 16/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class FavoriteItem: MManagerItem {
    public var id: Int
    public var parkingId: Int
    
    public init(id: Int, parkingId: Int) {
        self.id = id
        self.parkingId = parkingId
    }
}
