//
//  TruckType.swift
//  TruckAvenue
//
//  Created by muhammadjon on 12/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class TruckType {
    public var id: Int?
    public var name: String?
    
    public func wrap(_ model: TruckTypeResponseItem) {
        self.id = model.id
        self.name = model.model
    }
}
