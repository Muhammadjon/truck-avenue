//
//  TruckTypeManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 12/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class TruckTypeManager: MManager {
    public typealias Model = TruckType
    public var items: [Model] = []
    
    public static let shared: TruckTypeManager = TruckTypeManager()
    
    private init() {
        
    }
    
    public func hasTypes() -> Bool {
        return !items.isEmpty
    }
    
    public func delete(id: Int) {
        _ = items.removeIf { (item) -> Bool in
            return item.id == id
        }
    }
    
    public func insert(item: TruckType) {
        items.append(item)
    }
}
