//
//  CardListItemInput.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public enum CardType: String {
    case visa = "visa"
    case paypal = "paypal"
    
    public static func identify(_ t: String) -> CardType {
        if t == "visa" {
            return .visa
        }
        
        return .paypal
    }
}

public class CardListItemInput: MManagerItem {
    public let id: Int
    public let cardholderName, number, expMonth, expYear: String
    public let cvv: String
    
    public var cardId: String?
    public var token: String?
    public var clientApi: String?
    public var type: CardType = .visa
    
    public var formattedNumber: String {
        if number.count == 16 {
            return number.replace(range: 6..<11, with: "••••••").separate(every: 4, with: " ")
        }
        return "0000 00•• •••• 0000"
    }
    
    public var fomattedMiniNumber: String {
        if number.count == 16 {
            return "•• \(number[12..<16])"
        }
        return "•• 0000"
    }
    
    public init(id: Int, cardholderName: String, number: String, expMonth: String, expYear: String, cvv: String) {
        self.id = id
        self.cardholderName = cardholderName
        self.number = number
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvv = cvv
    }
    
    public convenience init(model: CardNetworkResponseItem) {
        self.init(id: model.id ?? 0, cardholderName: model.cardholderName ?? "", number: model.number ?? "", expMonth: model.expMonth ?? "", expYear: model.expYear ?? "", cvv: model.cvv ?? "")
        self.type = CardType.identify(model.type ?? "visa")
        self.setExtra(model.cardid ?? "", model.token ?? "", model.clientip ?? "")
    }
    
    public func setExtra(_ cId: String, _ token: String, _ cApi: String) {
        self.cardId = cId
        self.token = token
        self.clientApi = cApi
    }
}
