//
//  CardManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class CardManager: MManager {
    public static let shared: CardManager = CardManager()
    
    public typealias Model = CardListItemInput
    
    private init() {
        
    }
    
    public var items: [CardListItemInput] = []
    
    public func insert(item: CardListItemInput) {
        self.items.append(item)
    }
    
    public func delete(id: Int) -> Bool {
        return self.items.removeIf({ (item) -> Bool in
            return item.id == id
        }) != nil
    }
    
    public func deleteAll() -> Bool {
        self.items.removeAll()
        return true
    }
}
