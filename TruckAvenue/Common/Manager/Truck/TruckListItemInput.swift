//
//  TruckListItemModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class TruckListItemInput: MManagerItem {
    public var id: Int
    public var image: UIImage?
    public var imageURL: String?
    public var title: String?
    public var model: String?
    public var isMain: Bool = false
    
    public var fullURL: String {
        return "\(ApplicationNetwork.baseAPI)\(imageURL ?? "")"
    }
    
    public init(id: Int) {
        self.id = id
    }
    
    public func setItems(_ m: TruckItemResponseModel) {
        self.imageURL = m.photo
//        self.image = nil
        self.isMain = m.isActive ?? false
        self.model = m.model
        self.title = m.truckNumber ?? ""
    }
}
