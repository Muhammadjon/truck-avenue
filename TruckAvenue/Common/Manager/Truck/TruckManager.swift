//
//  TruckManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 12/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class TruckManager: MManager {
    public typealias Model = TruckListItemInput
    
    public var items: [Model] = []
    
    public var hasTrucks: Bool {
        return !items.isEmpty
    }
    
    public static let shared: TruckManager = TruckManager()
    
    private init() {
        
    }
    
    public func insert(item: Model) {
        self.items.append(item)
    }
    
    public func deleteAll() -> Bool {
        self.items.removeAll()
        return true
    }
    
    public func delete(id: Int) {
        _ = self.items.removeIf { (item) -> Bool in
            return item.id == id
        }
    }
    
    public func makeMain(_ id: Int) {
        self.items.forEach { (item) in
            item.isMain = item.id == id
        }
    }
    
    public func update(item: TruckListItemInput) {
        self.items.forEach { (i) in
            if i.id == item.id {
                i.imageURL = item.imageURL
                i.isMain = item.isMain
                i.model = item.model
                i.title = item.title
            }
        }
    }
    
    public func append(response: TruckItemResponseModel) {
        guard let id = response.id else {
            return
        }
        
        let item = TruckListItemInput(id: id)
        item.title = response.model ?? ""
        item.model = response.truckNumber ?? ""
        item.imageURL = response.photo ?? ""
        item.image = #imageLiteral(resourceName: "no_imagephoto_icon")
        item.isMain = response.isActive ?? false
        items.append(item)
    }
    
    public func sort() {
        if let main = self.items.removeIf({$0.isMain}) {
            self.items.insert(main, at: 0)
        }
    }
}
