//
//  HistoryItem.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class HistoryItemInput: MManagerItem {
    public var model: HistoryItem
    
    public init(_ model: HistoryItem) {
        self.model = model
    }
}
