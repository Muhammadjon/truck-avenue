//
//  HistoryManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/07/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class HistoryManager: MManager {
    public typealias Model = HistoryItemInput
    
    public var shouldUpdate: Bool = true
    
    public static let shared: HistoryManager = HistoryManager()
    
    public var items: [HistoryItemInput] = []
    
    public var reservations: [HistoryItemInput] {
        return items.filter({$0.model.type == .approved || $0.model.type == .new})
    }
    
    public var history: [HistoryItemInput] {
        return items.filter({$0.model.type == .finished})
    }
    
    public func insert(item: HistoryItemInput) {
        self.items.append(item)
    }
    
    @discardableResult
    public func deleteAll() -> Bool {
        self.items.removeAll()
        return true
    }
}
