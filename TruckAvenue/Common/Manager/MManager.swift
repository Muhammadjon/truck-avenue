//
//  MManager.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public protocol MManager {
    associatedtype Model
    var items: [Model] {get set}
    func insert(item: Model)
    func update(item: Model)
    func delete(id: Int)
    func getAll() -> [Model]
    @discardableResult
    func deleteAll() -> Bool
}

extension MManager {
    
    public func insert(item: Model) {
        
    }
    
    public func update(item: Model) {
        
    }
    
    public func delete(id: Int) {
        
    }
    
    public func getAll() -> [Model] {
        return self.items
    }
    
    @discardableResult
    public func deleteAll() -> Bool {
        return false
    }
}
