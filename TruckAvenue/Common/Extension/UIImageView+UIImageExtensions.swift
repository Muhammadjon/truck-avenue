//
//  UIImageView+UIImageExtensions.swift
//  TruckAvenue
//
//  Created by muhammadjon on 10/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    public func download(urlString: String, placeholder: UIImage? = nil, withCache: Bool = true, completion: ((Bool) -> ())? = nil) {
        
        let cache = ImageCache.default
        
        let url = URL(string: urlString.encodeUrl)
        if let url = url {
            
            var resource = ImageResource(downloadURL: url, cacheKey: urlString)
            
            if !withCache {
                resource = ImageResource(downloadURL: url)
            }
            
            self.kf.setImage(with: resource, placeholder: placeholder, options: nil, progressBlock: nil) { (image, error, cacheType, imageURL) in
                
                completion?(image != nil)
                guard let image = image else {
                    return
                }
                
                if cache.imageCachedType(forKey: urlString).cached {
                    return
                }
                
                if withCache {
                    cache.store(image, forKey: urlString)
                }
            }
        } else {
            completion?(false)
        }
    }
    
    public func saveImage(_ name: String) {
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last ?? "no app directory"
        let directory = "\(path)/qrImages/"
        
        if !fileManager.fileExists(atPath: directory) {
            fileManager.createFile(atPath: directory, contents: nil, attributes: nil)
        }
        
        let filePath = "\(directory)\(name).png"
        
        if !fileManager.fileExists(atPath: filePath) {
            do {
                if let data = self.asImage().pngData() {
                    try data.write(to: URL(fileURLWithPath: filePath))
                }
            } catch let error {
                print(error)
            }
        }
    }
    
}

extension UIImage {
    
    func tintColor(color color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func withInsets(_ insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: size.width + insets.left + insets.right,
                   height: size.height + insets.top + insets.bottom),
            false,
            self.scale)
        
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imageWithInsets
    }
    
    func resizeImage(_ newSize: CGSize) -> UIImage? {
        func isSameSize(_ newSize: CGSize) -> Bool {
            return size == newSize
        }
        
        func scaleImage(_ newSize: CGSize) -> UIImage? {
            func getScaledRect(_ newSize: CGSize) -> CGRect {
                let ratio   = max(newSize.width / size.width, newSize.height / size.height)
                let width   = size.width * ratio
                let height  = size.height * ratio
                return CGRect(x: 0, y: 0, width: width, height: height)
            }
            
            func _scaleImage(_ scaledRect: CGRect) -> UIImage? {
                UIGraphicsBeginImageContextWithOptions(scaledRect.size, false, 0.0);
                draw(in: scaledRect)
                let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
                UIGraphicsEndImageContext()
                return image
            }
            return _scaleImage(getScaledRect(newSize))
        }
        
        return isSameSize(newSize) ? self : scaleImage(newSize)!
    }
}
