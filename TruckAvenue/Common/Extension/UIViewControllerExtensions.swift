//
//  UIViewControllerExtension.swift
//  TruckAvenue
//
//  Created by muhammadjon on 31/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

extension UIViewController {
    var spinnerViewTag: Int { return 999999 }
    public func addSubview(_ v: UIView) {
        self.view.addSubview(v)
    }
    
    public func addSubviews(_ views: [UIView]) {
        views.forEach { (view) in
            self.view.addSubview(view)
        }
    }
    
    public var topHeight: CGFloat {
        var res = CGFloat(0)
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            res = (topPadding ?? 23)
        }
        if let c = self.navigationController {
            if !c.isNavigationBarHidden {
                res = res + 44
            }
        }
        return res
    }
    
    public var bottomHeight: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom
            return bottomPadding ?? 0
        }
        return 0
    }
    
    var spinnerView: UIView {
        get {
            let v = UIView()
            if let spinner = findSpinnerView() {
                
                return spinner
            }
            v.tag = self.spinnerViewTag
            
            self.view.addSubview(v)
            return v
        }
    }
    
    public var spinnerTitle: String? {
        set {
            DispatchQueue.main.async {
                if let spinner = self.findSpinnerView() {
                    spinner.subviews.forEach { (view) in
                        if view.tag == 1001 {
                            let textLabel = view as! UILabel
                            textLabel.text = newValue
                            return
                        }
                    }
                }
            }
        }
        get {
            var text: String?
            
            if let spinner = findSpinnerView() {
                spinner.subviews.forEach { (view) in
                    if view.tag == 1001 {
                        let textLabel = view as! UILabel
                        text = textLabel.text
                        return
                    }
                }
            }
            return text
        }
    }
    
    func showResultableSpinner(text: String) -> (body: UIView, loaderView: LoaderView)? {
        let height = self.navigationController?.navigationBar.height
        let y = self.view.y - (height ?? 0)
        let h = self.view.height + (height ?? 0)
        
        
        let ai = loaderBodyBuilder2(withTitle: text)
        
        self.spinnerView.frame = CGRect(x: 0, y: y, width: self.view.width, height: h)
        
        ai.center = self.spinnerView.center
        
        self.spinnerView.addSubview(ai)
        
        self.view.bringSubviewToFront(self.spinnerView)
        if let loader = ai.viewWithTag(122) as? LoaderView {
            loader.start()
            return (body: ai, loaderView: loader)
        }
        return nil
    }
    
    private func loaderBodyBuilder2(withTitle: String) -> UIView {
        let w = self.view.width * 0.4
        let h = w * 0.7
        
        let bodyView = UIView(x: 0, y: 0, w: w, h: h)
        let ai = LoaderView()
        let titleLabel = UILabel()
        
        titleLabel.text = withTitle
        titleLabel.tag = 1001
        bodyView.backgroundColor = .white
        
        ai.tag = 122
        ai.height = bodyView.height / 3
        ai.width = bodyView.width
        ai.y =  4
        ai.x = 0
        
        titleLabel.height = bodyView.height - (ai.maxY + 4)
        titleLabel.y = ai.maxY + 4
        titleLabel.width = bodyView.width - bodyView.width * 0.2
        titleLabel.x = bodyView.width * 0.1
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(ai)

        ai.center = bodyView.center

        bodyView.layer.cornerRadius = 6
        
        return bodyView
    }
    
    func showSpinner(text: String) {
        let height = self.navigationController?.navigationBar.height
        let y = self.view.y - (height ?? 0)
        let h = self.view.height + (height ?? 0)
        
        
        let ai = loaderBodyBuilder(withTitle: text)
        
        DispatchQueue.main.async {
            self.spinnerView.frame = CGRect(x: 0, y: y, width: self.view.width, height: h)
            
            ai.center = self.spinnerView.center
            
            self.spinnerView.addSubview(ai)
            
            self.view.bringSubviewToFront(self.spinnerView)
        }
    }
    
    func removeSpinner(_ completion: (() -> ())? = nil) {
        DispatchQueue.main.async {
            if let spinner = self.findSpinnerView() {
                self.view.willRemoveSubview(spinner)
                spinner.removeFromSuperview()
                completion?()
            }
        }
    }
    
    @discardableResult
    public func addRightMenu(title: String, image: UIImage?, selector: Selector, target: Any?) -> UIBarButtonItem {
        let rightAddBarButtonItem: UIBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.done, target: target, action: selector)
        rightAddBarButtonItem.image = image
        
        var items = self.navigationItem.rightBarButtonItems ?? []
        items.append(rightAddBarButtonItem)
        
        self.navigationItem.setRightBarButtonItems(items, animated: true)
        return rightAddBarButtonItem
    }
    
    @discardableResult
    public func addRightMenu(view: UIView, selector: Selector, target: Any?) -> UIBarButtonItem {
        let tapGesture = UITapGestureRecognizer(target: target, action: selector)
        view.addGestureRecognizer(tapGesture)
        let rightAddBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: view)
        
        var items = self.navigationItem.rightBarButtonItems ?? []
        items.append(rightAddBarButtonItem)
        
        self.navigationItem.setRightBarButtonItems(items, animated: true)
        return rightAddBarButtonItem
    }
    
    
    private func loaderBodyBuilder(withTitle: String) -> UIView {
        let w = self.view.width * 0.4
        let h = w * 0.7
        
        let bodyView = UIView(x: 0, y: 0, w: w, h: h)
        let ai = UIActivityIndicatorView.init(style: .white)
        let titleLabel = UILabel()
        
        titleLabel.text = withTitle
        titleLabel.tag = 1001
        bodyView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        ai.startAnimating()
        
        ai.center = bodyView.center
        
        ai.y = bodyView.height * 0.2
        
        titleLabel.height = bodyView.height - (ai.maxY + bodyView.height * 0.2)
        titleLabel.y = ai.maxY + bodyView.height * 0.1
        titleLabel.width = bodyView.width - bodyView.width * 0.2
        titleLabel.x = bodyView.width * 0.1
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.white
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(ai)
        
        bodyView.layer.cornerRadius = 6
        
        return bodyView
    }
    
    private func findSpinnerView() -> UIView? {
        var sv: UIView?
        self.view.subviews.forEach { (v) in
            if v.tag == spinnerViewTag {
                sv = v
                return
            }
        }
        return sv
    }
    
    private func alert(title: String, message: String) -> UIAlertController {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        return UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    
    public func alertInfo(title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        let alert = self.alert(title: title, message: message)

        let action = UIAlertAction(title: "Ok", style: .default, handler: handler)
        action.setValue(UIColor.black, forKey: "titleTextColor")
        alert.addAction(action)
        
        DispatchQueue.main.async {
            if self.presentingViewController == nil {
                self.present(alert, animated: false, completion: nil)
            }
        }
    }
    
    public func bottomAlert(title: String, message: String, _ completion: @escaping ((Bool) -> ())) {
        
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default) { action in
            completion(true)
        }
        
        alertAction.setValue(UIColor.black, forKey: "titleTextColor")
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(false)
        }
        
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
        
        actionSheet.addAction(alertAction)
        actionSheet.addAction(cancelAction)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    public func alertInfo(title: String, message: String, action: @escaping ((Bool) -> ())) {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        let alert = self.alert(title: title, message: message)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { act in
            action(true)
        }
        okAction.setValue(UIColor.black, forKey: "titleTextColor")
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { act in
            action(false)
        }
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alert, animated: false, completion: nil)
        }
    }
}
