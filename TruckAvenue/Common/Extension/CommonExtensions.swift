//
//  CommonExtensions.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit
import MapKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}


extension UIColor {
    func color(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}


extension UITextField {
    
    public func doneAccessory(target: Any, selector: Selector, isEnabled: Bool = true) {
        if isEnabled {
            addDoneButtonOnKeyboard(target, selector)
        } else {
            if self.inputAccessoryView?.tag == 543 {
               self.inputAccessoryView = nil
            }
        }
    }
    
    @IBInspectable var doneAccessory: Bool {
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard(_ target: Any? = nil, _ selector: Selector? = nil)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        doneToolbar.tag = 543
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        var done: UIBarButtonItem?
        
        if let t = target, let s = selector {
            done = UIBarButtonItem(title: "Done", style: .done, target: t, action: s)
        } else {
            done = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        }

        let items = [flexSpace, done!]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension UILabel {
    func widthByText(height: CGFloat) -> CGFloat {
        return self.text?.width(withConstrainedHeight: height, font: self.font) ?? 0
    }
    
    func heightWith(width: CGFloat) -> CGFloat {
        return self.text?.height(withConstrainedWidth: width, font: self.font) ?? 0
    }
}

extension MKMapSnapshotter {
    public static func mapImage(longitude: String, lattitude: String, width: CGFloat, height: CGFloat, pinImage: UIImage? = nil, _ completion: @escaping ((UIImage?) -> Void)) {
        let annotation = MKPointAnnotation()
        let lat = CLLocationDegrees(exactly: Double(lattitude) ?? 0)
        let lon = CLLocationDegrees(exactly: Double(longitude) ?? 0)
        if let la = lat, let lo = lon {
            annotation.coordinate = CLLocationCoordinate2D(latitude: la, longitude: lo)
            mapImage(with: annotation, width: width, height: height, pinImage: pinImage, completion)
        }
    }
    
    public static func mapImage(with pin: MKAnnotation, width: CGFloat, height: CGFloat, pinImage: UIImage? = nil, _ completion: @escaping ((UIImage?) -> Void)) {
        let mapSnapshotOptions = self.Options()
        // Set the region of the map that is rendered.
        
        let location = CLLocationCoordinate2DMake(pin.coordinate.latitude, pin.coordinate.longitude) // Apple HQ
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
        
        mapSnapshotOptions.region = region
        
        mapSnapshotOptions.scale = UIScreen.main.scale
        
        mapSnapshotOptions.size = CGSize(width: width, height: height)
        
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
        
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        
        snapShotter.start(with: DispatchQueue.main, completionHandler: { (snapshot, error) in
            
            UIGraphicsBeginImageContextWithOptions(mapSnapshotOptions.size, true, 0)
            
            var point = snapshot!.point(for: pin.coordinate)
            let rect = CGRect(x: 0, y: 0, width: width, height: height)
            let pinView = MKPinAnnotationView(annotation: pin, reuseIdentifier: nil)
            
            if let pi = pinImage {
                pinView.image = pi
            }
            
            let pinImage = pinView.image
            
            snapshot?.image.draw(at: .zero)
            
            if rect.contains(point) {
                let pinCenterOffset = pinView.centerOffset
                point.x = point.x - pinView.bounds.size.width / 2
                point.y = point.y - pinView.bounds.size.height / 2
                point.x = point.x + pinCenterOffset.x
                point.y = point.y + pinCenterOffset.y
                pinImage?.draw(at: point)
            }
            
            let img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            DispatchQueue.main.async {
                completion(img)
            }
        })
    }
}



extension URLRequest {
    
    public enum HeaderField: String {
        case contentType = "Content-Type"
        case accept = "Accept"
        case authorization = "Authorization"
        case language = "Language"
    }
    
    public enum HeaderValue: String {
        case applicationJSON = "application/json"
        case token = "Token "
    }
    
    /**
     Sets value to header field
     
     - Parameter value: Header Value
     - Parameter field: Header Field
     
     */
    public mutating func set(value: HeaderValue,toField field: HeaderField) {
        self.setValue(value.rawValue, forHTTPHeaderField: field.rawValue)
    }
    
    /**
     Sets value to header field
     
     - Parameter value: Header Value
     - Parameter field: Header Field
     
     */
    public mutating func set(value: String, toField field: HeaderField) {
        self.setValue(value, forHTTPHeaderField: field.rawValue)
    }
    
    /**
     Gets value from content and sets it to header field
     
     - Parameter content: The dictionary
     - Parameter field: Header Field
     
     */
    public mutating func set(contentValue content: [String:String], toField field: HeaderField) {
        self.setValue(content[field.rawValue], forHTTPHeaderField: field.rawValue)
    }
    
}

extension Array {
    public mutating func removeIf(_ closure: ((_ item: Element) -> Bool)) -> Element? {
        for i in 0..<self.count {
            let item = self[i]
            if closure(item) {
                self.remove(at: i)
                return item
            }
        }
        return nil
    }
}


extension Date {
    
    public static func year(timeinterval interval: TimeInterval) -> Int {
        let formatter = DateFormatter(format: "YYYY")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        return Int(formatter.string(from: date)) ?? 0
    }
    
    public static func day(timeinterval interval: TimeInterval) -> Int {
        let formatter = DateFormatter(format: "dd")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        return Int(formatter.string(from: date)) ?? 0
    }
    
    public static func month(timeinterval interval: TimeInterval) -> Int {
        let formatter = DateFormatter(format: "MM")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        return Int(formatter.string(from: date)) ?? 0
    }
    
    public static var currentTime: String {
        let interval = Date.timeIntervalSinceReferenceDate
        let formatter = DateFormatter(format: "HH:mm:ss")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        
        return formatter.string(from: date)
    }
    
    public static var currentDate: String {
        let interval = Date.timeIntervalSinceReferenceDate
        let formatter = DateFormatter(format: "YYYY-MM-dd")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        
        return formatter.string(from: date)
    }
    
    public static var currentDay: String {
        let interval = Date.timeIntervalSinceReferenceDate
        let formatter = DateFormatter(format: "dd")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        
        return formatter.string(from: date)
    }
    
    public static var currentYear: String {
        let interval = Date.timeIntervalSinceReferenceDate
        let formatter = DateFormatter(format: "YYYY")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        
        return formatter.string(from: date)
    }
    
    public static var currentMonth: String {
        let interval = Date.timeIntervalSinceReferenceDate
        let formatter = DateFormatter(format: "MM")
        let date = Date(timeIntervalSinceReferenceDate: interval)
        
        return formatter.string(from: date)
    }
}


extension DateFormatter {
    public convenience init(format dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }
}

extension Date {
    public static func fromString(_ format: String, string: String) -> Date? {
        let dateFormatter = DateFormatter(format: format)
        dateFormatter.locale = .current
        return dateFormatter.date(from: string)
    }
    
    public func toString(format: String = "E, MMM dd") -> String {
        let dateFormatter = DateFormatter(format: format)
        dateFormatter.locale = .current
        return dateFormatter.string(from: self)
    }
}
