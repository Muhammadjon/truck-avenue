//
//  StringExtension.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    var encodeUrl : String
    {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
    var decodeUrl : String
    {
        return self.removingPercentEncoding!
    }
    
    func separate(every: Int, with separator: String) -> String {
        return String(stride(from: 0, to: Array(self).count, by: every).map {
            Array(Array(self)[$0..<min($0 + every, Array(self).count)])
            }.joined(separator: separator))
    }
    
    public func removeSpaces() -> String {
        return self.replacingOccurrences(of: "^\\s|\\s+|\\s$", with: "", options: .regularExpression)
    }
    
    /// ex: "{\"amount\":\"500\",\"clientid\":\"+9989xxxxxxxx\"}" to dictionary
    func convertToDictionary() -> [String: String]? {
        if let data = self.data(using: .utf8) {
            do {
                let objc = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                var result: [String: String] = [:]
                objc?.forEach({ (obj) in
                    result[obj.key] = "\(obj.value)"
                })
                
                return result
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    public func getDateAndTimeAsString(_ format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> (date: String, time: String) {
        let date = Date.fromString(format, string: self)
        
        let dateFormatter = DateFormatter(format: "dd.MM.yyyy")
        let timeFormatter = DateFormatter(format: "HH:mm")
        
        if let date = date {
            return (dateFormatter.string(from: date), timeFormatter.string(from: date))
        }
        return ("", "")
    }
    
    public subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    public subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    public subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start ..< end])
    }
    
    public func replace(range: Range<Int>, with str: String) -> String {
        let st = self[0..<range.lowerBound]
        let lt = self[range.upperBound + 1..<self.count]
        
        return "\(st)\(str)\(lt)"
    }
}

