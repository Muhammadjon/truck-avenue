
//
//  File.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

extension UIView {
    public func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
    
    public func asImage(frame frm: CGRect) -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: frm)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(frm.size)
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
    
    public func addShadow(shadowColor: UIColor, radius: CGFloat? = 2, width: CGFloat? = 0, height: CGFloat? = 3) {
        self.addShadow(shadowColor: shadowColor, radius: radius, width: width, height: height, opacity: 0.8)
    }
    
    public func addShadow(shadowColor: UIColor, radius: CGFloat? = 2, width: CGFloat? = 0, height: CGFloat? = 3, opacity: Float) {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = radius!
        self.layer.shadowOpacity = opacity
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: width!, height: height!)
    }

}

extension UIView {
    
    convenience init(x: CGFloat, y: CGFloat, w: CGFloat, h: CGFloat) {
        self.init(frame: CGRect(x: x, y: y, width: w, height: h))
    }
    
    public func addSubviews(_ views: [UIView]) {
        views.forEach { (view) in
            self.addSubview(view)
        }
    }
    
    public func addSubviewWithAnimation(_ view: UIView, duration: TimeInterval = 0.3, options: UIView.AnimationOptions, _ completion: ((Bool) -> Void)? = nil) {
        UIView.transition(with: view, duration: duration, options: options, animations: {
            self.addSubview(view)
        }) { (isFinished) in
            completion?(isFinished)
        }
    }
    
    public func removeFromSuperviewWithAnimation(duration: TimeInterval = 0.3, options: UIView.AnimationOptions, _ completion: ((Bool) -> Void)? = nil) {
        UIView.transition(with: self, duration: duration, options: options, animations: {
            self.removeFromSuperview()
        }) { (isFinished) in
            completion?(isFinished)
        }
    }

    public func addGradient(left topColor: UIColor, right bottomColor: UIColor, _ p1: CGPoint, _ p2: CGPoint, locations: [NSNumber] = [NSNumber(floatLiteral: 0.0), NSNumber(floatLiteral: 1.0)]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = p1
        gradientLayer.endPoint = p2
        gradientLayer.locations = locations
        gradientLayer.frame = self.bounds
        
        if let oldLayer = layer.sublayers?.firstIndex(where: {$0.name == "cardGradient"}) {
            layer.sublayers?.remove(at: oldLayer)
        }
        gradientLayer.name = "cardGradient"
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    public func addGradient(left topColor: UIColor, right bottomColor: UIColor) {
        self.addGradient(left: topColor, right: bottomColor, CGPoint(x: 0, y: 0.5), CGPoint(x: 1, y: 0.5))
    }
    
    public var height: CGFloat {
        set {
            let x = self.frame.origin.x
            let y = self.frame.origin.y
            let w = self.frame.width
            self.frame = CGRect(x: x, y: y, width: w, height: newValue)
        }
        get {
            return self.frame.height
        }
    }
    
    public var width: CGFloat {
        set {
            let x = self.frame.origin.x
            let y = self.frame.origin.y
            let h = self.frame.height
            self.frame = CGRect(x: x, y: y, width: newValue, height: h)
        }
        get {
            return self.frame.width
        }
    }
    
    public var x: CGFloat {
        set {
            let y = self.frame.origin.y
            self.frame = CGRect(x: newValue, y: y, width: self.width, height: self.height)
        }
        get {
            return self.frame.origin.x
        }
    }
    
    public var y: CGFloat {
        set {
            self.frame = CGRect(x: self.x, y: newValue, width: self.width, height: self.height)
        }
        get {
            return self.frame.origin.y
        }
    }

    public var maxY: CGFloat {
        return self.frame.maxY
    }
    
    public var maxX: CGFloat {
        return self.frame.maxX
    }
    
    public func contains(tag: Int) -> UIView? {
        var v: UIView? = nil
        self.subviews.forEach { (view) in
            if view.tag == tag {
                v = view
            }
        }
        
        return v
    }
    
    public func contains(view v1: UIView) -> Bool {
        var has: Bool = false
        self.subviews.forEach { (view) in
            if v1 == view  {
                has = true
            }
        }
        return has
    }
    
}

extension UIView {
    
    
    public func showLoader(_ backBlur: Bool = false) {
        let center = CGPoint(x: self.width / 2, y: self.height / 2)
        
        self.showLoader(center: center, backBlur)
    }
    
    public func showLoader(center: CGPoint, _ backBlur: Bool = false) {
        
        if subviews.contains(where: {$0.tag == 213}) {
            return
        }
        let bnds = CGRect(x: 0, y: 0, width: center.x * 2, height: center.y * 2)
        let bgView = UIView(frame: bnds)
        
        let ai = UIActivityIndicatorView.init(style: .white)
        bgView.tag = 213
        bgView.backgroundColor = UIColor.gray.withAlphaComponent(0.1)
        bgView.layer.cornerRadius = self.layer.cornerRadius
//
//        if backBlur {
////            bgView.backgroundColor = .clear
////            bgView.addBlur(0.8)
//        }
//
////        self.setNeedsLayout()
        ai.center = center
        
        ai.startAnimating()
        
        ai.style = .gray
        
        self.addSubview(bgView)
        
        bgView.addSubview(ai)
        bgView.bringSubviewToFront(ai)
        
        self.bringSubviewToFront(bgView)
    }
    
    public func removeLoader(_ completion: (()->())? = nil) {
        self.subviews.forEach { (view) in
            if view.tag == 213 {
                view.isHidden = true
                self.sendSubviewToBack(view)
                view.removeFromSuperview()
                completion?()
                return
            }
        }
    }
    
    public func onClick(target: Any?, selector: Selector) {
        let gesture = UITapGestureRecognizer(target: target, action: selector)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
    
    public func onLongClick(target: Any?, selector: Selector) {
        let gesture = UILongPressGestureRecognizer(target: target, action: selector)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
}


extension UIImageView {
    public func tintColor(_ color: UIColor) {
        self.image = self.image?.tintColor(color: color)
        self.tintColorDidChange()
    }
}

public extension UIView {
    
    static func reportUser(_ message: String = "Done") {
        if let keyWindow = UIApplication.shared.keyWindow {
            let bounds = UIScreen.main.bounds
            
            let view = UIView()
            view.backgroundColor = UIColor.lightGray
            view.alpha = 0
            
            let label = UILabel()
            label.text = message
            label.textColor = UIColor.white
            label.font = UIFont.systemFont(ofSize: 12)
            label.textAlignment = .center
            
            view.addSubview(label)
            keyWindow.addSubview(view)
            
            var h = CGFloat(50)
            var w = CGFloat(180)
            var x = bounds.midX - w/2
            var y = bounds.midY - h/2
            view.frame = CGRect(x: x, y: y, width: w, height: h)
            view.layer.cornerRadius = h/2
            
            x = 5
            w = view.bounds.width - 2 * x
            h = 14
            y = view.bounds.midY - h/2
            label.frame = CGRect(x: x, y: y, width: w, height: h)
            
            /// Report user with animation
            UIView.animate(withDuration: 1.0, delay: 0, options: [.curveLinear], animations: {
                view.alpha = 1.0
            }, completion: { (success) in
                if success {
                    UIView.animate(withDuration: 0.7, delay: 0.3, options: [.curveLinear], animations: {
                        view.alpha = 0.0
                    }, completion: { (success) in
                        if success {
                            view.removeFromSuperview()
                        }
                    })
                }
            })
        }
    }
}
