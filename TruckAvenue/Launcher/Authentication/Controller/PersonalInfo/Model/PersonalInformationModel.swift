//
//  PersonalInformationModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 01/06/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class PersonalInformationModel {
    public var controller: PersonalInformationController
    public let imagePicker = UIImagePickerController()
    
    private let server: AuthenticationApiWorker = AuthenticationApiWorker.shared
    
    public let truckModelList = TruckTypeManager.shared.getAll()
    public let hasTruckType = TruckTypeManager.shared.hasTypes()
    
    public var truckNumber: StringValue = StringValue(24)
    public var truckModel: StringValue = StringValue(100)
    public var truckIsMain: Bool = true
    
    public init(controller: PersonalInformationController) {
        self.controller = controller
        truckNumber.isRequired = true
        truckModel.isRequired = true
    }
    
    @objc public func loadImageFromGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = controller
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            controller.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    public func checkInputFields() -> Bool {
        return self.controller.truckNumberInputField.checkInput("Truck number not valid") &&
            self.controller.truckModelInputField.checkInput("Truck model not valid")
    }
    
    public func uploadPersonalInformation(_ completion: @escaping ((Bool) -> Void)) {
        if checkInputFields() {
            server.createTruck(image: self.controller.truckPhotoImageView.image,
                               truckNumber: truckNumber.value,
                               truckModel: truckModel.value,
                               isMain: truckIsMain) { isOK in
                completion(isOK)
            }
        } else {
            completion(false)
        }
    }
}

