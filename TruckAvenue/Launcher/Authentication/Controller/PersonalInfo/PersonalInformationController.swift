//
//  PersonalInformationController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class PersonalInformationController: BaseViewController {
    private let bgView: UIView = UIView()
    private let titleLabel: UILabel = UILabel()
    public let truckNumberInputField: EditTextField = EditTextField()
    public let truckModelInputField: EditTextField = EditTextField()
    private let truckPhotoLabel: UILabel = UILabel()
    private let truckPhotoOptionalLabel: UILabel = UILabel()
    public let truckPhotoImageView: UIImageView = UIImageView()
    private let actionButton: UIButton = UIButton()
    private let makeMainLabel: UILabel = UILabel()
    private let mainSwitch: UISwitch = UISwitch()
    
    private var model: PersonalInformationModel? = nil
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDelegate.route.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    public override func initialize() {
        super.initialize()
        model = PersonalInformationModel(controller: self)
        
        if self.model?.hasTruckType ?? false {
            self.model?.truckModel.value = self.model?.truckModelList[0].name ?? ""
        }
        
        bgView.backgroundColor = theme.appWhite
        bgView.addShadow(shadowColor: theme.appShadow)
        
        self.view.addSubviews([bgView, actionButton])
        
        let truckNumberInput = EditTextFieldInput(title: "Number truck", description: "Enter truck number")
        truckNumberInputField.setInput(input: truckNumberInput)
        truckNumberInputField.inputType = .text
        if let binder = self.model?.truckNumber {
            truckNumberInputField.bind(binder)
        }
        
        let truckModelInput = EditTextFieldInput(title: "Truck model", description: "Enter truck model")
        truckModelInputField.setInput(input: truckModelInput)
        truckModelInputField.inputType = .text
        truckModelInputField.inputField.inputView = pickerView
        truckModelInputField.inputField.doneAccessory = true
        
        if let binder = self.model?.truckModel {
            truckModelInputField.bind(binder)
        }
        
        truckPhotoLabel.text = "Driver / Truck photo"
        truckPhotoOptionalLabel.text = "(Optional)"
        truckPhotoLabel.font = theme.appFont.withSize(14)
        truckPhotoOptionalLabel.font = theme.appFont.withSize(14)
        truckPhotoOptionalLabel.textColor = theme.labelGray
        
        titleLabel.font = theme.appFont.withSize(30)
        titleLabel.textColor = theme.bigBlueTextColor
        titleLabel.text = "Profile Information"
        
        actionButton.setTitle("Next".uppercased(), for: .normal)
        
        actionButton.addTarget(self, action: #selector(actionButtonAction), for: .touchUpInside)
        
        truckPhotoImageView.image = #imageLiteral(resourceName: "no_imagephoto_icon")
        
        truckPhotoImageView.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onImageViewClicked))
        truckPhotoImageView.isUserInteractionEnabled = true
        truckPhotoImageView.addGestureRecognizer(tapGestureRecognizer)
        
        mainSwitch.isOn = true
        mainSwitch.isUserInteractionEnabled = false
        mainSwitch.isEnabled = false
        
        makeMainLabel.text = "Make main"
        makeMainLabel.font = theme.appFont.withSize(14)
        
        bgView.addSubviews([titleLabel, truckNumberInputField, truckModelInputField, truckPhotoLabel, truckPhotoOptionalLabel, truckPhotoImageView,
            makeMainLabel, mainSwitch])
        
    }
    
    @objc private func onImageViewClicked() {
        self.model?.loadImageFromGallery()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var x: CGFloat = 0
        var y: CGFloat = self.topHeight
        var w: CGFloat = self.view.width
        var h: CGFloat = sizeCalculator.height(from: 433)
        
        self.bgView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = sizeCalculator.width(from: 30)
        y = sizeCalculator.height(from: 44)
        w = self.view.width - 2 * x
        h = sizeCalculator.height(from: 33)
        
        self.titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.titleLabel.maxY + sizeCalculator.height(from: 33)
        x = sizeCalculator.width(from: 16)
        w = self.view.width - 2 * x
        h = 70
        
        self.truckNumberInputField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = truckNumberInputField.maxY + sizeCalculator.height(from: 20)

        self.truckModelInputField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = truckModelInputField.maxY + sizeCalculator.height(from: 8)
        w = self.view.width / 2
        h = 31
        self.makeMainLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        w = 51
        x = self.view.width - w - 16
        self.mainSwitch.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = makeMainLabel.maxY + sizeCalculator.height(from: 10)
        h = sizeCalculator.height(from: 20)
        w = truckPhotoLabel.widthByText(height: h)
        x = sizeCalculator.width(from: 16)
        truckPhotoLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = truckPhotoLabel.maxX + sizeCalculator.width(from: 8)
        w = truckPhotoOptionalLabel.widthByText(height: h)
        truckPhotoOptionalLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = sizeCalculator.width(from: 16)
        y = truckPhotoLabel.maxY + sizeCalculator.height(from: 12)
        w = sizeCalculator.height(from: 80)
        h = w
        self.truckPhotoImageView.frame = CGRect(x: x, y: y, width: w, height: h)
        truckPhotoImageView.layer.cornerRadius = 4
        
        w = self.view.width
        h = sizeCalculator.height(from: 60)
        y = self.view.height - h
        x = 0
        actionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        actionButton.addGradient(
            left: theme.buttonBackgroundGradientColor.left,
            right: theme.buttonBackgroundGradientColor.right)
    }
    
    @objc func actionButtonAction() {
        self.showSpinner(text: "Uploading info, please wait!")
        self.model?.uploadPersonalInformation { isOK in
            self.removeSpinner()
            if isOK {
                appDelegate.route.navigate(to: .main)
            }
        }
    }
}

extension PersonalInformationController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.truckPhotoImageView.image = image
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.truckPhotoImageView.image = image
            }
        })
    }
}

extension PersonalInformationController {
    public override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.model?.truckModelList.count ?? 0
    }
    
    public override func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = self.model?.truckModelList[row].name ?? ""
        return title
    }
    
    public override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        truckModelInputField.inputField.text = self.model?.truckModelList[row].name ?? ""
    }
}
