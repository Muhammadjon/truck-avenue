//
//  ConfirmationModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 31/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class ConfirmationModel {
    public var codeNumber: StringValue = StringValue(6)
    private var server: AuthenticationApiWorker = AuthenticationApiWorker.shared
    public private(set) var fromRegistration: Bool = true
    
    public init(_ afterRegistration: Bool = true) {
        codeNumber.isRequired = true
        self.fromRegistration = afterRegistration
    }
    
    public func confirm(_ completion: @escaping ((Bool) -> Void)) {
        if self.codeNumber.isValid() {
            let login = UserPreferences.login
            server.confirmationCode(phone: login, code: codeNumber.text) { (isOK) in
                completion(isOK)
            }
        }
    }
}
