//
//  OTPConfirmationController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class OTPConfirmationController: BaseViewController {
    
    private let bodyView: UIView = UIView()
    private let backButton: UIButton = UIButton()
    private let titleLabel: UILabel = UILabel()
    private let inputField: EditTextField = EditTextField()
    private let repeatOTPLabel: UILabel = UILabel()
    private let repeatOTPImageView: UIImageView = UIImageView()
    private let actionButton: UIButton = UIButton()

    private var model = ConfirmationModel(true)
    
    public override func initialize() {
        super.initialize()
        bodyView.backgroundColor = theme.appWhite
        bodyView.addShadow(shadowColor: theme.appShadow)
        
        actionButton.setTitle("Confirm".uppercased(), for: .normal)
        actionButton.addTarget(self, action: #selector(onConfirmActionButtonClicked), for: .touchUpInside)
        
        self.addSubviews([bodyView, actionButton])
     
        titleLabel.font = theme.appFont.withSize(30)
        titleLabel.textColor = theme.bigBlueTextColor
        titleLabel.text = "Number confirmation"
        
        backButton.setImage(#imageLiteral(resourceName: "icon_back_2"), for: .normal)
        
        let input = EditTextFieldInput(title: "Confirmation code", description: "Code sent to you")
        inputField.setInput(input: input)
        
        inputField.bind(self.model.codeNumber)
        
        backButton.addTarget(self, action: #selector(pop), for: .touchUpInside)
        
        repeatOTPLabel.text = "Resend code"
        
        repeatOTPImageView.image = #imageLiteral(resourceName: "repeat_icon")
        bodyView.addSubviews([backButton, titleLabel, inputField, repeatOTPLabel, repeatOTPImageView])
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var x: CGFloat = 0
        var y: CGFloat = self.topHeight
        var h: CGFloat = sizeCalculator.height(from: 384)
        var w: CGFloat = self.view.width
        
        bodyView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = sizeCalculator.width(from: 16)
        y = sizeCalculator.height(from: 40)
        h = 24
        w = h
        backButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = sizeCalculator.width(from: 16)
        y = backButton.maxY + sizeCalculator.height(from: 75)
        h = sizeCalculator.height(from: 32)
        w = self.view.width - 2 * x
        titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = titleLabel.maxY + sizeCalculator.height(from: 22)
        h = 70
        inputField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = inputField.maxY + sizeCalculator.height(from: 44)
        h = sizeCalculator.height(from: 16)
        w = repeatOTPLabel.widthByText(height: h)
        
        x = (self.view.width - w) / 2
        repeatOTPLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = sizeCalculator.width(from: 32)
        w = h
        x = repeatOTPLabel.maxX + sizeCalculator.width(from: 8)
        y = y - (h - repeatOTPLabel.height) / 2
        repeatOTPImageView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        
        w = self.view.width
        h = sizeCalculator.height(from: 60)
        y = self.view.height - h
        x = 0
        actionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        actionButton.addGradient(
            left: theme.buttonBackgroundGradientColor.left,
            right: theme.buttonBackgroundGradientColor.right)
    }
    
    @objc func onConfirmActionButtonClicked() {
        
        if self.inputField.checkInput("Not valid code") {
        self.showSpinner(text: "Please wait")
            self.model.confirm { (isOK) in
                self.removeSpinner()
                if isOK {
                    if self.model.fromRegistration {
                        self.navigateToPersonalInformationController()
                    } else {
                        self.navigateToMain()
                    }
                } else {
                    ApplicationNetwork.printError(self)
                }
            }
        }
    }
    
    public func setModel(_ model: ConfirmationModel) {
        self.model = model
    }
    
    private func navigateToPersonalInformationController() {
        let vc = PersonalInformationController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigateToMain() {
        appDelegate.route.navigate(to: .main)
    }
}
