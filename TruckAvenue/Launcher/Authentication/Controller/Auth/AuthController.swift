//
//  AuthController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class AuthController: BaseViewController {
    
    private let signUpSectionButton: UIButton = UIButton()
    private let logInSectionButton: UIButton = UIButton()
    
    private let bodyView: UIView = UIView()
    private let logInBodyView: LoginBodyView = LoginBodyView()
    private let signUpBodyView: SignUpBodyView = SignUpBodyView()
    
    private let titleLabel: UILabel = UILabel()
    
    private let actionButton: UIButton = UIButton()
    
    private let model: AuthModel = AuthModel()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.route.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    public override func initialize() {
        super.initialize()
        
        bodyView.backgroundColor = UIColor.white
        bodyView.addShadow(shadowColor: theme.appShadow)
        
        self.addSubviews([bodyView, actionButton])
        
        signUpSectionButton.isUserInteractionEnabled = true
        signUpSectionButton.setTitleColor(theme.labelGray, for: .normal)
        signUpSectionButton.setTitle("Sign Up", for: .normal)
        
        logInSectionButton.isUserInteractionEnabled = true
        logInSectionButton.setTitle("Log In", for: .normal)
        logInSectionButton.setTitleColor(theme.appBlack, for: .normal)
        
        bodyView.addSubview(logInBodyView)
        bodyView.addSubviews([signUpSectionButton, logInSectionButton, signUpBodyView])
        
        signUpBodyView.isHidden = true
        
        signUpSectionButton.addTarget(self, action: #selector(onSignUpSectionButtonClicked(_:)), for: .touchUpInside)
        logInSectionButton.addTarget(self, action: #selector(onLogInSectionButtonClicked(_:)), for: .touchUpInside)
        actionButton.addTarget(self, action: #selector(onActionButtonClicked), for: .touchUpInside)
    }
    
    public override func initializeData() {
        super.initializeData()
        if model.isForRegistration {
            actionButton.setTitle("Sing Up".uppercased(), for: .normal)
        } else {
            actionButton.setTitle("Log In".uppercased(), for: .normal)
        }
        
        self.signUpBodyView.onFieldStartsEditing = { field in
            print(self.view!.convert(field.frame, from: self.signUpBodyView))
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HistoryManager.shared.shouldUpdate = true
        UserPreferences.token = ""
        UserPreferences.login = ""
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var y: CGFloat = 0
        var w: CGFloat = 0
        var h: CGFloat = 0
        var x: CGFloat = 0
        
        w = self.view.width
        if model.isForRegistration {
            h = self.view.height * 0.75
        } else {
            h = self.view.height * 0.50
        }
        y = self.topHeight
        x = 0
        self.bodyView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = self.topHeight + self.view.frame.height * 0.043
        w = 100
        h = 40
        x = self.view.frame.width / 2  - w - 20
        self.signUpSectionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = self.view.frame.width / 2 + 20
        self.logInSectionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        x = self.view.frame.width * 0.044
        y = self.logInSectionButton.frame.maxY + self.view.frame.height * 0.078
        w = (bodyView.frame.width - 2 * x)
        h = self.bodyView.bounds.height - y
        logInBodyView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        signUpBodyView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = sizeCalculator.height(from: 60)
        w = self.view.bounds.width
        x = 0
        y = self.view.bounds.height - h
        actionButton.frame = CGRect(x: x, y: y, width: w, height: h)
        
        actionButton.addGradient(left: theme.buttonBackgroundGradientColor.left, right: theme.buttonBackgroundGradientColor.right)
    }
    
    @objc func onLogInSectionButtonClicked(_ sender: UIButton) {
        self.model.isForRegistration = false
        signUpSectionButton.setTitleColor(theme.labelGray, for: .normal)
        logInSectionButton.setTitleColor(theme.appBlack, for: .normal)
        logInBodyView.isHidden = false
        signUpBodyView.isHidden = true
        initializeData()
        self.view.setNeedsLayout()

    }
    
    @objc func onSignUpSectionButtonClicked(_ sender: UIButton) {
        self.model.isForRegistration = true
        signUpSectionButton.setTitleColor(theme.appBlack, for: .normal)
        logInSectionButton.setTitleColor(theme.labelGray, for: .normal)
        logInBodyView.isHidden = true
        signUpBodyView.isHidden = false
        initializeData()
        self.view.setNeedsLayout()
        
    }
    
    @objc func onActionButtonClicked() {
        if self.model.isForRegistration {
            self.registerUser()
        } else {
            self.loginUser()
        }
    }
    
    private func registerUser() {
        if !self.signUpBodyView.checkInputFields() {
            return
        }
        self.showSpinner(text: "Regisration")
        self.signUpBodyView.model.register { (isOK) in
            self.removeSpinner()
            if isOK {
                print("registered")
                self.confirmOTP()
            } else {
                ApplicationNetwork.printError(self)
            }
        }
    }
    
    private func loginUser() {
        if !self.logInBodyView.checkInputFields() {
            return
        }
        self.showSpinner(text: "Logging in")

        self.logInBodyView.model.login { (isOK) in
            self.removeSpinner()
            if isOK {
                print("logged in")
                self.confirmOTP()
            } else {
                ApplicationNetwork.printError(self)
            }
        }
    }
    
    private func confirmOTP () {
        let otpVC = OTPConfirmationController()
        otpVC.setModel(ConfirmationModel(self.model.isForRegistration))
        appDelegate.route.navigationController?.pushViewController(otpVC, animated: true)
    }
    
    public override func onKeyboardShow(_ notification: Notification) {
        view.y = 0
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            let diff = signUpBodyView.maxY - (self.view.height - keyboardHeight) - 40
            
            if  diff >= 0 {
                view.y = view.y - diff
            }
        }
    }
    
    public override func onKeyboardHide(_ notification: Notification) {
        view.y = 0
    }
}
