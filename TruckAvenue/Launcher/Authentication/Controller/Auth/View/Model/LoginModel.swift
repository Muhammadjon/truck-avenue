//
//  LoginModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 31/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class LoginModel {
    public var phoneNumber: StringValue = StringValue()
    private var server: AuthenticationApiWorker = AuthenticationApiWorker.shared
    public init() {
        phoneNumber.isRequired = true
    }
    
    public func login(_ completion: @escaping ((Bool) -> Void)) {
        if self.phoneNumber.isValid() {
            server.loginUser(phone: phoneNumber.text) { (isOK) in
                completion(isOK)
            }
        }
    }
}
