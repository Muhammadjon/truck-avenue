//
//  SignUpModel.swift
//  TruckAvenue
//
//  Created by muhammadjon on 28/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import Foundation

public class SignUpModel {
    public var phoneNumber: StringValue = StringValue()
    public var email: EmailValue = EmailValue(70)
    public var userName: StringValue = StringValue(25)
    
    private let server: AuthenticationApiWorker = AuthenticationApiWorker.shared
    
    public var isReady: Bool {
        return phoneNumber.isValid() && email.isValid() && userName.isValid()
    }
    
    public init() {
        email.isRequired = true
        phoneNumber.isRequired = true
        userName.isRequired = true
    }
    
    public func register(_ completion: @escaping ((Bool) -> Void)) {
        if isReady {
            self.server.registerUser(phone: phoneNumber.text, email: email.text, username: userName.text) { (isOK) in
                completion(isOK)
            }
        }
    }
}
