//
//  LoginBodyView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class SignUpBodyView: UIView {
    private let titleLabel: UILabel = UILabel()
    
    private let loginField: EditTextField = EditTextField()
    private let emailField: EditTextField = EditTextField()
    private let usernameField: EditTextField = EditTextField()
    
    public var model: SignUpModel = SignUpModel()
    
    public var onFieldStartsEditing: ((UITextField) -> Void)?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        let theme = appDelegate.model.themeManager.currentTheme()
        
        titleLabel.font = theme.appFont.withSize(30)
        titleLabel.textColor = theme.bigBlueTextColor
        titleLabel.text = "Create your Account"
        
        self.addSubviews([titleLabel, loginField, emailField, usernameField])
        
        let loginFieldInput = EditTextFieldInput(title: "Phone number", description: "")
        loginFieldInput.prefix = "+1"
        loginField.setInput(input: loginFieldInput)
        loginField.inputType = .phoneNumber
        loginField.bind(self.model.phoneNumber)
        
        let emailFieldInput = EditTextFieldInput(title: "Email", description: "")
        emailField.setInput(input: emailFieldInput)
        emailField.inputType = .email
        emailField.bind(self.model.email)
        
        let usernameFieldInput = EditTextFieldInput(title: "Username", description: "")
        usernameField.setInput(input: usernameFieldInput)
        usernameField.inputType = .text
        usernameField.bind(self.model.userName)
        
        self.analizeData()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        var y: CGFloat = 0
        var w: CGFloat = 0
        var h: CGFloat = 0
        var x: CGFloat = 0
        
        x = 0
        y = 0
        w = frame.width
        h = 35
        titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = 60
        y = titleLabel.frame.maxY + 23/640 * UIScreen.main.bounds.height
        loginField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = loginField.frame.maxY + 15/640 * UIScreen.main.bounds.height
        emailField.frame = CGRect(x: x, y: y, width: w, height: h)
        
        y = emailField.frame.maxY + 15/640 * UIScreen.main.bounds.height
        usernameField.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    private func analizeData() {
        loginField.endEditing = { field in
            if self.model.phoneNumber.hasError {
                //handle login error
                self.loginField.errorMessage = "Invalid phone number"
            }
        }
        
        emailField.endEditing = { field in
            if self.model.email.hasError {
                //handle email error
                self.emailField.errorMessage = "Invalid email"
            }
        }
        
        usernameField.endEditing = { field in
            if self.model.userName.hasError {
                //handle username error
                self.usernameField.errorMessage = "Invalid username"
            }
        }
    }
    
    public func checkInputFields() -> Bool {
        return self.loginField.checkInput("Invalid phone number") &&
            self.emailField.checkInput("Invalid email") &&
            self.usernameField.checkInput("Invalid username")
    }
}
