//
//  LoginBodyView.swift
//  TruckAvenue
//
//  Created by muhammadjon on 27/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class LoginBodyView: UIView {
    private let titleLabel: UILabel = UILabel()
    
    private let loginField: EditTextField = EditTextField()

    public let model: LoginModel = LoginModel()
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        let theme = appDelegate.model.themeManager.currentTheme()
        
        titleLabel.font = theme.appFont.withSize(30)
        titleLabel.textColor = theme.bigBlueTextColor
        titleLabel.text = "Welcome back!"
        
        self.addSubviews([titleLabel, loginField])
        
        let loginFieldInput = EditTextFieldInput(title: "Phone number", description: "")
        loginFieldInput.prefix = "+1"
        loginField.setInput(input: loginFieldInput)
        loginField.inputType = .phoneNumber
        loginField.bind(self.model.phoneNumber)
        
        self.analizeData()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        var y: CGFloat = 0
        var w: CGFloat = 0
        var h: CGFloat = 0
        var x: CGFloat = 0
        
        x = 0
        y = 0
        w = frame.width
        h = 35
        titleLabel.frame = CGRect(x: x, y: y, width: w, height: h)
        
        h = 60
        y = titleLabel.frame.maxY + 23/640 * UIScreen.main.bounds.height
        loginField.frame = CGRect(x: x, y: y, width: w, height: h)        
    }
    
    public func analizeData() {
        loginField.endEditing = { field in
            if self.model.phoneNumber.hasError {
                field.errorMessage = "Not valid phone number"
            }
        }
    }
    
    public func checkInputFields() -> Bool {
        return self.loginField.checkInput("Invalid phone number")
    }
}
