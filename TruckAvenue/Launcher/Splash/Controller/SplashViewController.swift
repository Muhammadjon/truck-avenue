//
//  SplashViewController.swift
//  TruckAvenue
//
//  Created by muhammadjon on 26/05/2019.
//  Copyright © 2019 muhammadjon. All rights reserved.
//

import UIKit

public class SplashViewController: UIViewController {
    private let backgroundImageView: UIImageView = UIImageView()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.route.navigationController?.setNavigationBarHidden(true, animated: false)
    
        self.addSubviews([backgroundImageView])

        self.backgroundImageView.frame = self.view.bounds
        self.backgroundImageView.image = #imageLiteral(resourceName: "splash_image_2")
        
        UIView.animate(withDuration: 2.0,
                       delay: 1,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                        
        }, completion: { _ in
            self.waitASecond()
        })
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.route.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func waitASecond() {
        if UserPreferences.isSignedIn {
            appDelegate.model.loadPrerequisites { (isOK) in
                if ApplicationNetwork.StatusDetector.status == 401 {
                    UserPreferences.logout()
                    return
                }
                
                if isOK {
                    appDelegate.route.navigate(to: .main)
                }
            }
            return
        }

        appDelegate.route.navigate(to: .authentication)
    }
}
